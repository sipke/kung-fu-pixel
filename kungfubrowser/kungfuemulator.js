var colors = [0x000000, 0xFFFFFF, 0x68372B, 0x70A4B2, 0x6F3D86, 0x588D43, 0x352879, 0xB8C76F, 0x6F4F25, 0x433900, 0x9A6759, 0x444444, 0x6C6C6C, 0x9AD284, 0x6C5EB5, 0x959595];
var joyVals=[];

// 6502
var memory;
var entryPoint;
var exitPoint;
var newSongPoint;
var currentSong;
var colorsAddressX;
var colorsAddressY;
var colorsAddressA;
var dc00 = 0xff;
var dc01 =0xff;
var //CPU (and CIA/VIC-IRQ) emulation constants and variables - avoiding internal/automatic variables to retain speed
flagsw=[0x01,0x21,0x04,0x24,0x00,0x40,0x08,0x28], branchflag=[0x80,0x40,0x01,0x02];
var PC=0, A=0, T=0, X=0, Y=0, SP=0xFF, IR=0, addr=0, ST=0x00, cycles=0, storadd=0; //STATUS-flags: N V - B D I Z C

//function initCPU (mempos) { PC=mempos; A=0; X=0; Y=0; ST=0; SP=0xFF; } 
var canvas;
var ctx;

function init(canvas, entryPoint, exitPoint, newSongPoint, colorsAddress, currentSong){
    this.canvas = canvas;
    this.entryPoint = entryPoint;
    this.exitPoint = exitPoint;
    this.newSongPoint = newSongPoint;
    this.colorsAddressA = colorsAddress;
    this.colorsAddressX = colorsAddress + 21;
    this.colorsAddressY = colorsAddress + 42;
    this.currentSong = currentSong;
    ctx = canvas.getContext('2d');
    //initMemory
    //var base64 = dataURI.substring(base64Index);
    var raw = window.atob(initMemS);
    var rawLength = raw.length;
    memory = new Uint8Array(new ArrayBuffer(rawLength));
    for (var i=0; i<rawLength; i++) {
        memory[i] = raw.charCodeAt(i);
    }
    var //CPU (and CIA/VIC-IRQ) emulation constants and variables - avoiding internal/automatic variables to retain speed
    flagsw=[0x01,0x21,0x04,0x24,0x00,0x40,0x08,0x28], branchflag=[0x80,0x40,0x01,0x02];
    var PC=0, A=0, T=0, X=0, Y=0, SP=0xFF, IR=0, addr=0, ST=0x00, cycles=0, storadd=0; //STATUS-flags: N V - B D I Z C
   
    function initCPU (mempos) { PC=mempos; A=0; X=0; Y=0; ST=0; SP=0xFF; } 

    A = 0x08;
    X = 0x0E;
    Y = 0x00;
    PC = 0x4d4c
    SP= 0x07;
    ST = 0b00100000;
    joyVals["w"]=1;
    joyVals["s"]=2;
    joyVals["a"]=4
    joyVals["d"]=8;
    joyVals["W"]=17;
    joyVals["S"]=28;
    joyVals["A"]=20
    joyVals["D"]=24;
    joyVals["Shift"]=16;
    initDraw();
    //window.onEachFrame = onEachFrame;
    //onEachFrame(doFrame);
    window.requestAnimationFrame(doFrame);
}

initDraw =function() {
  var onEachFrame;
  if (window.requestAnimationFrame) {
    onEachFrame = function(cb) {
      var _cb = function() { cb(); requestAnimationFrame(_cb); }
      _cb();
    };
  } else {
    onEachFrame = function(cb) {
      setInterval(cb, 1000 / 60);
    }
  }
}



function doFrame(){
    PC = entryPoint;
    var count =0;
    while (PC !== exitPoint && count<10000){
        CPU();
    }
    drawFromMemory();
    window.requestAnimationFrame(doFrame);
}
function drawFromMemory() {
    var m;
    var cx;
    var cy;
    var ca;
    var c;
    for (var x=0;x<12; x++){
        for (var y=0;y<21; y++){
            ca = this.memory[this.colorsAddressA+y];
            cx = this.memory[this.colorsAddressX+y];
            cy = this.memory[this.colorsAddressY+y];
            cN = (cx & ca);
            m = this.memory[2 + x + y*12];

            switch (m) {
                case 0x8C:
                    c = cy;
                    break;
                    case 0x8D:
                    c = ca;
                    break;
                    case 0x8E:
                    c = cx;
                    break;
                    case 0x8F:
                    c = cN;
                    break;
                    default:
                    console.error('illegal data ' + m + ' at ' +  (2 + x + y*12));
            }
            this.ctx.fillStyle ="#"+ this.colors[c].toString(16);
            this.ctx.fillRect(x*32,y*12,x*32 + 32, y*12+12);
            
        }
        
    }
}
/*
CPU function from jsSID by
Mihaly Horvath (Hermit)
2016, Hungary
http://hermit.sidrip.com
*/
function CPU () //the CPU emulation for SID/PRG playback (ToDo: CIA/VIC-IRQ/NMI/RESET vectors, BCD-mode)
{ //'IR' is the instruction-register, naming after the hardware-equivalent
    //joystick value intercept
    memory[0xdc00] = dc00;
    memory[0xdc01] = dc01;
    //new song intercept
    if(PC==newSongPoint){
        var s = memory[currentSong] % 4;
        playSong(s);
     }


    IR=memory[PC]; cycles=2; storadd=0; //'cycle': ensure smallest 6510 runtime (for implied/register instructions)
 
 if(IR&1) {  //nybble2:  1/5/9/D:accu.instructions, 3/7/B/F:illegal opcodes
  switch (IR&0x1F) { //addressing modes (begin with more complex cases), PC wraparound not handled inside to save codespace
   case 1: case 3: addr = memory[memory[++PC]+X] + memory[memory[PC]+X+1]*256; cycles=6; break; //(zp,x)
   case 0x11: case 0x13: addr = memory[memory[++PC]] + memory[memory[PC]+1]*256 + Y; cycles=6; break; //(zp),y
   case 0x19: case 0x1F: addr = memory[++PC] + memory[++PC]*256 + Y; cycles=5; break; //abs,y
   case 0x1D: addr = memory[++PC] + memory[++PC]*256 + X; cycles=5; break; //abs,x
   case 0xD: case 0xF: addr = memory[++PC] + memory[++PC]*256; cycles=4; break; //abs
   case 0x15: addr = memory[++PC] + X; cycles=4; break; //zp,x
   case 5: case 7: addr = memory[++PC]; cycles=3; break; //zp
   case 0x17: addr = memory[++PC] + Y; cycles=4; break; //zp,y for LAX/SAX illegal opcodes
   case 9: case 0xB: addr = ++PC; cycles=2;  //immediate
  }
  addr&=0xFFFF;
  switch (IR&0xE0) {
   case 0x60: T=A; A+=memory[addr]+(ST&1); ST&=20; ST|=(A&128)|(A>255); A&=0xFF; ST|=(!A)<<1 | (!((T^memory[addr])&0x80) && ((T^A)&0x80))>>1; break; //ADC
   case 0xE0: T=A; A-=memory[addr]+!(ST&1); ST&=20; ST|=(A&128)|(A>=0); A&=0xFF; ST|=(!A)<<1 | (((T^memory[addr])&0x80) && ((T^A)&0x80))>>1; break; //SBC
   case 0xC0: T=A-memory[addr]; ST&=124;ST|=(!(T&0xFF))<<1|(T&128)|(T>=0); break; //CMP
   case 0x00: A|=memory[addr]; ST&=125;ST|=(!A)<<1|(A&128); break; //ORA 
   case 0x20: A&=memory[addr]; ST&=125;ST|=(!A)<<1|(A&128); break; //AND
   case 0x40: A^=memory[addr]; ST&=125;ST|=(!A)<<1|(A&128); break; //EOR
   case 0xA0: A=memory[addr]; ST&=125;ST|=(!A)<<1|(A&128); if((IR&3)==3) X=A; break; //LDA / LAX (illegal, used by my 1 rasterline player)
   case 0x80: memory[addr]=A & (((IR&3)==3)?X:0xFF); storadd=addr;   //STA / SAX (illegal)
  }
 }
 
 else if(IR&2) {  //nybble2:  2:illegal/LDX, 6:A/X/INC/DEC, A:Accu-shift/reg.transfer/NOP, E:shift/X/INC/DEC
  switch (IR&0x1F) { //addressing modes
   case 0x1E: addr = memory[++PC] + memory[++PC]*256 + ( ((IR&0xC0)!=0x80) ? X:Y ); cycles=5; break; //abs,x / abs,y
   case 0xE: addr = memory[++PC] + memory[++PC]*256; cycles=4; break; //abs
   case 0x16: addr = memory[++PC] + ( ((IR&0xC0)!=0x80) ? X:Y ); cycles=4; break; //zp,x / zp,y
   case 6: addr = memory[++PC]; cycles=3; break; //zp
   case 2: addr = ++PC; cycles=2;  //imm.
  }  
  addr&=0xFFFF; 
  switch (IR&0xE0) {
   case 0x00: ST&=0xFE; case 0x20: if((IR&0xF)==0xA) { A=(A<<1)+(ST&1); ST&=60;ST|=(A&128)|(A>255); A&=0xFF; ST|=(!A)<<1; } //ASL/ROL (Accu)
     else { T=(memory[addr]<<1)+(ST&1); ST&=60;ST|=(T&128)|(T>255); T&=0xFF; ST|=(!T)<<1; memory[addr]=T; cycles+=2; }  break; //RMW (Read-Write-Modify)
   case 0x40: ST&=0xFE; case 0x60: if((IR&0xF)==0xA) { T=A; A=(A>>1)+(ST&1)*128; ST&=60;ST|=(A&128)|(T&1); A&=0xFF; ST|=(!A)<<1; } //LSR/ROR (Accu)
     else { T=(memory[addr]>>1)+(ST&1)*128; ST&=60;ST|=(T&128)|(memory[addr]&1); T&=0xFF; ST|=(!T)<<1; memory[addr]=T; cycles+=2; }  break; //RMW
   case 0xC0: if(IR&4) { memory[addr]--; memory[addr]&=0xFF; ST&=125;ST|=(!memory[addr])<<1|(memory[addr]&128); cycles+=2; } //DEC
     else {X--; X&=0xFF; ST&=125;ST|=(!X)<<1|(X&128);}  break; //DEX
   case 0xA0: if((IR&0xF)!=0xA) X=memory[addr];  else if(IR&0x10) {X=SP;break;}  else X=A;  ST&=125;ST|=(!X)<<1|(X&128);  break; //LDX/TSX/TAX
   case 0x80: if(IR&4) {memory[addr]=X;storadd=addr;}  else if(IR&0x10) SP=X;  else {A=X; ST&=125;ST|=(!A)<<1|(A&128);}  break; //STX/TXS/TXA
   case 0xE0: if(IR&4) { memory[addr]++; memory[addr]&=0xFF; ST&=125;ST|=(!memory[addr])<<1|(memory[addr]&128); cycles+=2; } //INC/NOP
  }
 }
 
 else if((IR&0xC)==8) {  //nybble2:  8:register/status
  switch (IR&0xF0) {
   case 0x60: SP++; SP&=0xFF; A=memory[0x100+SP]; ST&=125;ST|=(!A)<<1|(A&128); cycles=4; break; //PLA
   case 0xC0: Y++; Y&=0xFF; ST&=125;ST|=(!Y)<<1|(Y&128); break; //INY
   case 0xE0: X++; X&=0xFF; ST&=125;ST|=(!X)<<1|(X&128); break; //INX
   case 0x80: Y--; Y&=0xFF; ST&=125;ST|=(!Y)<<1|(Y&128); break; //DEY
   case 0x00: memory[0x100+SP]=ST; SP--; SP&=0xFF; cycles=3; break; //PHP
   case 0x20: SP++; SP&=0xFF; ST=memory[0x100+SP]; cycles=4; break; //PLP
   case 0x40: memory[0x100+SP]=A; SP--; SP&=0xFF; cycles=3; break; //PHA
   case 0x90: A=Y; ST&=125;ST|=(!A)<<1|(A&128); break; //TYA
   case 0xA0: Y=A; ST&=125;ST|=(!Y)<<1|(Y&128); break; //TAY
   default: if(flagsw[IR>>5]&0x20) ST|=(flagsw[IR>>5]&0xDF); else ST&=255-(flagsw[IR>>5]&0xDF);  //CLC/SEC/CLI/SEI/CLV/CLD/SED
  }
 }
 
 else {  //nybble2:  0: control/branch/Y/compare  4: Y/compare  C:Y/compare/JMP
  if ((IR&0x1F)==0x10) { PC++; T=memory[PC]; if(T&0x80) T-=0x100; //BPL/BMI/BVC/BVS/BCC/BCS/BNE/BEQ  relative branch 
   if(IR&0x20) {if (ST&branchflag[IR>>6]) {PC+=T;cycles=3;}} else {if (!(ST&branchflag[IR>>6])) {PC+=T;cycles=3;}}  } 
  else {  //nybble2:  0:Y/control/Y/compare  4:Y/compare  C:Y/compare/JMP
   switch (IR&0x1F) { //addressing modes
    case 0: addr = ++PC; cycles=2; break; //imm. (or abs.low for JSR/BRK)
    case 0x1C: addr = memory[++PC] + memory[++PC]*256 + X; cycles=5; break; //abs,x
    case 0xC: addr = memory[++PC] + memory[++PC]*256; cycles=4; break; //abs
    case 0x14: addr = memory[++PC] + X; cycles=4; break; //zp,x
    case 4: addr = memory[++PC]; cycles=3;  //zp
   }  
   addr&=0xFFFF;  
   switch (IR&0xE0) {
    case 0x00: memory[0x100+SP]=PC%256; SP--;SP&=0xFF; memory[0x100+SP]=PC/256;  SP--;SP&=0xFF; memory[0x100+SP]=ST; SP--;SP&=0xFF; 
      PC = memory[0xFFFE]+memory[0xFFFF]*256-1; cycles=7; break; //BRK
    case 0x20: if(IR&0xF) { ST &= 0x3D; ST |= (memory[addr]&0xC0) | ( !(A&memory[addr]) )<<1; } //BIT
     else { 
         memory[0x100+SP]=(PC+2)%256; SP--;SP&=0xFF; memory[0x100+SP]=(PC+2)/256;  SP--;SP&=0xFF; PC=memory[addr]+memory[addr+1]*256-1; cycles=6; 
        }  
         break; //JSR
    case 0x40: if(IR&0xF) { PC = addr-1; cycles=3; } //JMP
     else { if(SP>=0xFF) return 0xFE; SP++;SP&=0xFF; ST=memory[0x100+SP]; SP++;SP&=0xFF; T=memory[0x100+SP]; SP++;SP&=0xFF; PC=memory[0x100+SP]+T*256-1; cycles=6; }  break; //RTI
    case 0x60: if(IR&0xF) { PC = memory[addr]+memory[addr+1]*256-1; cycles=5; } //JMP() (indirect)
     else { if(SP>=0xFF) return 0xFF; SP++;SP&=0xFF; T=memory[0x100+SP]; SP++;SP&=0xFF; PC=memory[0x100+SP]+T*256-1; cycles=6; }  break; //RTS
    case 0xC0: T=Y-memory[addr]; ST&=124;ST|=(!(T&0xFF))<<1|(T&128)|(T>=0); break; //CPY
    case 0xE0: T=X-memory[addr]; ST&=124;ST|=(!(T&0xFF))<<1|(T&128)|(T>=0); break; //CPX
    case 0xA0: Y=memory[addr]; ST&=125;ST|=(!Y)<<1|(Y&128); break; //LDY
    case 0x80: memory[addr]=Y; storadd=addr;  //STY
   }
  }
 }
 
 PC++; PC&=0xFFFF; return 0; //memory[addr]&=0xFF; 
} 

function handleFire(ev){
    console.log(ev.type);
    dc00 &= (0xff-joyVals['f']);    
}
function handleFireEnd(ev){
    console.log(ev.type);
    dc00 |= joyVals['f'];    
}
function handleMoveEnd(ev){
    console.log("eb"+dc00);
    dc00 |= joyVals['u'];    
    dc00 |= joyVals['d'];    
    dc00 |= joyVals['l'];    
    dc00 |= joyVals['r'];  
    console.log("ea"+dc00);  
}
function handleMove(ev){
    console.log(dc00);
    switch (ev.type){
        
        case 'panup':
            dc00 &= (0xff-joyVals['u']);    
            break;
        case 'pandown':
            dc00 &= (0xff-joyVals['d']);    
            break;
        case 'panleft':
            dc00 &= (0xff-joyVals['l']);    
            break;
        case 'panright':
            dc00 &= (0xff-joyVals['r']);    
            break;
    }
    console.log("a"+ dc00);
}

function handleKeyUp(e){
    if(joyVals[e.key]){
        dc00 |= joyVals[e.key];
        return null;
    }
}
function handleKeyDown(e){
    if(joyVals[e.key]){
        dc00 &= (0xff-joyVals[e.key]);
        return null;
    }
    
}

function handleMouseDown(e){
    var c = e.target;
    var j = c.dataset.joy=="1" ? dc01: dc00;
    if(c.dataset.joy=="1") {
        dc01 &= (0xff-joyVals[c.dataset.dir]);
    }else{
        dc00 &= (0xff-joyVals[c.dataset.dir]);
    }
}
function handleMouseUp(e){
    var c = e.target;
    if(c.dataset.joy=="1") {
        dc01 |= joyVals[c.dataset.dir];
    }else {
        dc00 |= joyVals[c.dataset.dir];
    }
}


