 // EFFECT TABLE
     //  medkick highkic lowkick lowpunc   punch jumpkic backwar highdef normald smallju highjum  crouch neutral
     .byte      11,      0,      0,      0,      0,      0,      0,      0,      4,      0,      0,      0,      6  // medkick
     .byte      0,      11,      0,      0,      0,      0,      0,      4,      0,      0,      0,      0,      7  // highkick
     .byte      0,      0,      11,      0,      0,      0,      0,      0,      0,      0,      4,      0,      8  // lowkick
     .byte      0,      0,      0,      11,      0,      0,      0,      0,      0,      4,      0,      0,      6  // lowpunch
     .byte      0,      0,      0,      0,      11,      0,      0,      0,      4,      0,      0,      0,      6  // punch
     .byte      0,      0,      0,      0,      0,      11,      4,      0,      0,      0,      0,      0,      7  // jumpkick
     .byte      0,      0,      0,      0,      0,      4,      0,      0,      0,      0,      0,      0,      0  // backwardsdefence
     .byte      0,      4,      0,      0,      0,      0,      0,      0,      0,      0,      0,      0,      0  // highdefense
     .byte      4,      0,      0,      0,      4,      0,      0,      0,      0,      0,      0,      0,      0  // normaldefense
     .byte      0,      0,      0,      4,      0,      0,      0,      0,      0,      0,      0,      0,      0  // smalljumpdefense
     .byte      0,      0,      4,      0,      0,      0,      0,      0,      0,      0,      0,      0,      0  // highjumpdefense
     .byte      0,      0,      0,      0,      0,      0,      0,      0,      0,      0,      0,      0,      0  // crouch
     .byte      1,      2,      3,      1,      1,      2,      0,      0,      0,      0,      0,      0,      0  // neutral
//------------effects------------
//          effect         player1         player2
//               0              --              --
//               1          HitMid          Score1
//               2         HitHigh          Score1
//               3          HitLow          Score1
//               4      LoseEnergy        NoEffect
//               5      DrivenBack        NoEffect
//               6          Score1          HitMid
//               7          Score1         HitHigh
//               8          Score1          HitLow
//               9        NoEffect      LoseEnergy
//              10        NoEffect      DrivenBack
//              11  BothLoseEnergy  BothLoseEnergy
//              12  Player2Penalty  Player2Penalty
//              13  Player1Penalty  Player1Penalty
//              14        NoEffect        NoEffect
//              15      Player1Win      Player1Win
//              16      Player2Win      Player2Win
//              17Player2NextLevelPlayer2NextLevel
//              18       OutOfTime       OutOfTime
effectJmpTbl1Lo:    .byte <Qplayer1HitMid,<Qplayer1HitHigh,<Qplayer1HitLow,<Qplayer1LoseEnergy,<Qplayer1DrivenBack,<Qplayer1Score1,<Qplayer1Score1,<Qplayer1Score1,<Qplayer1NoEffect,<Qplayer1NoEffect,<Qplayer1BothLoseEnergy,<Qplayer1Player2Penalty,<Qplayer1Player1Penalty,<Qplayer1NoEffect,<Qplayer1Player1Win,<Qplayer1Player2Win,<Qplayer1Player2NextLevel,<Qplayer1OutOfTime,<Qplayer1Player2WinMaxLevel
effectJmpTbl1Hi:    .byte >Qplayer1HitMid,>Qplayer1HitHigh,>Qplayer1HitLow,>Qplayer1LoseEnergy,>Qplayer1DrivenBack,>Qplayer1Score1,>Qplayer1Score1,>Qplayer1Score1,>Qplayer1NoEffect,>Qplayer1NoEffect,>Qplayer1BothLoseEnergy,>Qplayer1Player2Penalty,>Qplayer1Player1Penalty,>Qplayer1NoEffect,>Qplayer1Player1Win,>Qplayer1Player2Win,>Qplayer1Player2NextLevel,>Qplayer1OutOfTime,>Qplayer1Player2WinMaxLevel
effectJmpTbl2Lo:    .byte <Qplayer2Score1,<Qplayer2Score1,<Qplayer2Score1,<Qplayer2NoEffect,<Qplayer2NoEffect,<Qplayer2HitMid,<Qplayer2HitHigh,<Qplayer2HitLow,<Qplayer2LoseEnergy,<Qplayer2DrivenBack,<Qplayer2BothLoseEnergy,<Qplayer2Player2Penalty,<Qplayer2Player1Penalty,<Qplayer2NoEffect,<Qplayer2Player1Win,<Qplayer2Player2Win,<Qplayer2Player2NextLevel,<Qplayer2OutOfTime,<Qplayer2Player2WinMaxLevel
effectJmpTbl2Hi:    .byte >Qplayer2Score1,>Qplayer2Score1,>Qplayer2Score1,>Qplayer2NoEffect,>Qplayer2NoEffect,>Qplayer2HitMid,>Qplayer2HitHigh,>Qplayer2HitLow,>Qplayer2LoseEnergy,>Qplayer2DrivenBack,>Qplayer2BothLoseEnergy,>Qplayer2Player2Penalty,>Qplayer2Player1Penalty,>Qplayer2NoEffect,>Qplayer2Player1Win,>Qplayer2Player2Win,>Qplayer2Player2NextLevel,>Qplayer2OutOfTime,>Qplayer2Player2WinMaxLevel
