.macro default_close_stuff(player) { 
	.if (player==1) {
		jmp (defaultCloseSwitch) 
	} else {
		jmp default_close_stuff2
	}
}

.macro playerLoseEnergy(player) {
	queueBlinkAnimation(player)
	//TODO: energy <0 stunned
	.var energy
	.if(player ==1){
		.eval energy = energy1 
	}else {
		.eval energy = energy2
	}
 	lda energy
	sec
	sbc #$01
	bcs !+
	lda #0
!:	sta energy
	default_close_stuff(player)
}				

.macro playerHitMid(player){
	queueAnimation(List().add(Animation(hithigh2,4),  Animation(hitmid3,4),	Animation(hithigh3,4)),
								PosChange(0,3),2,player )
	default_close_stuff(player)
}
.macro playerHitHigh(player){
	queueAnimation(List().add(Animation(hithigh1,4),  Animation(hithigh2,4),	Animation(hithigh3,4)),
								PosChange(0,3),2,player )	
	default_close_stuff(player)
}
.macro playerHitLow(player){
	queueAnimation(List().add(Animation(hitmid1,4),  Animation(hitmid2,4),	Animation(hitmid3,4)),
								PosChange(0,3),2,player )	
	default_close_stuff(player)
}


.macro playerPenalty(player) {
		queueAnimation(List().add(Animation(penalty,8)),PosChange(0,3),2,player )
		default_close_stuff(player)
}
.macro playerNeutralWhilePenalty(player) {
		queueAnimation(List().add(Animation(neutral,7)),PosChange(0,3),2,player )
		default_close_stuff(player)
}

.macro playerResetPassivity(player){
	lda #0
	.if (player == 1) {
		sta passivityCount1
	}else {
		sta passivityCount2
	}
	default_close_stuff(player)
}


player1Ready:	.byte 1
player2Ready:	.byte 1

setEffect2: {
	//first turn off player interaction
		setJumpTo(after_move2a, default_close_stuff2)
		lda #0
		sta doHitTest	//turn off hittest to prevent loop of hits
		lda #0			//indication player 2 is not interactive
		sta doPassivityCheck
		sta player2Ready
		ldx currentEffect2	
		lda effectJmpTbl2Lo,x	
		sta se+1	
		lda effectJmpTbl2Hi,x	
		sta se+2	
se: 	jmp default_close_stuff2 					
}		


setEffect1: {
	//first turn off ai/ player1 interaction
		lda noInteractionValue
		sta afterMoveSwitch //defaultCloseSwitch
		lda noInteractionValue + 1
		sta afterMoveSwitch + 1 //defaultCloseSwitch + 1

		lda #0
		sta doHitTest	//turn off hittest to prevent loop of hits
		sta doPassivityCheck
		lda #0			//indication player 1 is not interactive
		sta player1Ready
		ldx currentEffect1	
		lda effectJmpTbl1Lo,x	
		sta seteffect+1	
		lda effectJmpTbl1Hi,x	
		sta seteffect+2	
seteffect: jmp $ffff//default_close_stuff 					
}		

Qplayer1HitMid:	
	queueSteps(1, List().add(player1HitMid, player1ToStartPos, player1WaitForPlayer2, reactivate_ai))	
Qplayer2HitMid:	
	queueSteps(2, List().add(player2HitMid, player2ToStartPos, player2WaitForPlayer1, reactivate_player2joy))	
Qplayer1HitLow:	
	queueSteps(1, List().add(player1HitLow, player1ToStartPos, player1WaitForPlayer2,reactivate_ai))
Qplayer2HitLow:	
	queueSteps(2, List().add(player2HitLow, player2ToStartPos, player2WaitForPlayer1, reactivate_player2joy))	
Qplayer1HitHigh:	
	queueSteps(1, List().add(player1HitHigh, player1ToStartPos, player1WaitForPlayer2,reactivate_ai))	
Qplayer2HitHigh:	
	queueSteps(2, List().add(player2HitHigh, player2ToStartPos, player2WaitForPlayer1, reactivate_player2joy))	

//increasing score can lead to winning animation interrupting previous animation, so should be late AFTER playerWait
Qplayer1Score1: 
	queueSteps(1, List().add(player1ToStartPos, player1WaitForPlayer2,increaseScore1, reactivate_ai))
Qplayer2Score1: 
	queueSteps(2, List().add(player2ToStartPos, player2WaitForPlayer1,increaseScore2, reactivate_player2joy))

Qplayer1LoseEnergy: queueSteps(1, List().add(player1LoseEnergy, reactivate_ai))
Qplayer2LoseEnergy: queueSteps(2, List().add(player2LoseEnergy, reactivate_player2joy))

Qplayer1DrivenBack:		
	queueSteps(1, List().add(player1DrivenBack, reactivate_ai))		
Qplayer2DrivenBack:		
	queueSteps(2, List().add(player2DrivenBack, reactivate_player2joy))		

Qplayer1NoEffect: 
	queueSteps(1, List().add(reactivate_ai))
Qplayer2NoEffect: 
	queueSteps(2, List().add(reactivate_player2joy))

Qplayer1OutOfTime:
	queueSteps(1,List().add(drawZero, drawZero, drawZero, drawZero,drawZero, clearZero_p1, timeOutResult1))
Qplayer2OutOfTime:
	queueSteps(2,List().add(clearZero, clearZero, clearZero, clearZero, clearZero, clearZero,timeOutResult2))
		
	
Qplayer1BothLoseEnergy:

Qplayer2BothLoseEnergy:

Qplayer1Player1Penalty:
	queueSteps(1, List().add(player1Penalty, player1ResetPassivity,player1Neutral,player1ToStartPos,  player1WaitForPlayer2,reactivate_ai))
Qplayer2Player1Penalty:	
	queueSteps(2, List().add(player2Neutral, increaseScore2, player2ResetPassivity, player2ToStartPos, player2WaitForPlayer1,reactivate_player2joy))
Qplayer1Player2Penalty:
	queueSteps(1, List().add(player1Neutral, increaseScore1, player1ResetPassivity, player1ToStartPos,  player1WaitForPlayer2,reactivate_ai))
Qplayer2Player2Penalty:	
	queueSteps(2, List().add(player2Penalty, player2ResetPassivity,player2Neutral, player2ToStartPos, player2WaitForPlayer1,reactivate_player2joy))
	
Qplayer2Player2WinMaxLevel:
{
	queueSteps(2, List().add(player2ToStartPos, player2WaitForPlayer1, do_bow2,startDelayer2, animationDelayer2, clearFrame2,clearLines2, printEndY,printEndU, printEndI,waitForJoystick2ToRestart))
	//queueSteps(2, List().add(clearLines1, clearLines2, printEndY,printEndO,  printEndU,printEndW, printEndI,printEndN,waitForJoystick2ToRestart))
}
Qplayer1Player2WinMaxLevel:
{
	queueSteps(1, List().add(player1ToStartPos,  player1WaitForPlayer2,do_bow1, startDelayer1, animationDelayer1, clearFrame1,  clearLines1, printEndO,  printEndW, printEndN,waitForJoystick1ToRestart))
	//queueSteps(2, List().add(clearLines1, clearLines2, printEndY,printEndO,  printEndU,printEndW, printEndI,printEndN,waitForJoystick2ToRestart))
}

Qplayer1Player2Win:
{
	lda #0	
	sta doCountDown
	lda playerCount
	cmp #1
	beq !+
	jmp twoPlayer
!:	//break()
	queueSteps(1, List().add(player1ToStartPos,  player1WaitForPlayer2,do_bow1, startDelayer1, animationDelayer1, clearFrame1, clearLines1, printLevelL1, printLevelV,  printLevelL2,waitForJoystick1ToRestartAfterPlayer2Win,  clearLines1a,reactivate_ai))
twoPlayer:
	queueSteps(1, List().add(player1ToStartPos,  player1WaitForPlayer2,do_bow1, startDelayer1, animationDelayer1, waitForJoystick1ToRestart))
}



Qplayer2Player2Win:
{
	lda playerCount
	cmp #1
	beq !+
	jmp twoPlayer
!:	ldx computerLevel
	inx
	cpx #MAX_LEVEL
	bne !+
	jmp endAll
!:	stx computerLevel
	queueSteps(2, List().add(player2ToStartPos, player2WaitForPlayer1, do_bow2, startDelayer2, animationDelayer2, clearFrame2, clearLines1, printLevelE1, printLevelE1, printLevelE2, printLevelNumber,waitForJoystick2ToRestartAfterWin, clearLines1a,reactivate_player2joy))
endAll:	
	//break()
	lda #0
	sta doDrawEnergy
	lda #19
	setEffectMacro()
	default_close_stuff(2) 
twoPlayer:
	queueSteps(2, List().add(player2ToStartPos, player2WaitForPlayer1, do_win2, startDelayer2, animationDelayer2, waitForJoystick2ToRestart))
}

	
Qplayer1Player1Win:	
{	
	lda #0	
	sta doCountDown
	lda playerCount
	cmp #1
	bne !+
	jmp queueCompPlayerWin
!:	queueSteps(1, List().add(player1ToStartPos,  player1WaitForPlayer2,do_win1, startDelayer1, animationDelayer1, waitForJoystick1ToRestart))

queueCompPlayerWin:

	queueSteps(1, List().add(player1ToStartPos,  player1WaitForPlayer2,do_win1, startDelayer1, animationDelayer1, clearFrame1, stopDraw1,clearLines1, printG, printA, printM, printE))
}	

Qplayer2Player1Win:	
{	
	lda playerCount
	cmp #1
	bne !+
	jmp queueCompPlayerWin
!:	queueSteps(2, List().add(player2ToStartPos, player2WaitForPlayer1, do_bow2, startDelayer2, animationDelayer2, waitForJoystick2ToRestart))

queueCompPlayerWin:
	queueSteps(2, List().add(player2ToStartPos,  player2WaitForPlayer1,do_bow2, startDelayer2, animationDelayer2, clearFrame2, stopDraw2,clearLines2, printO, printV,printE2, printR, waitForJoystick2ToRestart))
}	
	
Qplayer1Player2NextLevel:	
	queueSteps(1, List().add(player1ToStartPos,  player1WaitForPlayer2,do_bow1, reactivate_ai))
Qplayer2Player2NextLevel:	
	queueSteps(2, List().add(player2ToStartPos, player2WaitForPlayer1, do_bow2, reactivate_player2joy))
//==================================== STEPS =====================================
player1Penalty: playerPenalty(1)
player2Penalty: playerPenalty(2)
player1Neutral: playerNeutralWhilePenalty(1)
player2Neutral: playerNeutralWhilePenalty(2)
player1ResetPassivity: playerResetPassivity(1) 
player2ResetPassivity: playerResetPassivity(2)

player1HitMid: playerHitMid(1)
player2HitMid: playerHitMid(2)
player1HitLow: playerHitLow(1)
player2HitLow: playerHitLow(2)
player1HitHigh: playerHitHigh(1)
player2HitHigh: playerHitHigh(2)

player1LoseEnergy: playerLoseEnergy(1)
player2LoseEnergy: playerLoseEnergy(2)

player1DrivenBack:
	checkedPosChange(1, 1)
	jmp (defaultCloseSwitch)
player2DrivenBack:
	checkedPosChange(2, 1)
	jmp default_close_stuff2
				
				
reactivate_ai:
		lda #1
		sta doHitTest
		sta doPassivityCheck
		
		lda #0
		sta stunned1
		sta passivityCount1
		
		lda interactionValue
		sta afterMoveSwitch 
		lda interactionValue + 1
		sta afterMoveSwitch + 1
		
		jmp (defaultCloseSwitch)			

reactivate_player2joy:
		lda #1
		sta doHitTest
		sta doPassivityCheck
		
		lda #0
		sta stunned2
		sta passivityCount2		
		setJumpTo(after_move2, player2JoystickRead)
		jmp default_close_stuff2			

do_bow1:	queueAnimation(List().add(Animation(bow1,5),  Animation(bow2,10),	Animation(bow1,2)),
								PosChange(0,3),2,1 )						
		jmp (defaultCloseSwitch)

	

do_win1:	lda #0	
			sta doCountDown	
		queueAnimation(List().add(Animation(win1,3), Animation(win2,3)),
								PosChange(0,3),1,1 )						
		jmp (defaultCloseSwitch)
	
		
player1ToStartPos:
{
		lda pos1
		beq end
	
		queueAnimation(List().add(Animation(neutral,1), Animation(walk1,2),  Animation(neutral,1))
					, PosChange(-1,3),0,1 )						
	//unlike most steps this is step could be repeated indefinitely so stepcounter should not decrease											
		inc stepqueuecounter_1						
		jmp (defaultCloseSwitch)					
end:		
		lda #neutral
		sta frame1
		jmp (defaultCloseSwitch)
}

do_bow2:	queueAnimation(List().add(Animation(bow1,5),  Animation(bow2,10),	Animation(bow1,2)),
								PosChange(0,3),2,2 )						
		jmp default_close_stuff2

do_win2:	
			lda #0	
			sta doCountDown
		queueAnimation(List().add(Animation(win1,3), Animation(win2,3)),
								PosChange(0,3),2,2 )						
		jmp default_close_stuff2


player2ToStartPos:
{
		lda pos2
		cmp #startPos2
		beq endp2sp
	
		queueAnimation(List().add(Animation(neutral,1), Animation(walk1,2),  Animation(neutral,1))
					, PosChange(1,3),0,2 )						
	//unlike most steps this is step could be repeated indefinitely so stepcounter should not decrease											
		inc stepqueuecounter_2						
		jmp default_close_stuff2					
	
endp2sp:		
		lda #neutral
		sta frame2
		jmp default_close_stuff2
}

waitForJoystick1ToRestartAfterPlayer2Win: {
		lda player2Ready
		bne !+
		inc stepqueuecounter_1	
!:		jmp (defaultCloseSwitch)
}

//only wait for player 1 if is two player game
waitForJoystick1ToRestart:
{
		lda joystick1
		and #%00010000
		bne endwait
		lda #INTRO_PAUSE_TIME
		sta introPause
		jmp restartIntro								
endwait:		
	//unlike most steps this is step could be repeated indefinitely so stepcounter should not decrease		
		inc stepqueuecounter_1	
		jmp (defaultCloseSwitch)
}

waitForJoystick2ToRestart:
{		
		lda joystick2
		and #%00010000
		bne endwait
		lda #INTRO_PAUSE_TIME
		sta introPause
		jmp restartIntro								
endwait:		
		//unlike most steps this is step could be repeated indefinitely so stepcounter should not decrease		
		inc stepqueuecounter_2	
		jmp default_close_stuff2
}

waitForJoystick2ToRestartAfterWin:
{		
		lda joystick2
		and #%00010000
		bne endwait
		lda playerCount
		cmp #1
		beq endOnePlayerGame
		lda #INTRO_PAUSE_TIME
		sta introPause
		jmp restartIntro								
endwait:		
		//unlike most steps this is step could be repeated indefinitely so stepcounter should not decrease		
		inc stepqueuecounter_2	
		jmp default_close_stuff2

endOnePlayerGame:
		//ldx computerLevel
		//inx
		//cpx #1//MAX_LEVEL
		//bne levelUp
		//jmp Qplayer2Player2WinMaxLevel
levelUp:		
		//stx	computerLevel	
		
		lda #0
		sta score1
		sta score2
		sta energyRegainCount1
		sta energyRegainCount2
		sta passivityCount1
		sta passivityCount2
		sta stunned1
		sta stunned2
		sta subTimer

		ldx computerLevel
		lda timerHiPerLevel,x
		sta timerHi
		lda timerLoPerLevel,x
		sta timerLo
	
		lda #1
		sta doCountDown
		sta doDrawEnergy
		sta doDrawPlayers                           
		//sta playerCount	
		
		lda #startPos1
		sta pos1
		sta pos1Old
		lda #startPos2
		sta pos2
		sta pos2Old
		lda #startPos2-startPos1-1
		sta dist
			
		ldy #maxEnergy		
		sty energy1		
		sty energy2		
		lda #0	
		sta score1	
		sta score2	
		
		lda #neutral
		sta frame1
		sta frame2

		lda #STY_ABS
		.for(var i = 0; i<12; i++) {
			sta zpStart + i
		}

		lda #1
		sta player2Ready
		
		jmp default_close_stuff2		
		 
}



player1WaitForPlayer2:
{
		lda player1Ready
		and player2Ready
		bne endp1sp
		lda #1
		sta player1Ready
	//unlike most steps this is step could be repeated indefinitely so stepcounter should not decrease											
		inc stepqueuecounter_1						
	
endp1sp:		
		jmp (defaultCloseSwitch)
}

animationDelayer1: animationDelayerMacro(1)
animationDelayer2: animationDelayerMacro(2)

startDelayer1: startDelayer(1)
startDelayer2: startDelayer(2)

.macro startDelayer(player){
	lda #0
	ldx #0	//reset delay value
	.if(player == 1) {
		sta player1Ready
		stx animationDelay1	
	}else {
		sta player2Ready
		stx animationDelay2
	} 
	default_close_stuff(player)
}

.macro animationDelayerMacro(player){
	.var animationDelay
	.var stepqueuecounter
	.var playerReady
	.var otherPlayerReady
	.if(player == 1) {
		.eval animationDelay = animationDelay1
		.eval stepqueuecounter = stepqueuecounter_1
		.eval playerReady = player1Ready
		.eval otherPlayerReady = player2Ready
	}else {
		.eval animationDelay = animationDelay2
		.eval stepqueuecounter = stepqueuecounter_2
		.eval playerReady = player2Ready
		.eval otherPlayerReady = player1Ready
	} 
	ldx animationDelay	
	inx	
	cpx #STANDARD_DELAY_VALUE	
	bne end
	lda #1
	sta playerReady
	lda otherPlayerReady
	bne !+			//not zero: player is ready
	inc stepqueuecounter
!:	default_close_stuff(player)
end:	
	inc stepqueuecounter	
	stx animationDelay	
	default_close_stuff(player)
}

player2WaitForPlayer1:
{
		lda player1Ready
		and player2Ready
		bne endp2sp
		lda #1
		sta player2Ready
	//unlike most steps this is step could be repeated indefinitely so stepcounter should not decrease											
		inc stepqueuecounter_2						
	
endp2sp:		
		jmp default_close_stuff2
}

.macro printLetter(pos, letterNumber, player){
	printLetterWithColor(pos,letterNumber, player, true)
}

.macro printLetterWithColor(pos, letterNumber, player, color){
	.if(color) {
		lda #STA_ABS
	}else {
		lda #STX_ABS
	}
	sta letterColor
	lda #pos
	sta writePos
	ldx #letterNumber
	jsr writeLetter
	.if(player ==1 ){
		jmp (defaultCloseSwitch)
	}else {
		jmp default_close_stuff2
	}
}
printEndY: printLetter(13,24,1)
printEndO: printLetter(17,14,2)
printEndU: printLetter(20,20,1)
printEndW: printLetter(13+120,22,2)
printEndI: printLetter(17+120,8,1)
printEndN: printLetter(19+120,13,2)

printG: printLetter(12,6,1)
printA: printLetter(15,0,1)
printM: printLetter(18,12,1)
printE: printLetter(22,4,1)
printO: printLetter(12+120,14,2)
printV: printLetter(15+120,21,2)
printE2: printLetter(19+120,4,2)
printR: printLetter(22+120,17,2)

printDrawD: printLetter(12,3,1)
printDrawR: printLetter(15,17,1)
printDrawA: printLetter(18,0,1)
printDrawW: printLetter(21,22,1)

printLevelL1: printLetterWithColor(12,11,1,true)
printLevelE1: printLetterWithColor(13,4,2,false)
printLevelV: printLetterWithColor(15,21,1,true)
printLevelE2: printLetterWithColor(18,4,2,false)
printLevelL2: printLetterWithColor(20,11,1,true)
printLevelNumber: {
	ldx computerLevel
	inx
	lda tableTen,x
	tax
	.for(var r=0; r<5;r++){
		.for(var c=0; c<2;c++){
			lda numberdefs+r*2+c, x
			sta.z zpStart + r*12 + 22 + c  
		} 
	} 
	jmp default_close_stuff2
}


stopDraw1:
	lda #0
	sta doDrawPlayers
	sta doCountDown
	sta doDrawEnergy

	jmp (defaultCloseSwitch)
stopDraw2:
	lda #0
	sta doDrawPlayers
	sta doCountDown
	sta doDrawEnergy
	jmp default_close_stuff2
	
clearFrame1:
	lda #stun2
	sta frame1
	jmp (defaultCloseSwitch)
clearFrame2:
	lda #stun2
	sta frame2
	
	//hack
	lda #0
	sta player2Ready
	
	jmp default_close_stuff2

clearLines1:
	lda #STY_ABS
	.for(var l=0; l<11; l++){
		clearLine(l)
	}
	lda #RED
	.for(var l=1; l<7; l++){
		sta colorStageA + l
	}
	jmp (defaultCloseSwitch)
clearLines2:
	lda #STY_ABS
	.for(var l=11; l<21; l++){
		clearLine(l)
	}
	lda #GREEN
	.for(var l=11; l<17; l++){
		sta colorStageA + l
	}
	jmp default_close_stuff2

clearLines1a:
	lda #STY_ABS
	.for(var l=0; l<11; l++){
		clearLine(l)
	}
	lda #BROWN
	.for(var l=1; l<7; l++){
		sta colorStageA + l
	}
	jmp (defaultCloseSwitch)

	
.macro clearLine(line){
	.for (var i=0; i<12; i++){
		sta zpStart + line*12 +i
	}
}

clearZero_p1: {
	lda #STY_ABS
	.for(var r=0; r<5;r++){
		.for(var c=0; c<2;c++){
			sta.z zpStart + r*12 + [15*12] + c + 3
			sta.z zpStart + r*12 + [15*12] + c + 3 + 4  
		} 
	} 
	default_close_stuff(1)
}


drawZero: {
	lda #STX_ABS
	.for(var r=0; r<5;r++){
		.for(var c=0; c<2;c++){
			sta.z zpStart + r*12 + [15*12] + c + 3
			sta.z zpStart + r*12 + [15*12] + c + 3 + 4  
		} 
	} 
	default_close_stuff(1)
}

clearZero: {
	lda #STY_ABS
	.for(var r=0; r<5;r++){
		.for(var c=0; c<2;c++){
			sta.z zpStart + r*12 + [15*12] + c + 3
			sta.z zpStart + r*12 + [15*12] + c + 3 + 4  
		} 
	} 
	default_close_stuff(2)
}


increaseScore1: increaseScore(1)
increaseScore2: increaseScore(2)

timeOutResult1:
{
	//compare scores, and depending on that, player 2 wins, player 1 wins or 
	// with a draw it is restart same level with one player or message 'draw'
	lda score2
	cmp score1
	beq player1Draw
	blt player1WinsAtTimeOut
	jmp Qplayer1Player2Win
player1WinsAtTimeOut:	
	 jmp Qplayer1Player1Win
player1Draw:	
		lda playerCount
		cmp #1
		beq player1WinsAtTimeOut
		queueSteps(1, List().add(player1ToStartPos,  player1WaitForPlayer2,do_bow1, clearFrame1, stopDraw1,clearLines1, printDrawD, printDrawA, waitForJoystick1ToRestart))	
}
			
timeOutResult2:
{
	//compare scores, and depending on that, player 2 wins, player 1 wins or 
	// with a draw it is restart same level with one player or message 'draw' 
	lda score2
	cmp score1
	beq player2Draw
	blt player1WinsAtTimeOut
	jmp Qplayer2Player2Win
player1WinsAtTimeOut:	
	 jmp Qplayer2Player1Win
player2Draw:	
		lda playerCount
		cmp #1
		beq player1WinsAtTimeOut
		queueSteps(2, List().add(player2ToStartPos,  player2WaitForPlayer1,do_bow2, clearFrame2, stopDraw2,clearLines2, printDrawR, printDrawW, waitForJoystick2ToRestart))	
}

