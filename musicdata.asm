.align $100
vibcodetable1:		.fill 16, ADC_IMM
					.fill 16, SBC_IMM
					.fill 16, ADC_IMM
					.fill 16, SBC_IMM
					.fill 16, ADC_IMM
					.fill 16, SBC_IMM
					.fill 16, ADC_IMM
					.fill 16, SBC_IMM
					.fill 16, ADC_IMM
					.fill 16, SBC_IMM
					.fill 16, ADC_IMM
					.fill 16, SBC_IMM
					.fill 16, ADC_IMM
					.fill 16, SBC_IMM		
					.fill 16, ADC_IMM
					.fill 16, SBC_IMM
vibcodetable2:		.fill 16, CLC
					.fill 16, SEC
					.fill 16, CLC
					.fill 16, SEC
					.fill 16, CLC
					.fill 16, SEC
					.fill 16, CLC
					.fill 16, SEC
					.fill 16, CLC
					.fill 16, SEC
					.fill 16, CLC
					.fill 16, SEC
					.fill 16, CLC
					.fill 16, SEC
					.fill 16, CLC
					.fill 16, SEC
//the previous two tables are 256 bytes long, so this is aligned to $100

/*					TAKE CARE! C#1 is changed specially for sync note on voice 3 to make voice 2 vibrate like a sort of shamisen  

*/

FreqTablePalLo:
				//		0	1	2  3   4   5   6   7   8   9   A   B
     	        //     C   C#  D   D#  E   F   F#  G   G#  A   A#  B
                .byte $17,$c7,$39,$4b,$5f,$74,$8a,$a1,$ba,$d4,$f0,$0e  // 1
                .byte $2d,$4e,$71,$96,$be,$e8,$14,$43,$74,$a9,$e1,$1c  // 2
                .byte $5a,$9c,$e2,$2d,$7c,$cf,$28,$85,$e8,$52,$c1,$37  // 3
                .byte $b4,$39,$c5,$5a,$f7,$9e,$4f,$0a,$d1,$a3,$82,$6e  // 4
                .byte $68,$71,$8a,$b3,$ee,$3c,$9e,$15,$a2,$46,$04,$dc  // 5
                .byte $d0,$e2,$14,$67,$dd,$79,$3c,$29,$44,$8d,$08,$b8  // 6
                .byte $a1,$c5,$28,$cd,$ba,$f1,$78,$53,$87,$1a,$10,$71  // 7
                .byte $42,$89,$4f,$9b,$74,$e2,$f0,$a6,$0e,$33,$20,$ff  // 8

FreqTablePalHi:
	         	//     C   C#  D   D#  E   F   F#  G   G#  A   A#  B
                .byte $01,$00,$01,$01,$01,$01,$01,$01,$01,$01,$01,$02  // 1
                .byte $02,$02,$02,$02,$02,$02,$03,$03,$03,$03,$03,$04  // 2
                .byte $04,$04,$04,$05,$05,$05,$06,$06,$06,$07,$07,$08  // 3
                .byte $08,$09,$09,$0a,$0a,$0b,$0c,$0d,$0d,$0e,$0f,$10  // 4
                .byte $11,$12,$13,$14,$15,$17,$18,$1a,$1b,$1d,$1f,$20  // 5
                .byte $22,$24,$27,$29,$2b,$2e,$31,$34,$37,$3a,$3e,$41  // 6
                .byte $45,$49,$4e,$52,$57,$5c,$62,$68,$6e,$75,$7c,$83  // 7
                .byte $8b,$93,$9c,$a5,$af,$b9,$c4,$d0,$dd,$ea,$f8,$ff  // 8                
//page+80
.align $100		
//position in pattern for voice 1,2,3
patternpos:		.byte 0,0,0
//value which is added each frame to patternpos changed depending on event (val>0) or no event (val=0, no change)
patternposadd:	.byte 2,2,2
currentPattern: .byte 0,0,0
//temporary values used during setup of a new event
val1:			.byte 255,255,255
val2:			.byte 255,255,255
val3:			.byte 255,255,255
//length before next event , initial value = 1 so a new event is triggered immediately
countTillNextEvent:	.byte 1,1,1
currentSong:	.byte 0

.var musiclength = *
.import source "generatedmusicdata.asm" 
.eval musiclength = *- musiclength
.print "music length:" + musiclength 
	
//				voice  1,  2,  3
.align $03
ad:				.byte $00,$00,$00
sr:				.byte $f0,$00,$00
pulselo:		.byte $80,$00,$00
pulsehi:		.byte $04,$00,$00
wave:			.byte $41,$41,$41
currentnote:	.byte $00,$00,$00
freqlo:			.byte $80,$00,$00
freqhi:			.byte $20,$00,$00
vibrcount:		.byte $00,$00,$00
plsvibrcount:	.byte $00,$00,$00
//.assert "page not crossed with registertables" + mod([plsvibrcount+3],256) + ": " + mod(ad,256) , floor([plsvibrcount+3]/256),floor(ad/256)

/**	============================
		BECAUSE OF INDIRECT JUMP BUG
		JUMP ADDRESS MUST NOT BE IN TWO DIFFERENT PAGES
		THIS IS EASILY ACHIEVED BY ALIGNING TO AN EVEN ADDRESS
	===========================**/		
//.align 2
.align $100		
//eventnr				0			1				2				3				4			5			6				7				8 (no event)			
jmptable1_1: .word setupnoteon1, setupnoteoffvib1,setupnoteon1,  setupinstr1_1,setupnoteon1,setupslide1,   setupnoteon1, setuppattern1_1, setupnone1_1 
jmptable1_2: .word setupnone1_2, setupvibr1_2,    setupnone1_2,  setupinstr1_2,setupnone1_2,setupnone1_2 , setupnone1_2, setuppattern1_2, setupnone1_2 

jmptable2_1: .word setupnoteon2, setupnoteoffvib2,setupnoteon2,  setupinstr2_1,setupnoteon2,setupslide2,   setupnoteon2, setuppattern2_1, setupnone2_1 
jmptable2_2: .word setupnone2_2, setupvibr2_2,    setupnone2_2,  setupinstr2_2,setupnone2_2,setupnone2_2 , setupnone2_2, setuppattern2_2, setupnone2_2 

jmptable3_1: .word setupnoteon3, setupnoteoffvib3,setupnoteon3,  setupinstr3_1,setupnoteon3,setupslide3,   setupnoteon3, setuppattern3_1, setupnone3_1 
jmptable3_2: .word setupnone3_2, setupvibr3_2,    setupnone3_2,  setupinstr3_2,setupnone3_2,setupnone3_2 , setupnone3_2, setuppattern3_2, setupnone3_2 

jmptableend:
.assert "page not crossed with jumptables", floor([jmptableend-1]/256),floor(jmptable1_1/256)

//			speed          0	1	 2
vibratospeedtable1: .byte NOP, NOP,INX  
vibratospeedtable2: .byte NOP, INX, INX 
vibratospeedtabeleend: 		
.assert "page not crossed with vibratospeedtable", floor([vibratospeedtabeleend-1]/256),floor(vibratospeedtable1/256)

//frame to chinese scale convert table
//frameToScaleConvert:    .byte 1,3,6,8,10,13,15,18,20,22,25,27,30,32,34,37,39,42,44,46,49,51,54,56,58,61,63,66,68,70,73,75,78,80,82,85,87,90,92,94,30,31,32,33,34,35,36,37,38,39
//frame to chinese scale convert table
frameToScaleConvert:    .byte 37,39,42,44,46,49,51,54,56,58,37,39,42,44,46,49,51,54,56,58,37,39,42,44,46,49,51,54,56,58,37,39,42,44,46,49,51,54,56,58,0,31,32,33,34,35,36,37,38,39




