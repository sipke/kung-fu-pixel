.import source "firstsongvars.asm"

.macro Event(type,value, length){
	.if (type=="nonote"){
		//.print "nonote val" + value + " result" +[[value*2] & 7] + " length:" + length
		.if(value > 63) {.error "value too large for no note" } 
		.if(length<2) {.error "length should be larger than 2, because of note off. Use notetied for note without noteoff" } 
		.byte 0 ,length-2, 129, 2
	}
	.if (type=="nonotetied"){
		//.print "nonote val" + value + " result" +[[value*2] & 7] + " length:" + length
		.if(value > 63) {.error "value too large for no note" } 
		.if(length<2) {.error "length should be larger than 2, because of note off. Use notetied for note without noteoff" } 
		.byte 0 ,length
	}
	.if (type=="noteon"){
		//.print "noteon val" + value + " result" +[[value*2] & 7] + " length:" + length
		.if(value > 127) {.error "value too large for note" } 
		.if(length<2) {.error "length should be larger than 2, because of note off. Use notetied for note without noteoff" } 
		.byte value*2,length-2, 129, 2
	}
	.if (type=="notetied"){
		//.print "noteon val" + value + " result" +[[value*2] & 7] + " length:" + length
		.if(value > 127) {.error "value too large for note" } 
		.byte value*2,length
	}
	.if (type=="noteoff"){
		//.print "noteoff val" + value + "result 129" + "length:" + length
		 .byte 129, length 	
	}
	.if (type=="noteofftied"){
		//.print "noteoff val" + value + "result 129" + "length:" + length
		 .byte 129+64, length, value 	
	}
	.if(type.size()>5){
		.if(type.substring(0,5)=="instr"){
			.var instrnumber = type.substring(5,type.size()).asNumber()
				.if(instrnumber > 31) {.error "value too large for instrument" }
				.if(value > 127) {.error "value too large for note" }
				//.print "instr val" + instrnumber + " eventnr" +[[[instrnumber*8]+3] & 7] + " notevalue: " +  value + "length:" + length
				.byte [instrnumber*8]+3, length-2,value, 129, 2
		}
	}
	.if(type.size()>5){
		.if(type.substring(0,5)=="instt"){
			.var instrnumber = type.substring(5,type.size()).asNumber()
				.if(instrnumber > 31) {.error "value too large for instrument" }
				.if(value > 127) {.error "value too large for note" }
				//.print "instr val" + instrnumber + " eventnr" +[[[instrnumber*8]+3] & 7] + " notevalue: " +  value + "length:" + length
				.byte [instrnumber*8]+3, length,value
		}
	}
	.if(type=="slideup"){
		.if(value > 15) {.error "value too large for slide" }
		//.print "slideup val" + [value*16] + " eventnr" +[[[value*16]+5] & 7] + " result: " +  [[value*16] +5]+ "length:" + length
		.byte [value*16]+5, length
	}
	.if(type=="slidedown"){
		.if(value > 15) {.error "value too large for slide" }
		//.print "slidedown val" + [value*16] + " eventnr" +[[[value*16]+13] & 7] + " result: " +  [[value*16] +9]+ "length:" + length
		.byte [value*16]+13, length	
	}
	.if(type.size()>4){
		.if(type.substring(0,4)=="vibr"){
			.if(value > 31) {.error "value too large for vibrato" }
			.var vibratospeed = type.substring(4,type.size()).asNumber()
			.if (vibratospeed<0) { .error "illegal vibrato speed" }
			.if (vibratospeed>2) { .error "illegal vibrato speed"}
			//.print "vibr val" + value + " eventnr" +[[[value*8]+1] & 7] + " result: " +  [[value*8] +1]+ "length:" + length + " speed:" + vibratospeed
			.byte [value*8]+1, length, vibratospeed
		}
	}
	.if (type=="end"){
		//.print "end val" + value + " eventnr" +[[[value*8]+7] & 7] + " result: " +  [[value*8] +7]+ "length:" + length
		.byte 7
	}
	//.error "unknown event:" + type +  value + length
}
//-------------------------------------
//   all odd line codes must be 66 cycles! 
//--------------------------------------
//	 no use of y register allowed unless first put on stack and retrieved before start of even line 
//   because the y register contains the background color. 
// -------------------------------------- 

.function differentpages(address1, address2){
	.return ![floor(address1/256)==floor(address2/256)]
}

//==============================================
//			EVAL NEW EVENT
//==============================================
.macro soundline1(){
   			dec countTillNextEvent +voice	//6
			bne nonewevent					//2,3,4
			//new event
b1:			ldx patternpos	+ voice			//4
		//sm1
			lda firstpattern+1,x			//5
			sta countTillNextEvent +voice	//4 contains length till next event
		//sm2
			lda firstpattern+2,x			//5
			sta val3 +voice					//4 might not be used, if not used patternposadd =2, if used patternposadd =3
		//sm3
			lda firstpattern,x				//5
			sta val2 +voice					//4
			and #$07						//2 lower three bits of second value are the event type, bit 0 =0, always note on
			asl								//2 A times 2
			tax								//2 keep val in X
			clc								//2
			adc #<jumptable1				//2
			sta switchlocation1+1			//4
			txa								//2 retrieve A from X
			adc #<jumptable2				//2
			sta switchlocation2+1			//4
			nop								//2
			bit $01
			jmp nextscanline				//3
					
nonewevent: 			
			//9 or 10 of 66 cycles used + 9 at the end, compensate 48 or 47 cycles 
			.if(differentpages(b1, nonewevent)){
				//branch to other page
				.fill 22,NOP
				bit $01
			}else{
				//branch to same page
				.fill 24, NOP			
			}
			// no event so patternpos need not change
			lda #0							//2
			sta patternposadd + voice		//4
			
   			jmp nextscanline				//3
}

//==============================================
//		SLIDE NOTE SETUP
//==============================================
.macro setupslide(){	

									//5
		lda #NOP					//2
		sta scanlineaftervibrato - 7//4
		sta scanlineaftervibrato - 5//4
		lda #NOP					//2
		sta scanlineaftervibrato - 6//4

		lax val2 +voice				//4	
		and #240					//2 ignore lower 4 bits
		sta vibratoroutine + vibratoAmountOffset //+ 4		//4 dv2+1 vibrato/slide value.
		txa							//2
		and #%00001000				//2
		bne slidedown1				//2, 3 or 4

	//vibrato uses LDA ABS, X so with X an alternating pattern of ADC /SBC can be achieved
	//by changing it to LDA ABS and 'freezing' it at a pos in the vibratotable 
//EDIT NICE IDEA, BUT YOU CAN'T DO THAT IF YOU NEED CYCLE PERFECT ACCURACY!	
//LDA ABS = 4 cycles, LDA ABSX = 5  cycles	
//so change of plan: slide will substitute alle inx at the end of vib routine to NOP, so index is  never changed 	
	//the vibrato/slide routine will not be alternating between ADC and SBC but will be 'stuck' on a  ADC or SBC  
bb:									//37
	//set index on 0, which is first SBC, so it is stuck on increasing value
		lda #0 						//2
		sta vibrcount + voice		//4

		.fill 10, NOP
		jmp nextscanline			//3
		
slidedown1:
									//38 or 39
	//set index on 16, which is first SBC, so it is stuck on decreasing value 									
		lda #16 					//2
		sta vibrcount + voice		//4

		.fill 8, NOP

	.if (differentpages(bb, slidedown1)){
		nop
	}else{
		bit $01
	}
		jmp nextscanline			//3
}

//==============================================
//			NOTE ON SETUP 
//==============================================
.macro setupnoteon(){
										//5
			lda #0						//2
			sta vibratoroutine + vibratoAmountOffset //+ 4		//4 dv2+1 reset vibrato/slide value.
			lda val2  + voice			//4										
			beq noNote					//2 value 0 indicates a no note event	
bb:			lsr							//2  
			tax							//2 no note event is used at beginning of pattern to let note ring
noteOn:
			lda wave + voice			//4
			sta $d404 + voice*7			//4
			stx currentnote + voice		//4
			lda FreqTablePalLo,x		//5
			sta $d400	+ voice*7		//4
			sta freqlo  + voice			//4
			lda FreqTablePalHi,x		//5
			sta $d401	+ voice*7		//4
			sta freqhi + voice			//4
			
			.fill 3, NOP
			jmp doAni			//3
										 	
noNote:									//18 or 19
			.fill 20,NOP
			.if (differentpages(bb, noNote)){
				//						+1 from diff page
				nop
				nop
			}else{
				nop
				bit $01
			}
			jmp nextscanline
			
doAni:			
			//clear prev frame
			lda #0
			.for(var x=0; x<8; x++){
				.for(var y=0; y<12; y++){
					sta $d800+x+y*40 + 2 + voice*10
				}
			}
	//break()	
			lda val2 + voice
			cmp #45
			blt !+
			lsr
			lsr
			tax
			lda spritepointerlo,x
			sta $fc
			lda spritepointerhi,x
			sta $fd
			ldy #0
			lda #voice + 1
			sta sm1 + 1
loop:
			lda ($fc),y
			cmp #$fe
			bne !+
			lda #YELLOW
			sta sm1+1
			iny
			jmp loop		
!:			cmp #$ff					
			bne !+					
			iny					
			jmp end					
!:			tax
			lda newposlo + voice*144,x
			sta sm2 + 1
			lda newposhi+ voice*144,x
			sta sm2 + 2
			//clc
			//adc #voice*12
			ldx #0
sm1:		lda #BLACK	
sm2:		sta $d800,x	
			inx 	
			cpx #2	
			bne sm1	
			iny	
			jmp loop	
end:			
			jmp nextscanline			//3
spritenum:	.byte 0		
timesthree:	.fill 85,3*i		
}

//==============================================
//			NOTE OFF OR VIBRATO PART I SETUP 
//==============================================
.macro setupnoteoffvib(){  
										//5 				
			lda val2 +voice 			//4				
			bmi setupnoteoff1			//2,3,4				
bb:									
		//reset vibrato count
			lda #0 						//2
			sta vibrcount + voice		//4

		//code will be modified so lda will be indexed
		 	lda #INX					//2
			sta scanlineaftervibrato - 7//4(other two INX/NOP's will be set to NOP or INX depending on speed in second part)

			.fill 20, NOP	
			jmp nextscanline			//3
			
setupnoteoff1:		
										//12 or 13
 			cmp #129+64					//2
 			blt seno2						
//nonote tied 			
seno1: 										//2
 			//lda wave + voice			//4
			//and #254					//2	
			//sta $d404 + [voice*7]		//4
			ldx val3 +voice				//4
			lda FreqTablePalLo,x		//5
			sta $d400	+ voice*7		//4
			sta freqlo  + voice			//4
			lda FreqTablePalHi,x		//5
			sta $d401	+ voice*7		//4
			sta freqhi + voice			//4

			inc patternposadd + voice	//6
			.if (differentpages(bb, setupnoteoff1)){
									//	+ 1 from branch
				.fill 5, NOP			//10
			}else {
				.fill 4,NOP				//8
				bit $01					//3
			}
 			jmp nextscanline			//3
 			
 								//14,15 + 3 or 4
 										//17, 18, or 19
//noteoff 										
seno2:									
			lda wave + voice			//4
			and #254					//2	
			sta $d404 + [voice*7] 		//4
			
			.if (differentpages(bb, setupnoteoff1)){
				.if (differentpages(seno1, seno2)){
					//+2 from branches	= 32 cycles
					.fill 17, NOP
				}else {
					//+1 from branches = 31 cycles
					.fill 16,NOP
					bit $01
				}
			}else {
				.if (differentpages(seno1, seno2)){
					//+1 from branches	= 31 cycles
					.fill 16,NOP
					bit $01
				}else {
					//0 from branches = 30 cycles
					.fill 18, NOP
				}
				
			}

 			jmp nextscanline			//3
} 			

//==============================================
//		PATTERN CHANGE SETUP  PART I
//==============================================
.macro setuppattern1(){	
										//5 for earlier jpm(switch)
		//at the end of cycle patternpos will be added with 2, but new pattern so should start with 0-> 0-2=254										
 		lda #254						//2
 		sta patternpos + voice			//4
 		//this is one so next frame will trigger new event
 		//(frame starts with dec counttillnextevent_1, bne)  
 		lda #1							//2
 		sta  countTillNextEvent + voice //4
 		ldx  currentPattern + voice 	//4
 		inx								//2	
 		lda patterntablehi,x			//5
 										//total 28
		bne ab 							//2,3,4	
			
bb:		ldx #0							//4
 		lda patterntablehi,x			//5

 		sta sm1+2						//4
 		sta sm2+2						//4
 		sta sm3+2						//4
 		stx currentPattern + voice		//4
		.fill 6, NOP					//8
 		jmp nextscanline				//3

										//28 for previous part
										//3 or 4 for branch
ab: 	sta sm1+2						//4
 		sta sm2+2						//4
 		sta sm3+2						//4
 		stx currentPattern + voice		//4
 		
 		.if(differentpages(bb,ab)) {
 										//+1 from branch
			.fill 8, NOP
		}else{ 
			.fill 7, NOP
			bit $01
		}
				
 		jmp nextscanline				//3
	
}

//==============================================
//		NEW INSTRUMENT SETUP PART I
//==============================================
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//		MUST IMMEDIATELY BE FOLLOWED BY NEXT SCANLINE
//		NOT ENOUGH CYCLES FOR JUMP!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
.macro setupinstr1(){

									//5 for earlier jpm(switch)		
		ldx val2 +voice				//4

	//dynamic values must be stored in shadow registers to be read 
		lda instruments + 3,x		//5 pulsetablehi,x			/
		sta pulsehi +voice			//4

		lda instruments + 4,x		//5 pulsetablelo,x			//5
		sta pulselo +voice			//4

		lda instruments + 2,x		//5 wavetable,x				//5
		sta wave + voice			//4

	//static values can immediately be stored in SID
		lda instruments,x			//5 adtable,x				//5				
		sta $d405 + voice*7			//4

		lda instruments + 1,x		//5 srtable,x				//5		
		sta $d406 + voice*7			//4

		lda #0						//2
		sta vibratoroutine + vibratoAmountOffset //4
		nop
		bit $01
		.fill 3, NOP				//6


 	
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//		MUST IMMEDIATELY BE FOLLOWED BY NEXT SCANLINE
//		NOT ENOUGH CYCLES FOR JUMP!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

}			

//==============================================
//		NO SETUP FILLER II
//==============================================
.macro setupnone1(){
								//5 for earlier jpm(switch)
		.fill 29,NOP			//58
		jmp nextscanline		//3
								
}
			
//==============================================
//		VIBRATO SETUP PART II
//==============================================
.macro setupvibr2(){
									//5
		lda val2 +voice 			//4				
		
		bmi setupnoteoff2			//2,3,4				
									
bb:		lda val2 + voice			//4			
		and #248					//2 ignore lower 3 bits (bit 7 always 0)
		sta vibratoroutine + vibratoAmountOffset // 4 routine +	8	//4 dv2+1 vibrato/slide value.

	//set vibrato speed	by having 1, 2 or 3 inx at the end of the vibrato routine
		ldx val3  + voice    		//4
		lda vibratospeedtable1,x	//4
		sta scanlineaftervibrato-6	//4
		lda vibratospeedtable2,x	//4
		sta scanlineaftervibrato-5	//4
		lda #3						//2
		sta patternposadd + voice	//4	
		.fill 8, NOP 				//16	
		jmp nextscanline			//3

setupnoteoff2:
									//12
		lda wave + voice			//4
		and #254					//2	
		sta $d404 + [voice*7]		//4

		.if (differentpages(bb, setupnoteoff2)){	
									//+1 from branch
			.fill 20, NOP									
												
		} else {	
			.fill 19,NOP
			bit $01
		}	
		jmp nextscanline			//3
}
//==============================================
//		PATTERN CHANGE SETUP  PART II
//==============================================
.macro setuppattern2(){	
									//5
		ldx currentPattern + voice	//4
		clc							//2

 		lda patterntablelo,x		//5
 		sta sm3+1					//4

 		adc #$01					//2
 		sta sm1+1					//4
		adc #$01					//2
 		sta sm2+1					//4
 		
		.fill 16,  NOP				//28
		//bit $01						//3
		jmp nextscanline			//3
}
//==============================================
//		NEW INSTRUMENT SETUP PART II
//==============================================
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//		MUST IMMEDIATELY BE FOLLOWED BY NEXT SCANLINE
//		NOT ENOUGH CYCLES FOR JUMP!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
.macro setupinstr2(){		
									//5							
		lda wave + voice			//4 
		sta $d404 + [voice*7]		//4 

		ldx val3	+voice			//4 
		stx currentnote + voice		//4 

		lda FreqTablePalLo,x		//5 
		sta $d400	+ [voice*7]		//4 
		sta freqlo  + voice			//4				

		lda FreqTablePalHi,x		//5 
		sta $d401	+[voice*7]		//4 
		sta freqhi + voice			//4 

	//extra value read, increase patternpos with three in stead of default 2 	

		lda #3						//2 
		sta patternposadd + voice	//4 

		ldx val2 + voice			//2 
		lda instruments + 5,x		//5 pulsevibrato,x			//5 
		sta pulsevibratosm			//4 
		bit $01 						//2 

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//		MUST IMMEDIATELY BE FOLLOWED BY NEXT SCANLINE
//		NOT ENOUGH CYCLES FOR JUMP!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
}

//==============================================
//		CLEAN UP AFTER SETUPS
//==============================================
.macro cleanupaftersetup(){
      //this must be done when a change of instrument has occurred
   	  //but if no instrument change has happened this wont be a problem.
		lda pulsehi + voice			//4
		sta $d403	+ voice*7		//4
		lda pulselo +voice			//4
		sta $d402	+voice*7 		//4

	//finally set jmp switches to original
		clc							//2
		lda  #<jumptable1			//2	
		adc #$10					//2
		sta switchlocation1+1		//4
		lda  #<jumptable2			//2
		adc #$10					//2
		sta switchlocation2+1		//4
		clc							//2
	//update patternpos (patternpos will be 2 (event),3 (instrument event) or 0 (no event)
		lda patternposadd + voice	//4
		adc patternpos + voice		//4
		sta patternpos + voice		//4
	//2 is standard value, there are not enough cycles to set it to two when event happens
	// but enough cycles to set it to zero when no event happens
		lda #2						//2
		sta patternposadd + voice	//4	
			
		.fill 6, NOP				//12
}


//==============================================
//
//==============================================
.macro setupnone2(){
										//5
		.fill 29,NOP					//58
		jmp nextscanline				//3
}

//==============================================
//				VIBRATO EXECUTE
//==============================================
	//this part takes care of vibrato or slide
	//this routine is always done, but with values of zero if there is no vibrato or slide
//  dovibrato:		
.macro vibrato(){
		
		ldx vibrcount + voice	//4
		//set code to adc/clc  or sbc/sec depending on note going up or down
dv4:	lda vibcodetable1,x		//5 !! SHOULD NOT CROSS PAGE BOUNDARY !!
		sta dv2					//4
		sta dv3					//4
dv5:	lda vibcodetable2,x		//5 !! SHOULD NOT CROSS PAGE BOUNDARY !!
		sta dv1					//4
		
		lda freqlo + voice		//4
dv1:	clc						//2
dv2:	adc #0					//2  
		sta $d400 + [voice*7]	//4
		sta freqlo + voice 		//4
		lda freqhi +voice		//4
	
dv3:	adc #0					//2		
		sta freqhi +voice		//4
		sta $d401  +[voice*7]	//4
// switching these to NOP can change the speed of vibrato or slide, switching all to NOP will 'lock' routine to ADC or SBC value  
// thereby basically creating a slide   		
		inx						//2
		inx						//2    switch inx for nop to make vibrato at half speed
		inx						//2   or switch this nop for inx for faster vibrato
		stx vibrcount + voice  	//4
		nop

}

//==============================================
//			PULSE VIBRATO EXECUTE
//==============================================
.macro pulsevibrato(){
		lda pulselo + voice		//4
		ldx plsvibrcount + voice//4
dv1:	clc						//2
dv2:	adc #0					//2		this value will be modified by instrument parameters
		sta $d402 + [voice*7]	//4
		sta pulselo + voice 	//4
		lda pulsehi + voice		//4
dv3:	adc #0					//2		
		sta pulsehi +voice		//4
		sta $d403  +[voice*7]	//4
dv4:	lda vibcodetable1,x		//5 !! SHOULD NOT CROSS PAGE BOUNDARY !!
		sta dv2					//4
		sta dv3					//4
dv5:	lda vibcodetable2,x		//5 !! SHOULD NOT CROSS PAGE BOUNDARY !!
		sta dv1					//4
		inx						//2
		nop						//2	These two NOP are not fillers but places for self modifying code where changing NOP to INX will increase vibrato speed  
		nop						//2
		stx plsvibrcount +voice  	//4
		nop
}
 
	.var vibratoAmountOffset = 23 

	.var voice = 0
	.var nextscanline=scanline218
	.var vibratoroutine= scanline225
 	.var jumptable1=jmptable1_1
	.var jumptable2=jmptable1_2
	.var switchlocation1=scanline219
	.var switchlocation2= scanline221
	.var sm1 =scanline217+8
	.var sm2 =scanline217+14
	.var sm3 =scanline217+20
	.var pulsevibratosm = scanline227+8
	.var scanlineaftervibrato = scanline226
	.var firstpattern = firstSongStartPattern1
 	.var patterntablelo = firstSongPatternTable1Lo
 	.var patterntablehi = firstSongPatternTable1Hi
 	
scanline217:
	:soundline1()
scanline218:             
	:lineType1(218)
	.eval nextscanline=scanline220
scanline219:
	jmp (jumptable1+16)					//5
setupnoteon1:
	:setupnoteon()
setupnoteoffvib1:				
	:setupnoteoffvib()				
setupslide1:	
	:setupslide()	
setupnone1_1:
	:setupnone1()

setuppattern1_1:	
	:setuppattern1()	
setupinstr1_1:
	:setupinstr1()

scanline220:             
    :lineType1(220)
scanline221:
	//setup part2
	jmp (jumptable2+16)				//5
	.eval nextscanline = scanline222
setupvibr1_2:
	:setupvibr2()
setuppattern1_2:	
	:setuppattern2()	
setupnone1_2:
	:setupnone2()
setupinstr1_2:		
	
	:setupinstr2()		
scanline222:             
    :lineType1(222)
scanline223:

	:cleanupaftersetup()
scanline224:             
	:lineType1(224)
scanline225:
	:vibrato()		
scanline226:             
    :lineType1(226)
scanline227:
   :pulsevibrato()		

scanline228:             
	:lineType1(228)

//=============================== VOICE 2 =======================
	.eval voice = 1
	.eval nextscanline=scanline230
	.eval vibratoroutine= scanline237
 	.eval jumptable1=jmptable2_1
	.eval jumptable2=jmptable2_2
	.eval switchlocation1=scanline231
	.eval switchlocation2= scanline233
	.eval sm1 =scanline229+8
	.eval  sm2 =scanline229+14
	.eval  sm3 =scanline229+20
	.eval  pulsevibratosm = scanline239+8
	.eval scanlineaftervibrato = scanline238
	.eval firstpattern = firstSongStartPattern2
 	.eval patterntablelo = firstSongPatternTable2Lo
 	.eval patterntablehi = firstSongPatternTable2Hi

scanline229:
	//scan event
   :soundline1()
scanline230:             
	:lineType1(230)
	.eval nextscanline=scanline232
scanline231:
	jmp (jumptable1+16)					//5
setupnoteon2:
	:setupnoteon()
setupnoteoffvib2:				
	:setupnoteoffvib()				
setupslide2:	
	:setupslide()	
setupnone2_1:
	:setupnone1()
setuppattern2_1:	
	:setuppattern1()	
setupinstr2_1:
	:setupinstr1()
scanline232:             
	:lineType1(232)
scanline233:
	//setup part2
	jmp (jumptable2+16)				//5
	.eval nextscanline = scanline234
setupvibr2_2:
	:setupvibr2()
setuppattern2_2:	
	:setuppattern2()	
setupnone2_2:
	:setupnone2()
setupinstr2_2:		
	:setupinstr2()		
scanline234:             
	:lineType1(234)
scanline235:
	:cleanupaftersetup()
scanline236:             
	:lineType1(236)
scanline237:
	:vibrato()
scanline238:             
	:lineType1(238)
scanline239:
	:pulsevibrato()
//=============================== VOICE 3 =======================
	.eval voice = 2
	.eval nextscanline=scanline242
	.eval vibratoroutine= scanline249
 	.eval jumptable1=jmptable3_1
	.eval jumptable2=jmptable3_2
	.eval switchlocation1=scanline243
	.eval switchlocation2= scanline245
	.eval sm1 =scanline241+8
	.eval  sm2 =scanline241+14
	.eval  sm3 =scanline241+20
	.eval  pulsevibratosm = scanline251+8
	.eval scanlineaftervibrato = scanline250
	.eval firstpattern = firstSongStartPattern3
 	.eval patterntablelo = firstSongPatternTable3Lo
 	.eval patterntablehi = firstSongPatternTable3Hi
	
	
scanline240:             :lineType1(240)
scanline241:
	//scanevent3
   :soundline1()
scanline242:             
	:lineType1(242)
	.eval nextscanline=scanline244
scanline243:
	//setuppart1 voice3
   jmp (jumptable1+16)					//5
setupnoteon3:
	:setupnoteon()
setupnoteoffvib3:				
	:setupnoteoffvib()				
setupslide3:	
	:setupslide()	
setupnone3_1:
	:setupnone1()
setuppattern3_1:	
	:setuppattern1()	
setupinstr3_1:
	:setupinstr1()
scanline244:             
	:lineType1(244)
scanline245:
	//setup part2
	jmp (jumptable2+16)				//5
	.eval nextscanline = scanline246
setupvibr3_2:
	:setupvibr2()
setuppattern3_2:	
	:setuppattern2()	

setupnone3_2:
	:setupnone2()
setupinstr3_2:		
	:setupinstr2()		
scanline246:             
	:lineType1(246)
scanline247:
	//play + restore after setup3
   :cleanupaftersetup()
scanline248:             
	:lineType1(248)
scanline249:
	//vibrato3
   :vibrato()
scanline250:             
	:lineType1(250)
scanline251:
	//plsvibrato3
   pulsevibrato()

 .macro lineType1(a) {}