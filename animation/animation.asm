.const MOM_TOP_LEFT = 6*40 + 6
.const MONITOR_TOP_LEFT = MOM_TOP_LEFT + 14 + 5*40

.var brkFile = createFile("debug.txt") 

.macro break() {
	.eval brkFile.writeln("break " + toHexString(*))
}

.import source "..\loader\loadervars"

.var writerPointer = $fc

.var musicStart = $be00

.var endlessloop

.pc = $0801 "Basic upstart"
:BasicUpstart($0810)		

.pc = $0810 
 
	//turn off basic
	lda #$36
	sta $01
	
	jsr copyMusic
	jsr copyChars

	//set char set pointer at $3800, char mem stays $0400
	lda $d018
    ora #$0e       //set chars location to $3800 for displaying the custom font
    sta $d018      //Bits 1-3 ($400+512bytes * low nibble value) of $d018 sets char location
                   //$400 + $200*$0E = $3800

	lda $d016
	//and #%11110111		//38columns
	ora #%00010000		//enable multicolor
	sta $d016

	jsr $e544 //clr screen

	initMusic(5)
	
	lda # DARK_GREY
	sta $D022
	lda # LIGHT_GREY
	sta $D023
	lda # BLACK
	sta $d020
	sta $d021

	//set char color white, multicolor
	ldx #0 
!:	lda # WHITE + 8 
	sta $d800,x
	sta $d900,x
	sta $da00,x
	sta $db00-24,x
	inx
	bne !-

	ldx #80
	lda # WHITE
!:	sta $d800,x
	dex
	bpl !-

//only monitor has char color blue 
!:	lda # BLUE + 8
	sta $d800 + MONITOR_TOP_LEFT + 2 + 40
	sta $d800 + MONITOR_TOP_LEFT + 3 + 40
	sta $d800 + MONITOR_TOP_LEFT + 4 + 40

	sta $d800 + MONITOR_TOP_LEFT + 2 + 80
	sta $d800 + MONITOR_TOP_LEFT + 3 + 80
	sta $d800 + MONITOR_TOP_LEFT + 4 + 80

	jsr drawClosedDoor
	jsr drawMonitorNeutral


//setup interrupt
	sei
          // turn off interrupts
        	lda #$7f
	        ldx #$01
			sta $dc0d
	        stx $d01a    // raster interrupts aan

	        lda #<int    // zet eerste interrupt vector
        	ldx #>int    // 
	        ldy #$f7     // lijn interrupt
        	sta $0314    // in interrupt vector
	        stx $0315
        	sty $d012

		lda $d011    // zorgt ervoor dat het bit 8 van Y-raster uit staat (dus < 255)
		and #$7f
		sta $d011
		cli
		//copy startroutine outside of load area		
		ldx #$80
!:		lda loader,x
		sta $b600,x
		dex
		bpl !-
.eval endlessloop = $b600 + (el - startgame)	
		jmp endlessloop

pause:	.byte 0

copyMusic:
	ldy #$12
cm1:	
	ldx #0
cm2:
	lda music,x
	sta $be00,x
	inx
	bne cm2
	inc cm2 + 2
	inc cm2 + 5
	dey
	bne cm2
	rts


drawTile:
	ldy #0
	sty $fb
	ldx #0
dt1:
	lda tilesetStart: $ffff,x
	clc
	adc #$40
	sta drawAreaStart: $ffff,y
	inx
	iny
	txa
	and #$07
	bne dt1
	ldy #0
	lda drawAreaStart
	clc
	adc #40
	sta drawAreaStart
	bcc !+
	inc drawAreaStart + 1
!:	inc $fb
	lda $fb
	cmp #$08
	bne dt1
	rts	

.macro initMusic (nr){
	lda #nr
	jsr musicStart
}

.macro playMusic() {jsr musicStart + 3}


.macro playSoundEffect(length, note, instrument){
	lda #length
	ldx #note
	ldy #instrument
	jsr musicStart + 12
}

.macro drawBlock(tilesetdataoffset, drawstartoffset) {
	lda #<$0400+ drawstartoffset
	sta drawAreaStart
	lda #>$0400+ drawstartoffset
	sta drawAreaStart + 1
	lda #<tileset_data+tilesetdataoffset
	sta tilesetStart
	lda #>tileset_data+tilesetdataoffset
	sta tilesetStart + 1
	jsr drawTile
	rts
}

drawClosedDoorColor:
	.for(var y = 0; y<8; y++) {
		.for(var x = 0; x<8; x++) {
			lda # WHITE + 8
			sta $d800 +  MOM_TOP_LEFT + x + 40*y
		}
	}
	rts

drawClosedDoor:
	drawBlock(2*64, MOM_TOP_LEFT)
drawDoorOpen1:
	drawBlock(64, MOM_TOP_LEFT)
drawDoorOpen2:
	drawBlock(5*64, MOM_TOP_LEFT)
drawMonitorNeutral:
	drawBlock(0, MONITOR_TOP_LEFT)
drawMonitorLeft:
	drawBlock(8*64, MONITOR_TOP_LEFT)
drawMonitorRight:
	drawBlock(7*64, MONITOR_TOP_LEFT)	
drawMonitorClosedEyes:
	drawBlock(6*64, MONITOR_TOP_LEFT)
drawMonitorMouthOpen:
	drawBlock(4*64, MONITOR_TOP_LEFT)
drawMonitorLookToDoor:
	drawBlock(3*64, MONITOR_TOP_LEFT)
drawMonitorLookingUp:
	drawBlock(9*64, MONITOR_TOP_LEFT)
drawMonitorClosedEyesScreenOff:
	drawBlock(10*64, MONITOR_TOP_LEFT)
drawMonitorNeutralMonitorOff:
	drawBlock(12*64, MONITOR_TOP_LEFT)
drawMonitorMouthOpenMonitorOff:	
	drawBlock(11*64, MONITOR_TOP_LEFT)
	
drawMonitorColor:
	.for(var y = 0; y<8; y++) {
		.for(var x = 0; x<8; x++) {
			lda # BLUE + 8
			sta $d800 +  MONITOR_TOP_LEFT + x + 40*y

		}
	}
	rts

newText1: {
	initMusic(4)
	writeText2(t1)
	endEvent(49)
}
newText2: {
	writeText2(t2)
	endEvent(49)
}
newText3: {
	writeText2(t3)
	endEvent(49)
}
newText4: {
	writeText2(t4)
	endEvent(49)
}
newText5: {
	writeText2(t5)
	endEvent(49)
}
newText6: {
	writeText2(t6)
	endEvent(49)
}
newText6a: {
	writeText2(t6a)
	endEvent(49)
}
newText6b: {
	writeText2(t6b)
	endEvent(49)
}
newText6c: {
	writeText2(t6c)
	endEvent(49)
}
newText7: {
	writeText2(t7)
	endEvent(49)
}
newText8: {
	writeText2(t8)
	endEvent(49)
}
newText9: {
	writeText2(t9)
	endEvent(49)
}

t1:
	.text "coders without borders present"
	.byte $ff
t2:	
	.text "a game-in-a-sprite production"
	.byte $ff
t3:	
	.text "        code by goerp"
	.byte $ff
t4:	
	.text "         graphics by goerp"
	.byte $ff
t5:	
	.text "        music by goerp"
	.byte $ff

t6:	
	.text "playtesting by roel "
	.byte $ff

t6a:	
	.text "        bugs by goerp"
	.byte $ff

t6b:	
	.text "this tune based on a remix by "   
	.text " roel of in-game tune no. 1"
	.byte $ff

t6c:
	.text "made for the forum64 and      " 
	.text "protovision game compo 2017"
	.byte $ff
t7:	
	.text "no vic registers were harmed"
	.byte $ff
t8:	
	.text "..except that one register.."
	.byte $ff
t9:	
	.text "     ..  a lot!"
	.byte $ff
t10:	
	.text "are you still sitting         "
	.text "behind your computer?         "
		.byte $ff
t11:	
	.text "i thought i told you          " 
	.text "to turn that screen off!      "
		.byte $ff
t12:	
	.text "                        ok ok "
	.text "                              "
		.byte $ff
t13:	
	.text "                 hold on . .  "
	.text "                              "
	.byte $ff
t14:	
	.text "poke 53265,peek(53265)and 239 "
	.text "                              "
	.byte $ff
t15:	
	.text " according to compute! gazette"
	.text "                              "
		.byte $ff
t16:	
	.text "         the screen is now off"
	.text "                              "
		.byte $ff
t17:	
	.text "            but what can i do "
	.text "         with the screen off? "
	.byte $ff
t18:	
	.text "                but of course!"
	.text "                              "
         //123456789012345678901234567890
	.byte $ff

copyChars:
	ldx #0
!:	lda helsingAlphabet,x
	sta $3800,x
	lda helsingAlphabet + 256,x
	sta $3800+256,x
	inx
	bne !-

	//96 chars 
	ldx #0
!:	lda charset_data,x
	sta $3a00,x
	lda charset_data + 256,x
	sta $3a00 + 256,x
	lda charset_data + 512,x
	sta $3a00 + 512,x
	inx
	bne !-
	rts
//----------------------------------- INT
.var FRAME_LENGTH = 5
.var FRAMES_TILL_FIRST_EVENT = 5
frameTimerLo:	.byte FRAME_LENGTH
frameTimerHi:	.byte FRAMES_TILL_FIRST_EVENT
currentEvent:	.byte 0

eventPointers:		
				.word eTickLeft, eNoTick, eTickRight,eTickLeft, eNoTick, eTickRight,eTickLeft, eNoTick, eTickRight,eTickLeft, eNoTick, eTickRight,eTickLeft, eNoTick, eTickRight,eTickLeft, eNoTick, eTickRight
				.word eOpenDoor,ePrintText1
				.word eMMouthOpen,eMMouthClosed, eMMouthOpen,eMMouthClosed, eMMouthOpen,eMMouthClosed, eMMouthOpen 
				.word ePrintText2
				.word eMMouthOpen,eMMouthClosed, eMMouthOpen,eMMouthClosed, eMMouthOpen,eMMouthClosed, eMMouthOpen,eMMouthClosed, eMMouthOpen
				.word eDrawMouthClosed, eDrawMouthOpen,ePrintText3
				.word eDrawMouthClosed,eDrawMouthOpen, eDrawMouthClosed
				.word eCloseDoor, eCloseEyes 
				.word eDrawMouthClosed, ePrintText4
				.word eDrawMouthClosed
				.word ePrintText5
				.word eTickLeft, eNoTick, eTickRight,eTickLeft, eNoTick, eTickRight,eTickLeft
				.word eLookUp2
				.word eLookUp3
				.word eLookUp2
				.word ePrintText6
				.word eDrawMouthClosedMonitorOff,eDrawMouthClosedMonitorOff
				.word ePrintText7
				.word eDrawMouthClosedMonitorOff,eDrawMouthClosedMonitorOff
				.word ePrintText8
				.word eDrawMouthClosedMonitorOff,eDrawMouthClosedMonitorOff
				.word eCloseEyes2
				.word eLookUp2, ePrintText9

				.word eLookUp2
				.word clearScreen
				.word newText1,startColorCycle, newText2,startColorCycle, newText3,startColorCycle, newText4,startColorCycle, newText5,startColorCycle, newText6,startColorCycle,newText6a,startColorCycle,newText6b,startColorCycle, newText6c,startColorCycle,newText7,startColorCycle, newText8,startColorCycle, newText9,startColorCycle
				.word setScreenBlack, drawKungFu, drawLoading   
				.word lastEvent  
.if((*-eventPointers)>255) {				
	.error("too many events!")
}else {				
	.print(*-eventPointers)
}				
pause10: 
	endEvent(10) 				
clearScreen:
	lda $d016
	//and #%11110111		//38columns
	and #%11101111		//disable multicolor
	sta $d016

	jsr $e544
	jsr setTextColor	
	endEvent(3)
/*-------------------------------------------------------------------------------------------------------------
						MAIN LOOOP
---------------------------------------------------------*/				

int:
	lda $d01a
	and #$01
	bne !+
	jmp end
!:	playMusic()	
	lda doingColorCycle	
	beq !+	
	jsr doColorCycle	
	jmp end	
!:	dec frameTimerLo
	beq !+
	jmp end
!:	lda #FRAME_LENGTH
	sta frameTimerLo
	dec frameTimerHi
	beq !+
	jmp end
!:	lda currentEvent
	asl 
	tax
	lda eventPointers,x
	sta jmpPoint + 1
	lda eventPointers + 1,x
	sta jmpPoint + 2
jmpPoint:	
	jmp *
end:	
	asl $d019
	jmp $ea31
eTickLeft:
	playSoundEffect(3,72,2)
	jsr drawMonitorLeft
	endEvent(3)
eNoTick:
	jsr drawMonitorNeutral
	endEvent(3)
eTickRight:
	playSoundEffect(3,70,2)
	jsr drawMonitorRight
	endEvent(3)

eOpenDoor:
	playSoundEffect(10,40,3)
	jsr drawDoorOpen2
	jsr drawMonitorLookToDoor
	endEvent(10)	
ePrintText1:
	initMusic(0)
	jsr drawDoorOpen2
	writeText1(t10)
	//writeText("are you still sitting behind        your computer?")
	endEvent(1)
eMMouthOpen:
	jsr drawDoorOpen2
	endEvent(4)
eMMouthClosed:
	jsr drawDoorOpen1
	endEvent(4)
ePrintText2:
	initMusic(2)
	writeText1(t11)
	//writeText("i thought i told you to turn       that screen off!")
	endEvent(1)
ePrintText3:	
	initMusic(1)	
	writeText1(t12)//writeText("                                                 ok ok")
	endEvent(1)
ePrintText4:	
	writeText1(t13)//writeText("hold on . . ")
	endEvent(18)
ePrintText5:	
	writeText1(t14)//writeText("poke 53265,peek(53265)and 239")
	endEvent(18)
ePrintText6:	
	writeText1(t15)//writeText("according to the programmers handbook              the screen is now off")
	endEvent(18)
ePrintText7:	
	writeText1(t16)//writeText("       hee hee            . . in a way")
	endEvent(18)
ePrintText8:	
	writeText1(t17)//writeText("but what can i do with                 the screen off")
	endEvent(18)
ePrintText9:	
	writeText1(t18)//writeText("but of course!")
	endEvent(26)
eDrawMouthClosedMonitorOff:	
	jsr drawMonitorNeutralMonitorOff
	endEvent(6)
eDrawMouthOpenMonitorOff:	
	jsr drawMonitorMouthOpenMonitorOff
	endEvent(3)
eDrawMouthClosed:	
	jsr drawMonitorNeutral
	endEvent(5)
eDrawMouthOpen:	
	jsr drawMonitorMouthOpen
	endEvent(5)
eCloseEyes:	
	initMusic(3)	
	jsr drawMonitorClosedEyes
	endEvent(20)
eCloseEyes2:	
	initMusic(3)
	jsr drawMonitorClosedEyesScreenOff
	endEvent(20)

eLookUp:	
	jsr drawMonitorLookingUp
	endEvent(16)

eLookUp2:	
	jsr drawMonitorLookingUp
	endEvent(1)
eLookUp3:		
	jsr drawMonitorNeutralMonitorOff
	endEvent(16)
eCloseDoor:
	playSoundEffect(10,40,4)
	jsr drawClosedDoor
	endEvent(3)
	
lastEvent:
	ldx #0
!:	lda loaderBinary,x
	sta loadfile,x
	lda loaderBinary+256,x
	sta loadfile + 256,x
	lda loaderBinary+512,x
	sta loadfile + 512,x
	lda loaderBinary+768,x
	sta loadfile + 768,x
	lda loaderBinary+1024,x
	sta loadfile + 1024,x
	inx
	bne !-


	lda #1
	sta go

	asl $d019
	jmp $ea31
	

/*LoadAddress+12, if you exported the tune
with the special SID-Maker-SFX. All FXes
are essentially instruments, they over-
ride channel3 notes during execution.
You have to set the CPU-registers first:
X=Note, Y=Instrument, A=Length (frames)
*/
.macro endEvent(framesTillNextEvent) {	
	lda #framesTillNextEvent
	sta frameTimerHi
	inc currentEvent
	jmp end
}	
writer: 
{	
	lda #WHITE
	sta textColor
	ldx #0
	stx colorCount
	
	ldx #120
	lda #0
!:	sta $0400 + 9*40 -1,x
	dex
	bne !-
	ldy #$00
	ldx #$00
loop:	
	lda (writerPointer),y
	cmp #$ff
	bne !+
	jmp end
!:	sta $0400 + 9*40 + 5,x
	iny
	inx
	cpx #30
	bne !+
	txa
	clc
	adc #10
	tax
!:	jmp loop
	
end:
	rts
}

writer1: 
{	
	ldy #$00
	ldx #$00
loop:	
	lda (writerPointer),y
	cmp #$ff
	bne !+
	jmp end
!:	sta $0401,x
	iny
	inx
	cpx #30
	bne !+
	txa
	clc
	adc #10
	tax
!:	jmp loop
	
end:
	rts
}
textColor:	.byte 1
colorCycle:	.byte WHITE, LIGHT_GRAY, WHITE, LIGHT_GRAY, LIGHT_GRAY, GRAY, LIGHT_GRAY, GRAY, GRAY, DARK_GREY, GRAY, DARK_GREY, DARK_GREY, BLACK,DARK_GREY, BLACK, BLACK, BLACK, BLACK, BLACK    
colorCount:	.byte 0    	
setTextColor: {
	lda textColor
	ldx #120
!:	sta $d800 + 9*40 -1,x
	dex
	bne !-
	rts	
}

startColorCycle:
	lda #1
	sta doingColorCycle
	endEvent(1)
	
doingColorCycle:	.byte 0

doColorCycle:
	ldx colorCount
	lda colorCycle,x
	sta textColor
	jsr setTextColor
	ldx colorCount
	inx
	stx colorCount
	cpx #18
	bne !+
	lda #0
	sta doingColorCycle
	sta colorCount
	lda #1
	sta textColor
	//jsr setTextColor
!:	rts

setScreenBlack:
	lda #21
	and #%00001111
	ora #%11100000
    sta $d018
    
    //vic bank2
    lda $dd00
    and #%11111100
    ora #%00000001
    sta $dd00
    
	ldx #0
	ldy #160
!:	lda #0
	sta $d800,x
	sta $d900,x
	sta $da00,x
	sta $db00-24,x
	tya
	sta $b800,x
	sta $b900,x
	sta $ba00,x
	sta $bb00-24,x
	inx
	bne !-
	endEvent(1)

startColorAt: .word $d800+10 
curPos:	.byte 0
drawKungFu:
{
	lda startColorAt
	sta $fc
	lda startColorAt + 1
	sta $fd
	ldy #0
	ldx #0
dkf1:
	lda drawList,x
	cmp #$fe
	bne !+
	jmp nextLine
!:	cmp #$fd
	bne !+
	jmp changeColor
!:	cmp #$ff
	bne !+
	jmp end
!:	sta plus + 1
	tya
	clc
plus:	
	adc #0
	tay
color:	
	lda  #1
	sta ($fc),y
	inx	
	jmp dkf1	   	
nextLine:	
	lda $fc	
	clc	
	adc #$28	
	sta $fc	
	bcc !+	
	inc $fd	
!:	inx	
	ldy #0	
	jmp dkf1
changeColor:	
	inc color + 1	
	inx	
	jmp dkf1
end:
	endEvent(15)
}

drawLoading:
{
	lda #<$d800 + 20*40 +5
	sta $fc
	lda #>$d800 + 20*40 +5
	sta $fd
	ldy #0
	ldx #0
dkf1:
	lda drawList2,x
	cmp #$fe
	bne !+
	jmp nextLine
!:	cmp #$ff
	bne !+
	jmp end
!:	sta plus + 1
	tya
	clc
plus:	
	adc #0
	tay
	lda #4
	sta ($fc),y
	inx	
	jmp dkf1	   	
nextLine:	
	lda $fc	
	clc	
	adc #$28	
	sta $fc	
	bcc !+	
	inc $fd	
!:	inx	
	ldy #0	
	jmp dkf1
end:
	endEvent(5)
}

drawList:
	.byte 0,$fe
	.byte 0,2,2,2,2,1,1,2,1,$fe
	.byte 0,1,3,2,2,2,2,1,$fe
	.byte 0,2,2,2,2,2,2,1, $fe
	.byte 0,2,2,1,1,2,2,3,$fe
	.byte 12,1,$fe
	.byte $fd, $fe
	.byte 2,1,2,2,$fe
	.byte 2,3,2,$fe
	.byte 2,1,2,2,$fe
	.byte 2,3,1,1,$fe
	.byte 2, $fe
	.byte $fd, $fe
	.byte 0,1,2,2,2,2,1,2,$fe
	.byte 0,1,2,3,3,1,2,$fe
	.byte 0,1,2,2,2,2,3,$fe
	.byte 0,3,2,2,2,1,2,1,$fe
	.byte 0,$fe, $fe, $ff

drawList2:	
	.byte 0,6,4, $fe	
	.byte 0,3,1,3,3,2,2,1,1,2,1,$fe	
	.byte 0,3,1,2,1,2,1,2,2,2,2,1,$fe	
	.byte 0,1,2,1,2,1,2,1,2,2,2,3,$fe	
	.byte 18,1, $ff	
 
	
	 

.macro writeText2(start){
	jsr setTextColor
	lda #<start
	sta writerPointer
	lda #>start
	sta writerPointer + 1
	jsr writer
}
.macro writeText1(start){
	lda #<start
	sta writerPointer
	lda #>start
	sta writerPointer + 1
	jsr writer1
}


.function getLetterNr(screenchar) {
	.if((screenchar.charAt(0)>0 && screenchar.charAt(0)<27)){
		.return screenchar.charAt(0)-1
	}
	.if((screenchar.charAt(0)>47 && screenchar.charAt(0)<57)){
		.return screenchar.charAt(0)-47+26
	}
	.if((screenchar.charAt(0) == 58)){
		.return 36
	}
}

loader: 

.pseudopc $b600
/*
{
startgame:
	sei
	lda #$31
	sta $0314
	lda #$ea
	sta $0315
	lda #$36
	sta $01
	ldx #0
     stx $d01a
	cli
        lda #fname_end-fname
        ldx #<fname
        ldy #>fname
        jsr $FFBD     //; call SETNAM
        lda #$01
        ldx $BA       //; last used device number
        bne skip
        ldx #$08      //; default to device 8
skip:   ldy #$01      //; not $01 means: load to address stored in file
        jsr $FFBA     //; call SETLFS

        lda #$00      //; $00 means: load to memory (not verify)
        jsr $FFD5     ///; call LOAD
        bcs err    //; if carry set, a load error has happened
        jmp $0810
err:
	
        // Accumulator contains BASIC error code

        //; most likely errors:
        //; A = $05 (DEVICE NOT PRESENT)
        //; A = $04 (FILE NOT FOUND)
        //; A = $1D (LOAD ERROR)
        //; A = $00 (BREAK, RUN/STOP has been pressed during loading)

        //... error handling ...
        rts

fname:  
	.encoding "petscii_upper"  
	.text "MAIN"
fname_end:
el:	
	lda go
	beq !+
	jmp startgame
!:	jmp el


go:	.byte 0
}
*/
{
startgame:
	sei
	lda #$31
	sta $0314
	lda #$ea
	sta $0315
	lda #$36
	sta $01
	ldx #0
     stx $d01a
	cli

	jsr initloader

	//jsr initfastload
	//jsr fastopen
	ldx #<filename
	ldy #>filename
	jsr loadfile
	bcc !+
	sta $bcff		//error?
!:	jmp $0810
	
filename:	.text "GAME"
			.fill 8, 0
partnamelength: .byte 4 //15			

el:	
	lda go
	beq !+
	jmp startgame
!:	jmp el
go:	.byte 0

}



helsingAlphabet:
.import binary "charset.bin" 

//.import source "animation_data.asm"

charset_data:
.import binary "animachars.bin"
.print toHexString(*)

//18blocks of music , starting address $be00
music:
.import binary "intro-c-be00.bin"
tileset_data:
.import binary "animatiles.bin"

//loader aprox. 5 blocks
.var exomizer = LoadBinary("..\loader\loader.prg", BF_C64FILE)
loaderBinary: .fill exomizer.getSize(), exomizer.get(i)
