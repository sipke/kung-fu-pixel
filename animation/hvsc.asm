#import "../Libs/MyPseudoCommands.lib"
.var brkFile = createFile("debug.txt") 

.macro break() {
	.eval brkFile.writeln("break " + toHexString(*))
}

	.var songChangeHiSm1V1 = setuppattern1_1 + 15
	.var songChangeHiSm1V2 = setuppattern2_1 + 15
	.var songChangeHiSm1V3 = setuppattern3_1 + 15
	.var songChangeHiSm2V1 = setuppattern1_1 + 22
	.var songChangeHiSm2V2 = setuppattern2_1 + 22
	.var songChangeHiSm2V3 = setuppattern3_1 + 22
	.var songChangeLoSm1V1 = setuppattern1_2 + 5
	.var songChangeLoSm1V2 = setuppattern2_2 + 5
	.var songChangeLoSm1V3 = setuppattern3_2 + 5
	.var songChangeFirstPattern1Sm1 = scanline217 + 9
	.var songChangeFirstPattern1Sm2 = scanline217 + 15		//217, 229, 241
	.var songChangeFirstPattern1Sm3 = scanline217 + 21		//8,14,20
	.var songChangeFirstPattern2Sm1 = scanline229 + 9
	.var songChangeFirstPattern2Sm2 = scanline229 + 15		//217, 229, 241
	.var songChangeFirstPattern2Sm3 = scanline229 + 21		//8,14,20
	.var songChangeFirstPattern3Sm1 = scanline241 + 9
	.var songChangeFirstPattern3Sm2 = scanline241 + 15		//217, 229, 241
	.var songChangeFirstPattern3Sm3 = scanline241 + 21		//8,14,20

.pc = $0801 "Basic upstart"
:BasicUpstart($0810)		

.pc = $0810 "setup" 

	
	ldx #0
	lda #0
!:	sta $d800,x
	sta $d900,x
	sta $da00,x
	sta $db00-24,x
	lda #160
	sta $0400,x
	sta $0500,x
	sta $0600,x
	sta $0700-24,x
	inx
	
	bne !- 
	sei
	lda #<irq
	sta $0314
	lda #>irq
	sta $0315
	cli
	rts

irq:	
	jsr play	
	jmp $ea31	
		


*=$0fd0
init:			
	sta currentSong
	cmp #4
	blt doInGameInit
doIntroInit:	
	sec	
	sbc #$04	
	jsr introSid	
	rts	
doInGameInit:		
	jsr initingamemusic		
	rts		

play:			
	lda currentSong
	cmp #4
	blt doInGamePlay
doIntroPlay:	
	jsr introSid + 3	
	rts	
doInGamePlay:		
	jsr ingamemusic		
	rts		
	

*=$1000				
introSid:				
	.import binary "introforsid.bin"				


musicData:	
	.import source "../musicdata.asm"	
currentsong:	.byte 0	
		//A register will contain song to play, >5 will be sid wizard		

ingamemusic:					
	.import source "../customlinecodecustomized.asm"					
	rts
initingamemusic:				
	.import source "../changesong.asm"
end:
	rts
doNothingIrq:	.word $ffff
stateSwitch:	.word $ffff
pause:			.byte 0

.import source "../spritedefs.asm"
newposlo:
	.fill 144, <(($d800+ (40*floor(i/12))+ (2 * mod(i,12))) + 2)
	.fill 144, <(($d800+ (40*floor(i/12))+ (2 * mod(i,12))) + 2 + 10)
	.fill 144, <(($d800+ (40*floor(i/12))+ (2 * mod(i,12))) + 2 + 20)
newposhi:
	.fill 144, >(($d800+ (40*floor(i/12))+ (2 * mod(i,12))) + 2)
	.fill 144, >(($d800+ (40*floor(i/12))+ (2 * mod(i,12))) + 2 + 10)
	.fill 144, >(($d800+ (40*floor(i/12))+ (2 * mod(i,12))) + 2 + 20)


