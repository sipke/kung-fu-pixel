changeSong: {

	ldx #0
	stx patternpos
	stx patternpos + 1
	stx patternpos + 2
	stx currentPattern
	stx currentPattern + 1
	stx currentPattern + 2
	inx
	stx countTillNextEvent
	stx countTillNextEvent + 1
	stx countTillNextEvent + 2
//reset vibrato value
	lda #0
	sta scanline225 + vibratoAmountOffset
	sta scanline237 + vibratoAmountOffset
	sta scanline249 + vibratoAmountOffset


//set volume filter attributes of song

	ldx currentSong
	lda d415data,x
	sta $d415
	lda d416data,x
	sta $d416
	lda d417data,x
	sta $d417
	lda d418data,x
	sta $d418

//------change pointers in music code 	
//voice 1
	lda songPointersLo1_lo,x	
	sta songChangeLoSm1V1
	sta v1_1 +1
	lda songPointersLo1_hi,x	
	sta v1_1 +2	
	sta songChangeLoSm1V1 + 1

	lda songPointersHi1_lo,x	
	sta songChangeHiSm1V1
	sta songChangeHiSm2V1
	sta v1_2 + 1 
	lda songPointersHi1_hi,x	
	sta songChangeHiSm1V1 + 1
	sta songChangeHiSm2V1 + 1
	sta v1_2 + 2
	//---------- set  first pattern of song in code 
v1_1: lda $ffff  		//dummy value will be changed by preceding code 
	sta songChangeFirstPattern1Sm3 
	clc
	adc #$01	//dont have to do overflow checks, asserts in generated music tacke care page is not crossed within pattern
	sta songChangeFirstPattern1Sm1
	adc #$01
	sta songChangeFirstPattern1Sm2
	
v1_2:	lda $ffff 
	sta songChangeFirstPattern1Sm1 + 1
	sta songChangeFirstPattern1Sm2 + 1
	sta songChangeFirstPattern1Sm3 + 1

//-------------same for voice 2
//voice 2
	lda songPointersLo2_lo,x	
	sta songChangeLoSm1V2
	sta v2_1 +1
	lda songPointersLo2_hi,x	
	sta songChangeLoSm1V2 + 1
	sta v2_1 +2
	
	lda songPointersHi2_lo,x	
	sta songChangeHiSm1V2
	sta songChangeHiSm2V2 
	sta v2_2 +1
	lda songPointersHi2_hi,x	
	sta songChangeHiSm1V2 + 1
	sta songChangeHiSm2V2 + 1
	sta v2_2 +2

v2_1: lda $ffff 
	sta songChangeFirstPattern2Sm3 
	clc
	adc #$01	//dont have to do overflow checks, asserts in generated music tacke care page is not crossed within pattern
	sta songChangeFirstPattern2Sm1
	adc #$01
	sta songChangeFirstPattern2Sm2
v2_2:	lda $ffff 
	sta songChangeFirstPattern2Sm1 + 1
	sta songChangeFirstPattern2Sm2 + 1
	sta songChangeFirstPattern2Sm3 + 1

//voice 3
	lda songPointersLo3_lo,x	
.assert "address defined ", false, songChangeLoSm1V3 == 0 		
	sta songChangeLoSm1V3
	sta v3_1 +1
	lda songPointersLo3_hi,x	
	sta songChangeLoSm1V3 + 1
	sta v3_1 +2

	lda songPointersHi3_lo,x	
	sta songChangeHiSm1V3
	sta songChangeHiSm2V3
	sta v3_2 +1 
	lda songPointersHi3_hi,x	
	sta songChangeHiSm1V3 + 1
	sta songChangeHiSm2V3 + 1
	sta v3_2 +2

v3_1: lda $ffff 
	sta songChangeFirstPattern3Sm3
	clc
	adc #$01	//dont have to do overflow checks, asserts in generated music tacke care page is not crossed within pattern
	sta songChangeFirstPattern3Sm1
	adc #$01
	sta songChangeFirstPattern3Sm2
v3_2:	lda $ffff 
	sta songChangeFirstPattern3Sm1 + 1
	sta songChangeFirstPattern3Sm2 + 1
	sta songChangeFirstPattern3Sm3 + 1
	
	lda #<doNothingIrq
	sta stateSwitch
	lda #>doNothingIrq
	sta stateSwitch + 1
	
	lda wave
	and #$fe
	sta $D404
	lda wave + 1
	and #$fe
	sta $D40B
	lda wave + 2
	and #$fe 
	sta $D412
	
	lda #3
	sta pause 
	jmp end
}
