/*
.function default_close_stuff(player) { 
	.if (player==1) {
		.return jmp (defaultCloseSwitch) 
	} else {
		.return jmp default_close_stuff2
	}
}


.macro playerHitMid(player){
	hitMidAni(player)	
	default_close_stuff(player)
}
.macro playerHitHigh(player){
	hitHighAni(player)	
	default_close_stuff(player)
}
.macro playerHitLow(player){
	hitLowAni(player)	
	default_close_stuff(player)
}

.macro hitHighAni(player) {queueAnimation(List().add(Animation(hithigh1,2),  Animation(hithigh2,2),	Animation(hithigh3,2)),
								PosChange(0,3),2,player )}
.macro hitLowAni(player) {queueAnimation(List().add(Animation(hitmid1,2),  Animation(hitmid2,2),	Animation(hitmid3,2)),
								PosChange(0,3),2,player )}
.macro hitMidAni(player) {queueAnimation(List().add(Animation(hithigh2,2),  Animation(hitmid3,2),	Animation(hithigh3,2)),
								PosChange(0,3),2,player )}

*/
