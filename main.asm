*=$0801
#import "Libs/MyPseudoCommands.lib"

.var brkFile = createFile("debug.txt") 

.macro break() {
	.eval brkFile.writeln("break " + toHexString(*))
}

//animation vars to add to beginning of asm 
.import source "spritevars.asm"
	.const o	= 140
	.const I	= 141
	.const yo = 0
	
	.const X	= 142	
	.const A 	= 141
	.const n 	= 143
	.const Y = 140
	.const startPos1 = 1
	.const startPos2 = 8

	.const SID = $D400
	.const volume = $D418
	.const pulseLo = $D402
	.const pulseHi = $D403
	.const controlV1 = $D404
	.const AD = $D405
	.const SR = $D406  
	.const osc3 = $D41B
	.const controlV3 = $D412

	.const betweenLineColor = LIGHT_GREY
	.const backgroundColor = LIGHT_GREY
	.const fleshColor = YELLOW
	.const suiteColor = BROWN
	.const lineColor = RED
	.const scoreColor = GREEN
	.const timerColor = PURPLE

	.const spriteDataLocation = $300

	.const joystick1 = $dc01
	.const joystick2 = $dc00

	.const linesFrame1 = List()
	.const linesFrame2 = List()
	.const zpStart = $2
	.const backgroundColorZp = $fe

	.const maxEnergy = 6 
	.const MAX_LEVEL = 5	
	.const INTRO_PAUSE_TIME = 50		
	.const HISTORY_AND_VALUE = 7		//determines how many player 2 moves are stored before old values are overwritten	
	.const STANDARD_DELAY_VALUE = 40	

	.const PENALTY_TIME = 160
			
	.var songChangeHiSm1V1 = setuppattern1_1 + 15
	.var songChangeHiSm1V2 = setuppattern2_1 + 15
	.var songChangeHiSm1V3 = setuppattern3_1 + 15
	.var songChangeHiSm2V1 = setuppattern1_1 + 22
	.var songChangeHiSm2V2 = setuppattern2_1 + 22
	.var songChangeHiSm2V3 = setuppattern3_1 + 22
	.var songChangeLoSm1V1 = setuppattern1_2 + 5
	.var songChangeLoSm1V2 = setuppattern2_2 + 5
	.var songChangeLoSm1V3 = setuppattern3_2 + 5
	.var songChangeFirstPattern1Sm1 = scanline217 + 9
	.var songChangeFirstPattern1Sm2 = scanline217 + 15		//217, 229, 241
	.var songChangeFirstPattern1Sm3 = scanline217 + 21		//8,14,20
	.var songChangeFirstPattern2Sm1 = scanline229 + 9
	.var songChangeFirstPattern2Sm2 = scanline229 + 15		//217, 229, 241
	.var songChangeFirstPattern2Sm3 = scanline229 + 21		//8,14,20
	.var songChangeFirstPattern3Sm1 = scanline241 + 9
	.var songChangeFirstPattern3Sm2 = scanline241 + 15		//217, 229, 241
	.var songChangeFirstPattern3Sm3 = scanline241 + 21		//8,14,20
	
	
	.var lineType1Length = 42		//the code length code of the lineType1 Macro 
	.var lineType2Length = 50		//the code length code of the lineType2 Macro
	.var curPixel=0 
		
	.var firstLine 	= 30

	.var after_move1 = $0000
	.var default_close_stuff1 = $0000
	.var after_move1_ai = $0000
	.var default_close_stuff1_ai = $0000
	.var after_move2 = after_move2a //$0000
	.var default_close_stuff2 = default_close_stuff2a //$0000
	.var ai = $0000
	.var player1JoystickRead = $0000
	.var player2JoystickRead = chkdointeraction //:$0000
	
.import source "macros.asm" 
.import source "AI.asm"	
	
* = $0801 "Basic upstart"
:BasicUpstart($0810)		

* = $0810 
		lda introSong
		sta currentSong
		lda $d012
		eor $dc04
		sbc $dc05
		sta randomSeed

.import source "setupandstabilizeirq.asm"		

frameSwitch:	jmp picStart1

.print "entrypoint: " + picStart1
.print "exitPoint: " + end
.print "change song:"+ changeSong
.print "colorStageA:"+ colorStageA


.var screencodesize = *

.import source "screencode.asm"
start2PlayerGame:
{
	:clearScreenForGame()

/* ----------------- setting player 1 to AI ----------------*/   	
	lda #<play1
	sta player1EntryPoint
	sta afterPauseGoto
	lda #>play1
	sta player1EntryPoint + 1
	sta afterPauseGoto + 1
	
	lda #<default_close_stuff1a
	sta defaultCloseSwitch
	sta noInteractionValue
	lda #>default_close_stuff1a
	sta defaultCloseSwitch + 1
	sta noInteractionValue + 1

	lda #<chkdointeraction1
	sta afterMoveSwitch
	sta afterMoveValue
	sta interactionValue
	
	lda #>chkdointeraction1
	sta afterMoveSwitch + 1
	sta afterMoveValue + 1
	sta interactionValue + 1
 /*--------------------------------------------------------*/
	
	lda musicChoice
	bne setMusic2
	getRandom()
	and #$07
	tax
	lda playList,x
	sta currentSong
	jmp endMusic
setMusic2:	
	sec
	sbc #$01
	sta currentSong
endMusic:	

	lda player1EntryPoint
	sta afterPauseGoto
	lda player1EntryPoint + 1
	sta afterPauseGoto + 1
	
	
	lda #<changeSong
	sta stateSwitch
	lda #>changeSong
	sta stateSwitch+1
	jmp endPlayer1Setup
}

start1PlayerGame:
{
	:clearScreenForGame()

/* ----------------- setting player 1 to AI ----------------*/   	
	lda #<play1_ai
	sta player1EntryPoint
	sta afterPauseGoto
	lda #>play1_ai
	sta player1EntryPoint + 1
	sta afterPauseGoto + 1
	
	lda #<skipAI
	sta defaultCloseSwitch
	sta noInteractionValue
	lda #>skipAI
	sta defaultCloseSwitch + 1
	sta noInteractionValue + 1

	lda #<aiInteraction
	sta afterMoveSwitch
	sta afterMoveValue
	sta interactionValue
	
	lda #>aiInteraction
	sta afterMoveSwitch + 1
	sta afterMoveValue + 1
	sta interactionValue + 1
 /*--------------------------------------------------------*/
	lda musicChoice
	bne setMusic2
	getRandom()
	and #$07
	tax
	lda playList,x
	sta currentSong
	jmp endMusic
setMusic2:	
	sec
	sbc #$01
	sta currentSong
endMusic:	
	lda player1EntryPoint
	sta afterPauseGoto
	lda player1EntryPoint + 1
	sta afterPauseGoto + 1
	
	lda #<changeSong //play1LocationLo
	sta stateSwitch
	lda #>changeSong//play1LocationHi
	sta stateSwitch+1
}	
endPlayer1Setup:	
/**	---------------------------------------------------------------------------------
	PAL C64C's suffer from 'GREY PIXEL BUG'
	annoying ghost pixels on the border between two st(axy) $d020 instructions 
	even when there is no color change
	Breadbin C64 don't have that problem
	The color of the ghost pixels is light grey so I use light grey as background color (=the color of the even inbewteen lines where nothing is drawn) 
	On PAL C64C you now see vertical lines of dots but they are less of a problem compared with other backgeound colors     
-------------------------------------------------------------------**/	
	lda #LIGHT_GREY
//------------------------------------------------------------------	
	sta backgroundColorZp
	jmp end

restartIntro:
	:clearScreenForIntro()
	lda #0
	sta score1
	sta score2
	sta energyRegainCount1
	sta energyRegainCount2
	sta passivityCount1
	sta passivityCount2
	sta stunned1
	sta stunned2
	sta subTimer
	sta computerLevel
	sta stepqueuecounter_1
	sta stepqueuecounter_2
	sta queuecounter1
	sta queuecounter2

	lda timerHiPerLevel
	sta timerHi
	lda timerLoPerLevel
	sta timerLo
	
	lda #1
	sta doCountDown
	sta doDrawEnergy
	sta doDrawPlayers
	//sta playerCount	
		
	lda #startPos1
	sta pos1
	lda #startPos2
	sta pos2
	lda #startPos2-startPos1
	sta dist
	lda #6
	sta energy1
	sta energy2
	
	lda interactionValue
	sta afterMoveSwitch 
	lda interactionValue + 1
	sta afterMoveSwitch + 1

	lda #1
	sta doHitTest

	lda #0
	sta $Dc00	//switch off keyboard/joystick read until pause time has elapsed

	lda #INTRO_PAUSE_TIME
	sta introPause
	
	queueAnimation(List().add(Animation(bow1,5),  Animation(bow2,10),	Animation(bow1,2)),
								PosChange(0,3),2,1 )
	queueAnimation(List().add(Animation(bow1,5),  Animation(bow2,10),	Animation(bow1,2)),
								PosChange(0,3),2,2 )								
	
	lda #<startAnimation
	sta animationPos
	lda #>startAnimation
	sta animationPos + 1

	

	/* ----------------- setting player 1 to AI ----------------*/   
	
	lda #<checkJoystick
	sta player1EntryPoint
	sta afterPauseGoto
	lda #>checkJoystick
	sta player1EntryPoint + 1
	sta afterPauseGoto + 1

	
	lda #0
	sta currentSong
	
	lda #<changeSong
	sta stateSwitch
	lda #>changeSong
	sta stateSwitch+1
jmp end




.import source "intro.asm"			

tableTen:	.byte 0, 10, 20, 30 ,40, 50 , 60 , 70 ,80 , 90 ,100

//wait time before intro screen responds to joystick
//to prevent you go from end game to intro to start in one click 
introPause:	.byte INTRO_PAUSE_TIME

score: .byte 0,0

// ===================================
// 		PLAYER ONE (COMPUTER) 
// =====================================

stepqueuecounter_1:
		.byte 0
stepqueuecounter_2:
		.byte 0
stepqueuelo_1:
		.fill 64,0
stepqueuehi_1:
		.fill 64,0
stepqueuelo_2:
		.fill 64,0
stepqueuehi_2:
		.fill 64,0

currentEffect1: 	.byte 0
currentEffect2: 	.byte 0
doHitTest:			.byte 1

/* 
--------------------	Computer player  -------------------------------------------
*/
play1_ai: 
	//inc $d020
	inc passivityCount1
	recuperate(1, true)
	hitPixelTest()
	drawEnergy(1)
	drawpose(1)		//always start with a draw
	drawpose(2)		//always start with a draw
	drawNumber(1)
	move_and_ani_macro(1, skipAI)

//afterMoveAI:
	jmp (afterMoveSwitch) //* + 3

aiInteraction:		
		newAI()
skipAI:	
	//hittest1//(1) 	
	
play1end:
	//dec $d020
		lda #<play2		
		sta stateSwitch		
		lda #>play2		
		sta stateSwitch+1	
		jmp end

//============== to easily switch between human player 1 and ai some points are indirect jumps 
player1EntryPoint:	.byte <play1_ai, >play1_ai  
defaultCloseSwitch: .byte <skipAI, >skipAI
//the point where you can turn if player/ai can interact  on or off 
afterMoveSwitch: .byte <aiInteraction, >aiInteraction
afterMoveValue: .byte <aiInteraction, >aiInteraction
noInteractionValue: .byte <skipAI, >skipAI
interactionValue: .byte <aiInteraction, >aiInteraction
afterPauseGoto:	.byte play1_ai, >play1_ai 
//========================================
//				PLAYER 1 (HUMAN)
//=========================================

play1: 
		inc passivityCount1
		recuperate(1, false) 
		hitPixelTest()
			drawEnergy(1)
			drawpose(1)
			drawpose(2)
			drawNumber(1)
			move_and_ani_macro(1, default_close_stuff1a)
after_move1a:			
			jmp (afterMoveSwitch)			
chkdointeraction1:	
			chkdointeractionmacro(1)	
default_close_stuff1a:						
		//hittest1()		
checkend1:			
		lda #<play2		
		sta stateSwitch		
		lda #>play2		
		sta stateSwitch+1	
		jmp end





//========================================
//				PLAYER 2  (HUMAN) 
//=========================================
play2: 
	//inc $d020
		inc passivityCount2
		timerCountDown()
		recuperate(2, false)
		checkPassivity()
		calcHitEffect1()
		calcHitEffect2()
			drawEnergy(2)
			//drawpose(2)
			drawNumber(2)
			move_and_ani_macro(2, checkend)
after_move2a:			
			jmp chkdointeraction			
chkdointeraction:	
			chkdointeractionmacro(2)	
default_close_stuff2a:						
			//(2)						
checkend:	lda player1EntryPoint		
			sta stateSwitch		
			lda player1EntryPoint + 1		
			sta stateSwitch+1					
		AIPreparation()			
		tune4()	
	//dec $d020			
			jmp end


.import source "animationsource.asm"
.import source "drawScore.asm"
.import source "changeSong.asm"

doNothingIrq:
	dec pause
	beq !+
	jmp end
!:	lda afterPauseGoto	
	sta stateSwitch 	
	lda afterPauseGoto + 1	
	sta stateSwitch + 1	
	jmp end
pause:		.byte 0

.import source "joytranslate.asm"

score1:			.byte 0
score2: 		.byte 0

energy1:		.byte 6
energy2:		.byte 6

regainThresholdPerLevel:	.byte $30, $28, $20, $18, $14, $12, $10//.byte $1f, $18, $10, $08, $04, $02, $01

energyRegainCount1:	.byte 0
energyRegainCount2:	.byte 0

passivityCount1:	.byte 0
passivityCount2:	.byte 0

stunned1:			.byte 0
stunned2:			.byte 0

subTimer:		.byte 0
timerHi:		.byte 6
timerLo:		.byte 0
timerHiPerLevel:		.byte 6,5,4,4,3,3
timerLoPerLevel:		.byte 0,0,5,0,5,0
blinker:		.byte $ff

pos1:		.byte startPos1			
pos2:		.byte startPos2			
dist:		.byte startPos2-startPos1
			
pos1Old:	.byte startPos1			
pos2Old:	.byte startPos2			
frame1:		.byte 0
frame2:		.byte 0
prevFrame1:	.byte 0
prevFrame2:	.byte 0
posChangeMoment1:	.byte 0
posChangeMoment2:	.byte 0
effectMoment1:		.byte 0
effectMoment2:		.byte 0
posChange1:			.byte 0
posChange2:			.byte 0
nextStance1:		.byte 0
nextStance2:		.byte 0
stance1:			.byte 12
stance2:			.byte 12
//used for collision detection, will be filled with pixel positions when drawn, ends with $ff
player1Pose:		.fill 36, $ff
player2Pose:		.fill 36, $ff
hitPoint1:			.byte 0
hitPoint2:			.byte 0
player1HitsPlayer2:	.byte 0
player2HitsPlayer1:	.byte 0

passivityThreshold1:	.byte PENALTY_TIME
passivityThreshold2:	.byte PENALTY_TIME


queuecounter1:		.byte 30
animationqueue1:	.fill 25,bow1
					.fill 15,bow2
					.fill 15,bow1
					.fill 5, neutral
queuecounter2:		.byte 30
animationqueue2:	.fill 25,bow1
					.fill 15,bow2
					.fill 15,bow1
					.fill 5, neutral
animationDelay1:	.byte 0
animationDelay2:	.byte 0

//if not zero the timer will count down
doCountDown:		.byte 1
//if not zero players will be drawn 
doDrawPlayers:		.byte 1
doDrawEnergy:		.byte 1
doPassivityCheck:	.byte 1

currentRow: 	.byte 0

state:			.byte 0
.align 		$100				
stateSwitch:	.byte <restartIntro, >restartIntro 
historyMoves: 	.fill 256,random()
historyCounter:	.byte 0
computerDelay:	.byte 8
computerDelayPerLevel: .byte 8,6,4,2,1,0,0
computerLevel:	.byte 0
doActionAbove:	.byte 6,8,12,17,26,30,30

doSomething:		.byte 0 


passiveBehaviorCount: 	.byte 0
retreatBehaviorCount: 	.byte 0
advanceBehaviorCount: 	.byte 0
attackBehaviorCount: 	.byte 0
defendBehaviorCount: 	.byte 0
advanceAndAttackBehaviorCount:	.byte 0
totalBehaviorCount:		.byte 0		
chosenBehavior:			.byte 0
//TODO 		could be smaller
behaviorTable:			.fill 128,0
//TODO generate table  
behaviorCountAndValue: .fill 256, i<4?3:i<8?7:i<16?15:i<32?31:i<64?63:i<128?127:255

timestwelve: .for(var i=0; i<21; i++){	.byte i*12	}
timesthirteen: .fill 13,  i*13	

soundEffects:	.byte 0
introSong:		.byte 0
onePlayerSong:	.byte 1
twoPlayerSong:	.byte 2
playList:		.byte 0,1,2,1,2,2,1,3
musicChoice:	.byte 0
//--------------  sprite poses  ---------------- 
.import source "spritedefs.asm"

//------------------------------------------------------------			
//optimum defense move for attack move
optimumDefense:
//attack:	  0  1  2  3  4  5	
      //.byte 8, 7, 9, 9, 8, 11, 255, 255, 255, 255, 255, 255, 255
		.byte 6,7, 9,10, 8, 11,  255, 255, 255, 255, 255, 255, 255             
/*
0	medkick
1	highkick
2	lowkick
3	lowpunch
4	punch
5	jumpkick
6	backwardsdefence
7	highdefense
8	normaldefense
9	smalljumpdefense
10	highjumpdefense
11	crouch
12	neutral

*/


.import source "randomtables.asm"			
effecttable:			
.import source "effecttable_safe.asm"
.import source "effectTable2.asm"
disttable:			
.import source "disttable.asm"

.import source "musicdata.asm"

pixelSource:
		.byte o,o,o,o,o,o,o,o,o,o,o,o
		.byte o,o,o,o,o,o,o,o,o,o,o,o
		.byte o,o,o,o,o,o,o,o,o,o,o,o
		.byte o,o,o,o,o,o,o,o,o,o,o,o
		.byte o,o,o,o,o,o,o,o,o,o,o,o
		.byte o,o,o,o,o,o,o,o,o,o,o,o
		.byte o,o,o,o,o,o,o,o,o,o,o,o
		.byte o,o,o,o,o,o,o,o,o,o,o,o
		.byte o,o,o,o,o,o,o,o,o,o,o,o
		.byte o,o,o,o,o,o,o,o,o,o,o,o
		.byte o,o,o,o,o,o,o,o,o,o,o,o
		.byte o,o,o,o,o,o,o,o,o,o,o,o
		.byte o,o,o,o,o,o,o,o,o,o,o,o
		.byte o,o,o,o,o,o,o,o,o,o,o,o
		.byte o,o,o,o,o,o,o,o,o,o,o,o
		.byte o,o,o,o,o,o,o,o,o,o,o,o
		.byte o,o,o,o,o,o,o,o,o,o,o,o
		.byte o,o,o,o,o,o,o,o,o,o,o,o
		.byte o,o,o,o,o,o,o,o,o,o,o,o
		.byte o,o,o,o,o,o,o,o,o,o,o,o
		.byte o,o,o,o,o,o,o,o,o,o,o,o
		.byte o,o,o,o,o,o,o,o,o,o,o,o
		.byte o,o,o,o,o,o,o,o,o,o,o,o

//values for debugging sound routine
//		.byte o,X,o,A,o,X,o,A,o,X,o,A
//		.byte o,X,o,A,o,X,o,A,o,X,o,A
//		.byte o,X,o,A,o,X,o,A,o,X,o,A
//		.byte o,X,o,A,o,X,o,A,o,X,o,A
//		.byte o,X,o,A,o,X,o,A,o,X,o,A
//		.byte o,X,o,A,o,X,o,A,o,X,o,A
//		.byte o,X,o,A,o,X,o,A,o,X,o,A


colorStageA:
		.fill 21, GREY
colorStageX:
	.fill 21, GREEN
colorStageY:
		.fill 21,LIGHT_GREY

.align $100
numberdefs:
	.import source "numberdefs.asm"

convertHitTable1:
	.byte 0,6,7,8,4,10,11,0,0,0,11,0,0,0,0
convertHitTable2:
	.byte 0,1,2,3,9,5,11,0,0,0,0,11,0,0,0,0

.macro copySourceToZeropage() {
	ldx #0
!:	lda pixelSource,x
	sta zpStart,x
	inx
	cpx #252
	bne !-
}



