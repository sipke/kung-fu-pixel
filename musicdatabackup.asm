//========= INSTRUMENTS ==================
//1:triang
//2:pulse
//3:pulsewide
//4:pulsesml
//5:pulsechrd
//6:leadwrel
//7:hihat
//8:ting
//9:empty24

// instrument               0    1    2    3    4    5    6    7    8
.var pagecheckstart
.var pagecheckend
adtable:            .byte  $63, $00, $00, $80, $33, $80, $02, $00, $00
srtable:            .byte  $f0, $68, $f0, $fe, $80, $fe, $00, $dc, $00
wavetable:          .byte  $11, $41, $41, $51, $51, $41, $81, $15, $00
pulsetablehi:       .byte  $00, $08, $08, $01, $02, $0a, $00, $00, $00
pulsetablelo:       .byte  $00, $00, $00, $10, $10, $11, $00, $00, $00
pulsevibrato:       .byte  $00, $00, $20, $00, $00, $00, $00, $00, $00
pattern40:
   :Event("instr1",13,12)
   :Event("instr6",84,12)
   :Event("noteon",72,12)
   :Event("noteon",84,12)
   :Event("instr1",25,12)
   :Event("instr6",84,12)
   :Event("noteon",72,12)
   :Event("noteon",84,12)
   :Event("instr1",30,12)
   :Event("instr6",84,6)
   :Event("noteon",60,6)
   :Event("instr1",27,12)
   :Event("instr6",72,6)
   :Event("noteon",48,5)
   :Event("end",1,1)
pattern7:
   :Event("instr8",0,143)
   :Event("end",1,1)
pattern41:
   :Event("instr1",49,72)
   :Event("noteon",56,12)
   :Event("noteon",54,54)
   :Event("noteoff",7,149)
   :Event("end",1,1)
patternf:
   :Event("instr0",66,6)
   :Event("noteon",61,6)
   :Event("noteon",61,6)
   :Event("noteon",56,6)
   :Event("noteon",54,6)
   :Event("noteon",51,6)
   :Event("noteon",66,6)
   :Event("noteon",61,6)
   :Event("noteon",61,6)
   :Event("noteon",56,6)
   :Event("noteon",54,6)
   :Event("noteon",51,6)
   :Event("noteon",66,6)
   :Event("noteon",61,6)
   :Event("noteon",61,6)
   :Event("noteon",56,6)
   :Event("noteon",54,6)
   :Event("noteon",51,6)
   :Event("noteon",66,6)
   :Event("noteon",61,6)
   :Event("noteon",61,6)
   :Event("noteon",56,6)
   :Event("noteon",54,6)
   :Event("noteon",51,5)
   :Event("end",1,1)
pattern10:
   :Event("instr2",27,287)
   :Event("end",1,1)
pattern11:
   :Event("instr2",32,287)
   :Event("end",1,1)
pattern42:
   :Event("instr1",49,72)
   :Event("noteon",56,12)
   :Event("noteon",58,54)
   :Event("noteoff",7,149)
   :Event("end",1,1)
pattern43:
   :Event("instr1",63,6)
   :Event("noteon",61,6)
   :Event("noteon",58,6)
   :Event("noteon",56,6)
   :Event("noteon",61,6)
   :Event("noteon",58,6)
   :Event("noteon",56,6)
   :Event("noteon",54,6)
   :Event("noteon",58,6)
   :Event("noteon",56,6)
   :Event("noteon",54,6)
   :Event("noteon",51,6)
   :Event("noteon",56,6)
   :Event("noteon",54,6)
   :Event("noteon",51,6)
   :Event("noteon",49,6)
   :Event("noteon",49,6)
   :Event("noteon",51,6)
   :Event("noteon",54,6)
   :Event("noteon",56,6)
   :Event("noteon",54,6)
   :Event("noteon",56,6)
   :Event("noteon",58,6)
   :Event("noteon",61,6)
   :Event("noteoff",7,24)
   :Event("instr8",75,119)
   :Event("end",1,1)
pattern45:
   :Event("instr8",13,36)
   :Event("instr7",61,6)
   :Event("noteoff",7,101)
   :Event("end",1,1)

//      ---- song1----
.eval pagecheckstart = *
song1Voice1Lo:  .byte <patternf
song1Voice1Hi:  .byte >patternf,0
.eval pagecheckend = *-1
 .assert " pagecross ", floor(pagecheckstart/256),floor(pagecheckend/256) 
.eval pagecheckstart = *
song1Voice2Lo:  .byte <pattern10,<pattern11
song1Voice2Hi:  .byte >pattern10,>pattern11,0
.eval pagecheckend = *-1
 .assert " pagecross ", floor(pagecheckstart/256),floor(pagecheckend/256) 
.eval pagecheckstart = *
song1Voice3Lo:  .byte <pattern7
song1Voice3Hi:  .byte >pattern7,0
.eval pagecheckend = *-1
 .assert " pagecross ", floor(pagecheckstart/256),floor(pagecheckend/256) 
//on the first song set first patterns for every voice
//      ---- song0----
.eval pagecheckstart = *
song0Voice1Lo:  .byte <pattern40
song0Voice1Hi:  .byte >pattern40,0
.eval pagecheckend = *-1
 .assert " pagecross ", floor(pagecheckstart/256),floor(pagecheckend/256) 
.eval pagecheckstart = *
song0Voice2Lo:  .byte <pattern7,<pattern7,<pattern7,<pattern41,<pattern42,<pattern43,<pattern7,<pattern7
song0Voice2Hi:  .byte >pattern7,>pattern7,>pattern7,>pattern41,>pattern42,>pattern43,>pattern7,>pattern7,0
.eval pagecheckend = *-1
 .assert " pagecross ", floor(pagecheckstart/256),floor(pagecheckend/256) 
.eval pagecheckstart = *
song0Voice3Lo:  .byte <pattern7,<pattern7,<pattern7,<pattern7,<pattern7,<pattern7,<pattern7,<pattern7,<pattern45,<pattern7,<pattern7
song0Voice3Hi:  .byte >pattern7,>pattern7,>pattern7,>pattern7,>pattern7,>pattern7,>pattern7,>pattern7,>pattern45,>pattern7,>pattern7,0
.eval pagecheckend = *-1
 .assert " pagecross ", floor(pagecheckstart/256),floor(pagecheckend/256) 