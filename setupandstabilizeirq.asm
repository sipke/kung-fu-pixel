			jsr $e544			//clear screen

			lda # betweenLineColor
			sta backgroundColorZp
			
     		lda #$35
    		sta $01		// turn off basic and kernel
    		
             sei  	
             lda #<int
             sta $fffe // can't use 0314/5 pointers with kernel turned off 
             lda #>int
             sta $ffff        
             
             lda #$7f
             sta $dc0d        	// Timer off
             sta $dd0d
			lda #firstLine
			sta $d012
			lda $d011
			and #$6f
			sta $d011
             lda #$01
             sta $d01a        // enable raster interrupt 
             lda $dc0d 	// ack CIA interrupts
             lsr $d019	// ack video interrupts
            :copySourceToZeropage() 
        	//sound setup

			lda #$0f
			sta $d418    
           
            
			cli
			jmp *

int:		sei
			lda #<int2														//				2
             sta $fffe														// 				6
             lda #>int2													//					8								
             sta $ffff        					//											12

  			 inc $d012			// raster interrupt at next line					timing: 	18										
             lda #$01														//				20
             sta $d019			// acknowledge irq											24
             tsx 				// not normal ending of irq, so stack pointer 				26
             					// will be broken, so save stack pointer 					
             cli									//										28
             
             nop									//										
             nop
             nop				
             nop
             nop
             nop
             nop
             nop								// 									8x2= 	44
		//                                             interrupt handling:		20-27 ->64-71 cycles 
             nop
             nop
    
    //--------------------------------------------------
    	// by now irq has been triggered while performing nop's 
int2:     	txs					// repair stack pointer 
    		ldx #$08
    		dex
    		bne *-1
    		bit $00				// a three cycles 'neutral' instruction
    		lda $d012			// see if  
    		cmp $d012			// raster changes at this point
    		beq *+2				// if not-> do branch, which takes 1 cycle more than not taking branch
              
    //--------------------------------------------------
    //now it's stable  
	//wait some cycles
	.fill 26, $ea // 55 cycles might be enough time  to set timers
	bit $01 						
	//idea set timer A to 63 cycles, timer B counts timer A overflow, timer B high byte can hopefully
	//be set to stable value, so jmp (timerB) will jump to table values every 2 lines
	 						

