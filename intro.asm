.const UP_DOWN_DELAY_VALUE = 10 
upDownDelay:	.byte UP_DOWN_DELAY_VALUE
playerCount: .byte 1

preChooseNumberOfPlayers:

//init stuff		
		lda #1
		sta doDrawPlayers
		sta doDrawEnergy
		sta doCountDown
		
		ldy #maxEnergy
		sty energy1		
		sty energy2		
		lda #0	
		sta score1	
		sta score2	
		sta passivityCount1
		sta passivityCount2
		sta player1HitsPlayer2
		sta player2HitsPlayer1
		sta computerLevel		
		sta stunned1		
		sta stunned2		
		lda computerDelayPerLevel
		sta computerDelay

		lda #1
		sta doHitTest
		
		lda #1
		sta pos1
		lda #7
		sta pos2

		lda interactionValue
		sta afterMoveSwitch 
		lda interactionValue + 1
		sta afterMoveSwitch + 1

		setJumpTo(after_move2, player2JoystickRead)

		drawMusicNote()

		lda #<chooseNumberOfPlayers
		sta stateSwitch
		lda #>chooseNumberOfPlayers
		sta stateSwitch + 1
		jmp end

chooseNumberOfPlayers:
			drawMusicSymbol()

!:			ldx #$ff	
			stx joystick2
	//
			lda joystick2
			and #%00000100						
			bne !+			
			lda #commodore
			sta frame1
			lda #1
			sta playerCount
			jmp drawplayers			
!:			lda joystick2			
			and #%00001000			
			bne  joyDown			
			lda #neutral
			sta frame1
			lda #2
			sta playerCount
			jmp drawplayers			

//sound on or off						
joyDown:	lda upDownDelay
			beq !+	
			dec upDownDelay	
			jmp endJoy	
!:			lda joystick2			
			and #%00000010			
			bne  joyUp					
			lda #UP_DOWN_DELAY_VALUE					
			sta upDownDelay					
			ldx musicChoice					
			dex					
			bpl !+					
			ldx #5					
!:			stx musicChoice 
			jmp drawplayers			
joyUp:			lda joystick2			
			and #%00000001			
			bne endJoy					
			lda #UP_DOWN_DELAY_VALUE					
			sta upDownDelay					

			ldx musicChoice					
			inx					
			cpx #6					
			bne !+				
			ldx #0					
!:			stx musicChoice 
			jmp drawplayers
			
endJoy:		lda pauseAnimation
			beq !+
			dec pauseAnimation
			jmp drawplayers
!:			lda joystick2			
			and #%00010000
			bne drawplayers
			jmp startGameStart 			
drawplayers:			
			drawpose(1)
			drawpose(2)
			jmp end			

startGameStart:
		lda #startPos1
		sta pos1
		lda #startPos2
		sta pos2
		lda #startPos2-startPos1-1
		sta dist

		lda #neutral
		sta frame1
		lda #INTRO_PAUSE_TIME
		sta introPause
		lda playerCount
		cmp #1
		bne !+
		jmp start1PlayerGame
!:		jmp start2PlayerGame

checkJoystick:			
	//this next bit is necessary, but why?, oh data direction I guess
			ldx #$ff	
			stx joystick2
	//
			lda introPause
			bmi !+
  			dec introPause 	
!:	
			lda joystick2			
			and #%00010000			
			beq  !+			
			jmp doNextFrame			
!:			lda #<preChooseNumberOfPlayers
			sta stateSwitch
			lda #>preChooseNumberOfPlayers
			sta stateSwitch + 1
			lda	playerCount
			cmp #1
			beq setToOnePlayer
		 	lda #neutral
			jmp !+
setToOnePlayer:		 
			lda #commodore
!:			sta frame1
			lda #neutral
			sta frame2
			lda #0
			sta pos1
			
			lda #50				//wait 1 second before reading joytsick button again
			sta pauseAnimation
			
			clearScreenForPlayerChoose()
			
			jmp end
			
doNextFrame:	

{
		lda pauseAnimation
		beq !+
		dec pauseAnimation
		jmp end
		lda #255
		sta $Dc00	//switch off keyboard/joystick read until pause time has elapsed
!:		lda animationPos
		sta $fe
		lda animationPos + 1
		sta $ff
		ldy #0
		lda ($fe),y 
		cmp  #$fd
		bne checkColorChange
//		pause		
		ldy #1		 
		lda ($fe),y
		sta pauseAnimation
		clc
		lda $fe
		adc #$02
		sta animationPos
		sta $fe
		lda $ff
		adc #$00
		sta animationPos + 1
		sta $ff
		ldy #0
		lda ($fe),y 
		jmp end
checkColorChange:		
		cmp #$fe
		bne checkEnd
		ldy #1
		lda ($fe),y
		sta drawColor
		clc
		lda $fe
		adc #$02
		sta animationPos
		sta $fe
		lda $ff
		adc #$00
		sta animationPos + 1
		sta $ff
		ldy #0
		lda ($fe),y 
		jmp doDraw
checkEnd:
		cmp #$ff
		bne doDraw
		lda #<startAnimation
		sta animationPos
		lda #>startAnimation
		sta animationPos + 1
		jmp end
doDraw: tax
		lda drawColor
		sta zpStart,x
		inc animationPos
		bne end
		inc animationPos + 1
end: 
}

			lda #backgroundColor
			sta backgroundColorZp
			//inc currentFrame	
						
//setup for next routine: 49 cycles
end:

    		lda #firstLine
    		sta $d012
    		
			lda $d011
			and #$7f
			sta $d011

    		lda #1
		    sta $D019
			sta $D01A

			lda #<int
			sta $fffe //0314
			lda #>int
			sta $ffff //0315
			
debugAdress:		
			
			cli

			jmp *

.import source "startscreens.asm" 