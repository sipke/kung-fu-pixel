.import source "main.sym"

.var brkFile = createFile("debug.txt") 

.macro break() {
	.eval brkFile.writeln("break " + toHexString(*))
}


//POKE FILE START AT $0400?


//computerDelayPerLevel: .byte 8,6,4,2,1,0,0
//regainThresholdPerLevel:	.byte $30, $28, $20, $18, $14, $12, $10
//playList:		.byte 0,1,2,3,4,5,6,7
//energy needed for every possible move
//energyNeeded:             .byte 1,2,1,1,1,2,0,0,0,1,1,0,0

.var emptyline = "                                        "
.var noattack = "poke " + (noAttackCheat+1).string() + ",128: rem no computer attacks"
.eval noattack = noattack + emptyline.substring(noattack.size(),40)

.var penaltytime1 =  "poke " + passivityThreshold1.string() + ",x: rem default =160"
.eval penaltytime1 = penaltytime1 +  emptyline.substring(penaltytime1.size(),40)
                                //    1234567890123456789012345678901234567890
.eval penaltytime1 = penaltytime1 +  "rem x: time before player 1 gets penalty"

.var penaltytime2 =  "poke " + passivityThreshold2.string() + ",x: rem default =160" 
.eval penaltytime2 = penaltytime2 +  emptyline.substring(penaltytime2.size(),40)
                                //    1234567890123456789012345678901234567890
.eval penaltytime2 = penaltytime2 +  "rem x: time before player 2 gets penalty"

.var speedCheat =  "poke " + doActionAbove.string() + "+i,x;rem speed per level"
.eval speedCheat = speedCheat +  emptyline.substring(speedCheat.size(),40)
						    //    1234567890123456789012345678901234567890
.eval speedCheat = speedCheat +  "rem x: computer speed (1-30)            "
.eval speedCheat = speedCheat +  "rem i: computer level (0-4)             "

//super simple writer

.pc = $0801 "Basic upstart"
:BasicUpstart($0810)		
*=$0810
	lda #BLUE
	sta $d020
	jsr $e544 //clr screen
loop:

	lda #160
t0:	sta $0400
	ldx #0
!:	inx
	bne !- 
	ldx #0
!:	inx
	bne !- 
	ldx #0
!:	inx
	bne !- 
	ldx #0
!:	inx
	bne !- 

s:	lda poketext
	cmp #$ff
	beq done
t:	sta $0400
	ldx #0
!:	inx
	bne !- 
	inc s + 1
	bne !+
	inc s+2
!:	inc t + 1
	bne !+
	inc t + 2
!:	inc t0 + 1
	bne !+
	inc t0 + 2
!:	jmp loop
done:	
!:	lda $dc01
	and #%00010000	
	bne !-	
	//move cursor to bottom screen
	lda #0
	sta 211
	lda #22
	sta 214
	rts
	
poketext:

//	   1234567890123456789012345678901234567890
.text "                                        "
.text "       kung fu pixel                    "
.text "        poke file                       "
.text "                                        "
.text "   try these pokes to change the game:  "
.text "                                        "
.text "                                        "
.text noattack
.text "                                        "
.text penaltytime1

.text "                                        "
.text penaltytime2

.text "                                        "
.text speedCheat


.text "                                        "
.text "                                        "
.text "  (space to close this)                 "
	.byte $ff
//	   1234567890123456789012345678901234567890

	