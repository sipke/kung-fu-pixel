.const DELAY_AND_VALUE = 31 

.macro newAI(){		

!:				
randomizer: 
{		
		getRandom()
		tay		
/* quote: To get a different chain (essentially a different PRNG altogether), the value of the EOR would have to be changed
   $1d (29),$2b (43),$2d (45),$4d (77),$5f (95),$63 (99),$65 (101),$69 (105),$71 (113),$87 (135),$8d (141),$a9 (169),$c3 (195),$cf (207),$e7 (231),$f5 (245)*/
			
}		
		
		lda doSomething
		bne !+
		jmp (defaultCloseSwitch)
/*		
		dec computerDelay
		beq !+
		jmp (defaultCloseSwitch)	
		//reset delay value	
!:		ldx computerLevel
		lda computerDelayPerLevel,x
		sta computerDelay
*/		
!:		tya		//retrieve random
		//x <- total number of possible behaviors
		ldx totalBehaviorCount
		lda behaviorCountAndValue,x		//find AND-value in table that contains all numbers up to total
		sta sm1 + 1						//so for instance, with 6 total behaviors, the AND value = 7
		lda randomSeed
sm1:	and #$ff						//random number now contains number up to AND-value
		cmp totalBehaviorCount			//could still be greater than totalBehaviorCount 
		blt !+
		lsr								// in that case move bits to right = divide by 2, thats always less than totalBehaviorCount  
!:		tay								// is now random index for possible behaviors, there is a slight bias to index values less than half of AND-value, oh well 
		lda behaviorTable,y
		bne !+
		jmp passive		 
!:		cmp #1
		bne !+
		jmp retreat
!:		cmp #2
		bne !+
		jmp advance
!:		cmp #3
		bne defend
		jmp attack	
!:		cmp #5
		bne defend
		jmp advanceAndAttack	


defend:			
	//if energy zero: choose random from zero energy defense moves
		lda energy1
		bne !+
		ldx randomSeed	
		lda randomZeroEnergyDefenseMove,x	
		jmp doAni1
!:		lda randomSeed			
		and #HISTORY_AND_VALUE			
		tax			
		lda historyMoves,x			
		tax			
		lda optimumDefense,x			
		jmp doAni1 		
			
passive:		
		//inc passivityCount1		
			jmp endAi	
retreat:
			:queueAnimation(List().add(Animation(neutral,1), Animation(walk1,2),  Animation(neutral,1))
					, PosChange(-1,3),0,1 )
		//inc passivityCount1
			jmp endAi
advance:
			:queueAnimation(List().add(Animation(neutral,1), Animation(walk1,2),  Animation(neutral,1))
					, PosChange(1,3),0,1 )						
		//inc passivityCount1						
			jmp endAi
attack:
			mov #0:passivityCount1
			lda randomAttackMove,x
	//TODO check if player already has defense pose that defeats that attack				
			tax				
			cmp optimumDefense,x	//has player 2 already optimum defense for that attack?>					
			bne notDefended					
		break()	
			sec						//yes: then just subtract 1 	
			sbc #$01					
			bpl notDefended					
			lda #$05					
notDefended:			
			jmp doAni1		
advanceAndAttack:
			mov #0:passivityCount1
			lda #5
			jmp doAni1		

		
doAni1:	tax
		lda energy1
		sec 
		sbc energyNeeded,x
		bcs !+
		jmp endAi	//no energy for move, will do nothing 
!:		sta energy1
		lda #0					//reset energyregaincount
		sta energyRegainCount1
		:setAni(1)
		jmp endAi
endAi:
}
 
.var passiveBehavior = 0
.var retreatBehavior = 1
.var advanceBehavior = 2
.var attackBehavior = 3
.var defendBehavior = 4
.var advanceAndAttackBehavior = 5
/*passiveBehaviorCount: .byte 0
retreatBehaviorCount: .byte 0
advanceBehaviorCount: .byte 0
attackBehaviorCount: .byte 0
defendBehaviorCount: .byte 0
chosenBehavior:		.byte 0
behaviorCountAndValue: .fill 256, i<4?3:i<8?7:i<15?15:i<31?31:i<63?63:i<127?127:255
*/

//this is done in player (2) turn in preparation of execution in AI (player 1) turn
.macro AIPreparation() {

	//random chance of doing somehting
		getRandom()
		and #DELAY_AND_VALUE
		ldx computerLevel
		cmp doActionAbove,x
		bcc !+
		lda #0
		sta doSomething	//this prevents execution of behavior in computer players turn
		jmp end
!:		
		lda #1
		sta doSomething	//this prevents execution of behavior in computer players turn

		//init values
		movArray(0, List().add(passiveBehaviorCount, advanceBehaviorCount, retreatBehaviorCount, defendBehaviorCount))

@noAttackCheat:		
.var doCheat = cmdLineVars.get("cheat")	
	.if(doCheat == "true") {
		lda #$80
		break()
	}else {
		lda #$0
	}
		sta attackBehaviorCount
		sta advanceAndAttackBehaviorCount

//if far away move closer (maybe calculate if time till penalty is more and it can 'hang back'
		lda dist
		cmp #4
		bcc checkIfCloseEnough
		lda #$80
		sta attackBehaviorCount
		sta advanceAndAttackBehaviorCount
		sta retreatBehaviorCount
		sta defendBehaviorCount
		lda #3
		sta advanceBehaviorCount 	
		jmp done
checkIfCloseEnough:		
		
		lda dist		
		cmp #2
		bne distNotTwo
		cmp #1
		bne distNotTwo
//case: dist = 2 or 1 and other player is attacking: don't walk into attack		
		
		lda stance2
		cmp #6
		bpl distNotTwo
		mov #$80:advanceBehaviorCount
		//now to prevent penalty increase chance of advaanceAndAttack
		inc advanceAndAttackBehaviorCount					
		inc advanceAndAttackBehaviorCount						
distNotTwo:		
		lda dist
		cmp #2
		blt closerThan2Pix
//case further than one pixel away		
furtherThanOnePix:		
 		mov #$80:attackBehaviorCount	//too far away for attack or defend
 		mov #$80:advanceAndAttackBehaviorCount
		mov #$80:defendBehaviorCount
		lda pos1
		bne !+	
		mov #$80:retreatBehaviorCount	//backup not possible
		lda passivityCount1		
		cmp passivityCount2		
		blt	!+	
		mov #$80:passiveBehaviorCount
		mov #$80:retreatBehaviorCount
!:		inc passiveBehaviorCount
		inc retreatBehaviorCount
		//todo don't advance if player 2 is already attacking
		//on higher levels: don't advance if penalty counter is lower
		inc advanceBehaviorCount				
		inc advanceBehaviorCount				
		lda dist				
		cmp #1				
		bne !+				
		inc advanceAndAttackBehaviorCount					
		inc advanceAndAttackBehaviorCount						
!:		jmp done				
closerThan2Pix:			
		mov #$80:passiveBehaviorCount			
!:		lda pos1	//if back to wall, retreat no option 
		bne !+
		mov #$80:retreatBehaviorCount
		lda dist
		bne !+
		mov #$80:advanceBehaviorCount	//advance no use when closest
!:		lda passivityCount2 
		cmp passivityCount1
		blt !+  		
		mov #$80:retreatBehaviorCount
!:		inc retreatBehaviorCount		
		inc advanceBehaviorCount
		inc passiveBehaviorCount
		clc
		lda attackBehaviorCount
		adc #$03
		sta attackBehaviorCount
		inc advanceAndAttackBehaviorCount
		lda defendBehaviorCount
		adc #$06
		sta defendBehaviorCount
		lda energy1
		sec
		sbc energy2
		bmi lessEnergy
		clc
		adc attackBehaviorCount	
		sta attackBehaviorCount	
//if other player is stunned: attack			
		lda	stun2
		beq !+
		lda attackBehaviorCount
		clc
		adc #$06
		sta attackBehaviorCount
!:		jmp done	
lessEnergy:		
		//energy zero: retreat or defend
		lda energy2
		bne energyNotZero
		lda #$80
		sta attackBehaviorCount
		sta advanceAndAttackBehaviorCount
		sta advanceBehaviorCount
		lda retreatBehaviorCount
		adc #$03
		sta retreatBehaviorCount
		inc defendBehaviorCount
		jmp done
energyNotZero:		
		sec
		sbc energy1
		clc
		adc defendBehaviorCount	
		sta defendBehaviorCount
		jmp done	
done:
generateBehaviorTable:
		ldy #0				//total behaviors
		ldx passiveBehaviorCount	//how many chances of passivebehavior?
		beq gbt1
		bmi gbt1					//>#$80, beavior not allowed
		lda #passiveBehavior		
!:		sta behaviorTable,y			//add behavior to table
		iny							// inc total count
		dex	
		bne !-	
gbt1:
		ldx retreatBehaviorCount
		beq gbt2
		bmi gbt2
		lda #retreatBehavior	
!:		sta behaviorTable,y	
		iny	
		dex	
		bne !-	
gbt2:
		ldx advanceBehaviorCount
		beq gbt3
		bmi gbt3
		lda #advanceBehavior	
!:		sta behaviorTable,y	
		iny	
		dex	
		bne !-	
gbt3:
		ldx attackBehaviorCount
		beq gbt4
		bmi gbt4
		lda #attackBehavior	
!:		sta behaviorTable,y	
		iny	
		dex	
		bne !-	
gbt4:
		ldx defendBehaviorCount
		beq gbt5
		bmi gbt5
		lda #defendBehavior	
!:		sta behaviorTable,y	
		iny	
		dex	
		bne !-	
gbt5:	sty totalBehaviorCount
end:
}