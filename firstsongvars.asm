//on the first song set first patterns for every voice
.var firstSongStartPattern1 = pattern1
.var firstSongPatternTable1Lo = song0Voice1Lo
.var firstSongPatternTable1Hi = song0Voice1Hi
.var firstSongStartPattern2 = pattern7
.var firstSongPatternTable2Lo = song0Voice2Lo
.var firstSongPatternTable2Hi = song0Voice2Hi
.var firstSongStartPattern3 = pattern1_a
.var firstSongPatternTable3Lo = song0Voice3Lo
.var firstSongPatternTable3Hi = song0Voice3Hi
