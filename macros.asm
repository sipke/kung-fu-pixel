.macro tune4() {
	//.byte [instrnumber*8]+3, length-2,value, 129, 2
	lda frame1
	cmp prevFrame1 
	beq setNoteToNone1
	tax
	lda frameToScaleConvert,x
	sta patternf0+2
	lda #(12*8)+3
	sta patternf0
	jmp do2
setNoteToNone1:	
	lda #(8*8)+3
	sta patternf0
	lda #0
	sta patternf0 + 2
do2:
//	break()	
	lda frame2
	cmp prevFrame2 
	beq setNoteToNone2
	tax
	lda frameToScaleConvert,x
	clc
	adc #$03
	sta patternf1+2
	lda #(12*8)+3
	sta patternf1
	jmp end
setNoteToNone2:	
	lda #(8*8)+3
	sta patternf1
	lda #0
	sta patternf1 + 2
end:
	lda frame1
	sta prevFrame1
	lda frame2
	sta prevFrame2
}
/*
			MACROS
*/
/* changes a jump address, the A register is chqanged by this*/
.macro setJumpTo(at, to){
	//.print "debug setjumpto" + toHexString(at) + "," +  toHexString(to)  
	lda #< to
	sta at + 1
	lda #> to
	sta at +2
}

.macro queueSteps(player, steps) { 
	lda #steps.size() 
	.if(player==1){
		sta stepqueuecounter_1
		lda #0
		sta queuecounter1
	} else {
		sta stepqueuecounter_2
		lda #0
		sta queuecounter2
	} 

	.for(var i=0; i<steps.size(); i++){
		.if(player==1){
			lda #<steps.get(i)
			sta stepqueuelo_1 + steps.size()-i
			lda #>steps.get(i)
			sta stepqueuehi_1 + steps.size()-i
		} else {
			lda #<steps.get(i)
			sta stepqueuelo_2 + steps.size()-i
			lda #>steps.get(i)
			sta stepqueuehi_2 + steps.size()-i
		} 
	}
	.if(player==1) {
		jmp (defaultCloseSwitch)
	}else{
		jmp default_close_stuff2
	}
}		

.struct Animation {framenr, framecount}
.struct PosChange {poschange, poschangemoment}

 /* macro should be called with x containing the animation number */
.macro setAni(player){
	.var dir 
	.if (player==1){
		.eval dir=1
		stx nextStance1
		//stx stance1
	}else {
		.eval dir=-1
		stx nextStance2
		//stx stance2
	}
			lda stanceJumpTableLo,x
			sta checkJump+1 	
			lda stanceJumpTableHi,x
			sta checkJump+2 	
checkJump:	jmp animjmp0			
	
animjmp0:	//
			:queueAnimation(List().add(Animation(medkick1,3), Animation(medkick2,3), Animation(neutral,1))
							, PosChange(0,5),0,player )						
			jmp endSetAni									
//------------------------------- jump table ---------------------------------------												
stanceJumpTableLo:	.byte <animjmp0,<animjmp1,<animjmp2,<animjmp3,<animjmp4,<animjmp5,<animjmp6,<animjmp7,<animjmp8,<animjmp9,<animjmp10,<animjmp11
stanceJumpTableHi:	.byte >animjmp0,>animjmp1,>animjmp2,>animjmp3,>animjmp4,>animjmp5,>animjmp6,>animjmp7,>animjmp8,>animjmp9,>animjmp10,>animjmp11
//------------------------------------------------------------------------------------												
animjmp1:	:queueAnimation(List().add(Animation(highkick1,3), Animation(highkick2,3), Animation(highkick3,8), Animation(neutral,1))
							, PosChange(0,5),5,player )						
			jmp endSetAni						
animjmp2:				
			:queueAnimation(List().add(Animation(lowkick1,3), Animation(lowkick2,3),Animation(lowkick3,6), Animation(neutral,1))
							, PosChange(0,5),5,player )						
			jmp endSetAni						
animjmp3:				
			:queueAnimation(List().add(Animation(lowpunch1,3), Animation(lowpunch2,3), Animation(neutral,1))
							, PosChange(0,5),3,player )						
			jmp endSetAni						
animjmp4:				
			:queueAnimation(List().add(Animation(punch1,3), Animation(punch2,3), Animation(punch3,5), Animation(neutral,1))
							, PosChange(0,5),3,player )						
			jmp endSetAni						
animjmp5:				
			:queueAnimation(List().add(Animation(jumpkick1,4), Animation(jumpkick2,4), Animation(jumpkick3,4), Animation(jumpkick4,10),  Animation(neutral,2))
							, PosChange(dir,8),6,player )						
			jmp endSetAni						
animjmp6:	//neutral			
			:queueAnimation(List().add(Animation(backwardsdefense,8))
							, PosChange(0,5),2,player )						
			jmp endSetAni						
animjmp7:	//down
			:queueAnimation(List().add(Animation(highdefense,8))
							, PosChange(0,5),2,player )						
			jmp endSetAni									
animjmp8:	:queueAnimation(List().add(Animation(normaldefense,8))
							, PosChange(0,5),2,player )						
											
			jmp endSetAni						
animjmp9:				
			:queueAnimation(List().add(Animation(smalljumpdefense1,4), Animation(smalljumpdefense2,8),Animation(smalljumpdefense1,4),Animation(neutral,1))
							, PosChange(0,5),3,player )						
			jmp endSetAni						
animjmp10:				
			:queueAnimation(List().add(Animation(smalljumpdefense1,4), Animation(smalljumpdefense2,4),Animation(highjumpdefense1,4), Animation(highjumpdefense2,4),Animation(highjumpdefense1,4),Animation(neutral,1))
							, PosChange(0,5),2,player )						
			jmp endSetAni						
animjmp11:				
			:queueAnimation(List().add(Animation(crouch1,4),  Animation(crouch2,8))
							, PosChange(0,3),3,player )						
endSetAni:

}

.macro queueAnimation(framelist,poschange, effectmoment, player){ 
	.var p=1		//start at 1, it never reaches zero
	//create a queue of frames 
 	.for(var i=framelist.size()-1; i>=0; i--){
 		.var a
 		.eval a= framelist.get(i)
 		//read frame 
 		lda # a.get("framenr")
 		//repeat a number of times
 		.for (var j=0; j<a.get("framecount"); j++){ 
 			.if(player==1){
 				sta animationqueue1+p
 			}else{
 				sta animationqueue2+p
 			}
 			.eval p=p+1
 		}
 	}
 	//animation read, now check if there is a +1 or -1 change in x position
		lda # poschange.get("poschange")
	//make sure there is no position change by setting it to a moment that will never happen
		.if (poschange.get("poschange")==0){
		 	ldx #$FF	
		}else{
			ldx # poschange.get("poschangemoment")
		} 	
		ldy # p-1		

 	.if(player==1){
		sta posChange1		
		stx posChangeMoment1	
		sty queuecounter1		
		lda # effectmoment		
		sta effectMoment1		
	}else{		
		sta posChange2		
		stx posChangeMoment2	
		sty queuecounter2		
		lda # effectmoment		
		sta effectMoment2		
	}		
}

.macro queueBlinkAnimation(player) {
	.var frame = player == 1 ? frame1: frame2
	.var posChange = player == 1 ? posChange1: posChange2
	.var posChangeMoment = player == 1 ? posChangeMoment1: posChangeMoment2
	.var queuecounter = player == 1 ? queuecounter1: queuecounter2
	.var effectmoment = player == 1 ? effectMoment1: effectMoment2
	.var animationqueue = player == 1 ? animationqueue1: animationqueue2
	
	lda frame 
	sta animationqueue + 3
	sta animationqueue + 5
	sta animationqueue + 7
	sta animationqueue + 9
	sta animationqueue + 11
	sta animationqueue + 13
	sta animationqueue + 15
	lda #neutral
	sta animationqueue + 1
	lda #stun2
	sta animationqueue + 2
	sta animationqueue + 4
	sta animationqueue + 6
	sta animationqueue + 8
	sta animationqueue + 10
	sta animationqueue + 12
	sta animationqueue + 14
	lda #0
	sta posChange
	lda #$ff
	sta effectmoment
	sta posChangeMoment
	lda #16
	sta queuecounter

}


.macro checkedPosChange(player, poschange){
	.if(player==1){
		.if(poschange>0){
			lda dist
			beq end
			ldy pos1
			iny
			sty pos1
			dec dist
		}else{
			ldy pos1
			beq end //bmi end
			dey
			sty pos1
			inc dist
		}
	
	}else{
		.if(poschange>0){
			ldy pos2
			cpy #8
			beq end
			iny
			sty pos2
			inc dist
		}else{
			lda dist
			beq end
			ldy pos2
			dey
			sty pos2
			dec dist
		}
	}
end:	
}
/*--------------------------------------------------------------
				Draw Pose
----------------------------------------------------*/				
// all addresses that look like 'sprite1rev' are  
// addresses that will be modyfied  
.macro drawpose(player){ 												
						
		lda doDrawPlayers
		bne !+
		jmp noDraw
!:
	//first clear existing frame
			clc			
			ldy #0			
clearmod1:	
			.if(player==1){
				lda sprite1rev,y
			}else{
			 	lda sprite2rev,y					
			}					
			cmp #$fe	//end of this cells for this color				
			beq !+					
			cmp #$ff 	//end of all cells
			beq	clearend					
			.if(player==1){				
				adc pos1Old 					
			}else{					
				adc pos2Old 				
			}					
			tax					
			lda #140		//STY absolute					
			sta zpStart,x					
			iny					
			bne clearmod1		// y will  never be zero here so will always branch, saves 0 or 1 cycles 					
!:			iny					// all 'sta' are cleared now the 'stx' 
			jmp clearmod1
clearend:	
			//now draw new frame
			.if(player==1){
				ldx frame1
				lda spritepointerlo,x 
			}else{					
				ldx frame2	
				lda spritepointerlorev,x
			}
			
			sta drawmod1+1 
			sta drawmod2+1
		//and set that frame as the next frame to clear
			sta clearmod1+1
			.if(player==1){
				lda spritepointerhi,x 
			}else{					
				lda spritepointerhirev,x
			}
			
			sta drawmod1+2 
			sta drawmod2+2
			sta clearmod1+2
			.if(player==1){
				lda pos1
				sta pos1Old
			}else{			
				lda pos2
				sta pos2Old
			}			
			ldy #0			
drawmod1:	lda sprite1rev,y					
			cmp #$fe					
			beq !+					
			cmp #$ff 
			beq	drawend					
			.if(player==1){
				adc pos1 					
			}else{					
				adc pos2				
			}
			tax					
			.if (player==1){ 							
				lda #STA_ABS //141			//STA absolute					
			} else {					
				lda #143   		//SAX absolute					
			}					
			sta zpStart,x					
			
			//now store pixelpos for hittesting				
			txa				
			.if (player==1){						
				sta player1Pose,y
			} else {
				sta player2Pose,y
			}
			 				
			iny					
			bne drawmod1		// y will  never be zero here so will always branch, saves 0 or 1 cycles 					
!:			iny 
drawmod2:	lda sprite1rev,y					
			cmp #$ff 
			beq	drawend					
			.if(player==1){
				adc pos1 					
			}else{					
				adc pos2				
			}					
			tax					
			lda #STX_ABS//142		//STX absolute					
			sta zpStart,x					

			//now store pixelpos for hittesting				
			txa				
			.if (player==1){						
				sta player1Pose,y
			} else {
				sta player2Pose,y
			}

			iny					
			bne drawmod2 		// y will  never be zero here so will always branch, saves 0 or 1 cycles			
drawend:	//store ff in last of list of pixelpositions to indicate end of sprite
			.if (player==1){						
				sta player1Pose,y
			} else {
				sta player2Pose,y
			} 				
noDraw:			
					
}
/*-----------------------------------------------------------
			RECUPERATE
-----------------------------------------------------------*/			
.macro recuperate(player, isComputer) {
		.if(isComputer) {

			ldx computerLevel
		}
		else {
			ldx #0
		}
		.var energyRegainCount
		.var energy
		.if(player ==1) {
			.eval energyRegainCount = energyRegainCount1
			.eval energy = energy1
		}else {
			.eval energyRegainCount = energyRegainCount2
			.eval energy = energy2
		}
			lda energyRegainCount
			clc
			adc #$01
			sta energyRegainCount
			cmp regainThresholdPerLevel,x
			blt noRegain
			lda #0
			sta energyRegainCount
			ldx energy
			cpx #$06
			beq noRegain
			inx
			stx energy
noRegain:
}

/*==========================================================
				MOVE AND ANIMATE MACRO
==========================================================*/				
.macro move_and_ani_macro(player, skipInteractionAddress) {

.var stepqueuecounter =  player==1 ? stepqueuecounter_1 : stepqueuecounter_2
.var queuecounter = player == 1 ? queuecounter1 : queuecounter2 
.var stepqueuelo = player == 1 ? stepqueuelo_1 : stepqueuelo_2
.var stepqueuehi = player == 1 ? stepqueuehi_1 : stepqueuehi_2
.var posChangeMoment = player == 1 ? posChangeMoment1 : posChangeMoment2
.var posChange = player == 1 ? posChange1 : posChange2
.var effectMoment = player == 1 ? effectMoment1 : effectMoment2
.var animationQueue = player == 1 ? animationqueue1 : animationqueue2 
.var frame = player == 1 ? frame1 : frame2
.var nextStance = player == 1 ? nextStance1 : nextStance2 
.var stance = player == 1 ? stance1	: stance2	

			ldx queuecounter
			bne queueNotEmpty	//not zero, so animation still running
			//zero: check if there is still more steps to perform
			ldx stepqueuecounter
			bne moreSteps 
			lda #$ff
			sta effectMoment
			//lda frame	
			//cmp neutral	
			//bne !+	
			lda #12				//set to neutral stance
			sta stance
			sta nextStance
!:			jmp endMacro
moreSteps:			
.if (player == 1) {			
			lda stepqueuelo,x
			sta afterMoveSwitch 
			lda stepqueuehi,x
			sta afterMoveSwitch + 1 
			dec stepqueuecounter  
} else {			
			lda stepqueuelo,x
			sta endMacro + 1 
			lda stepqueuehi,x
			sta endMacro + 2 
			dec stepqueuecounter  
}			
			jmp endMacro			
						
queueNotEmpty:
		//check for position change
!:			cpx posChangeMoment
			bne nomove1
			lda posChange
		//plus: move 1 left
			bpl moveright1
			:checkedPosChange(player, -1)
			jmp nomove1
moveright1: 	:checkedPosChange(player, 1)
		
nomove1:	cpx effectMoment
			bne !+
			lda nextStance
			//now set new sprite frame
			sta stance
!:			lda animationQueue,x
			sta frame
			dec queuecounter
skipInteraction:			
			jmp skipInteractionAddress 			
endMacro:				
   		//for player 2 there must be a jmp at this address

}				


/*================================================================================
				PLAYER JOYSTICK HANDLING MACRO
====================================================================================*/				

.macro chkdointeractionmacro(player) {	
		.var joystick = player == 1? joystick1 : joystick2
		.var nextStance = player == 1 ? nextStance1 : nextStance2 
		.var stance = player == 1 ? stance1	: stance2	
		.var energy = player == 1 ? energy1	: energy2	
		.var energyRegainCount	 = player == 1 ? energyRegainCount1	: energyRegainCount2
		.var passivityCount 	 = player == 1 ? passivityCount1	: passivityCount2
		.var joytranslate 		 =  player == 1 ? joytranslate1	: joytranslate2
			lda joystick 			
			and #$1f			
		//left			
			cmp # [31-4]			
			bne !+			
			
			.if (player == 1) {
				ldx pos1
				beq !+
			}else {	
				ldx dist				
				beq !+	
			}	
	 			
			queueAnimation(List().add(Animation(neutral,1), Animation(walk1,2),  Animation(neutral,1))
							, PosChange(-1,3),0,player )						
			lda #12		//register as neutral											
			sta stance2													
			jmp checkEnd
			
		//right
!:			cmp # [31-8]			
			bne !+			
			.if (player == 1) {		
				ldx dist		
				beq !+
				//cpx #$01			
			}else {			
				ldx pos2			
				cpx #$0a						
				beq !+						
			}			
		
			queueAnimation(List().add(Animation(neutral,1), Animation(walk1,2),  Animation(neutral,1))
							, PosChange(1,3),0,player )						
			lda #12		//register as neutral													
			sta stance2													
			jmp checkEnd						
									
!:			tax
			lda joytranslate,x	
			tax						//there are 12 moves, translate joy direction to move, some joy values don't have a move, those return a minus value
			tay
			bpl !+
		//inc passivityCount
			jmp checkEnd
!:			lda energy
			cmp energyNeeded,x
			bcs !+
			jmp checkEnd		//more energy needed than player has ? -> end
!:			sec
			sbc energyNeeded,x
			sta energy
			lda #0					//reset energyregaincount
			sta energyRegainCount
			tya

setMove:			
	.if (player == 2) {			
	//for ai: keep track of moves to predict			
			tay			
			lda historyCounter			
			and #HISTORY_AND_VALUE			
			tax			
			tya			
			cmp #6			
			bcs end			
//only log attack moves						
			sta historyMoves,x
			inc historyCounter
end:			
	}
	//TODO remove stance functionality
			sta nextStance
			tax
			cmp #6		//move value smaller than 6 is attack move
			bcs	!+
			lda #0		//if attack move, reset passivityCount
			sta passivityCount
!:			setAni(player)
			jmp checkEnd
checkEnd:				
}

/*===========================================================================
						INCREASE SCORE
=============================================================================*/						

.macro increaseScore(player) {

	.if (player == 1) {
		inc score1
		drawScore()
		lda score1
		cmp #6
		bne end1                                                      
		//player1 wins
		lda #15
		setEffectMacro()
		lda #0
		sta doHitTest

end1:		
		jmp (defaultCloseSwitch)
	} else { 
		inc score2
		drawScore()
		lda score2 
		cmp #6
		bne end2
		//player2 wins	
		lda #16 
		setEffectMacro()
		lda #0
		sta doHitTest

end2:	
		jmp default_close_stuff2
	}
}
						

/*=====================================================================				
				Hit test				
=====================================================================*/								
.macro calcHitEffect1(){
		lda doHitTest
		bne !+
		jmp end
!:			lda player1HitsPlayer2 
			bne !+
			jmp end				//no hit: done
			lda stunned1			
			beq !+			
			jmp end				//player 1 is stunned: cannot attack	
!:			ldx frame1			//get current frame
			lda frame2attack,x	//check if is attack
			cmp #$ff
			beq end
			tax					//if so frame is index
			lda effectPointerLo,x
			sta sm1+1
			lda effectPointerHi,x
			sta sm1+2
			ldx frame2
sm1:	
			lda $ffff,x
			bne setEffect
			jmp end 
setEffect:	
			//something happens
			tax
			lda convertHitTable1,x	//because I had old table not fixing now but ugly
		cmp #4		//4: player 1 stunned
		bne !+
		ldy stunned1	//already stunned? not stunned again
		bne end
		ldy #1
		sty stunned1

!:	setEffectMacro()

		lda #0
		sta doHitTest
			jmp end
end:
}

.macro calcHitEffect2(){
		lda doHitTest
		bne !+
		jmp end
!:			lda player2HitsPlayer1	
			bne !+
			jmp end				//no hit: done
!:			lda stunned2
			beq !+
			jmp end				//player 2 is stunned: cannot attack
!:			ldx frame2			//get current frame
			lda frame2attack,x	//check if is attack
			cmp #$ff
			beq end
			tax					//if so frame is index
			lda effectPointerLo,x
			sta sm1+1
			lda effectPointerHi,x
			sta sm1+2
			ldx frame1			//check if other player does perfect defense
sm1:	
			lda $ffff,x
			bne setEffect		//no: do attack 
			jmp end 
setEffect:	
			//something happens
			tax
			lda convertHitTable2,x	//because I had old table not fixing now but ugly
		cmp #9		//9: player 2 stunned
		bne !+
		ldy stunned2	//already stunned? not stunned again
		bne end
		ldy #1
		sty stunned2
		
!:	setEffectMacro()
		lda #0
		sta doHitTest
			jmp end

end:
}
.macro hitPixelTest(){								
			lda #0
			sta player1HitsPlayer2
			sta player2HitsPlayer1
			
		//each frame may have a 'hitcell' an index value where player can place a hit			
			ldx frame1
			lda hitCellPlayer1,x
			beq endchecks1	//if zero there is no hitcell
			clc					//hitcell is a 'sprite relative value' (position relative to sprite origin) 
			adc pos1			//position must be added to get a absolute index value (position on screen)
			sta hitPoint1
			ldx #0
np1:		lda player2Pose,x	//playerpose indexes are already absolute
			cmp #$ff
			beq endchecks1	
			cmp hitPoint1		//index of player2 pixel same as index of hitpoint index?				
			bne !+				
			lda #1				
			sta player1HitsPlayer2				
			jmp endchecks1	//no further checking necessary 				
!:			inx		
			bne np1		//never equal so saves 1 cycle from jmp (depending on page boundaries ofcourse)		
endchecks1:			
		//each frame may have a 'hitcell' an index value where player can place a hit			
			ldx frame2
			lda hitCellPlayer2,x
			beq endchecks2	//if zero there is no hitcell
			clc					//hitcell is a 'sprite relative value' (position relative to sprite origin) 
			adc pos2			//position must be added to get a absolute index value (position on screen)
			sta hitPoint2
			ldx #0
np2:		lda player1Pose,x	//playerpose indexes are already absolute
			cmp #$ff
			beq endchecks2	
			cmp hitPoint2		//index of player2 pixel same as index of hitpoint index?				
			bne !+				
			lda #2			//other value from player 1				
			sta player2HitsPlayer1				
			jmp endchecks2	//no further checking necessary 				
!:			inx		
			bne np2		//never equal so saves 1 cycle from jmp (depending on page boundaries ofcourse)		
endchecks2:			


}								

.macro checkPassivity() {								
		lda doPassivityCheck				
		bne !+				
		jmp endChecks						
!:		lda passivityCount1
		cmp passivityCount2
		bne !+
		jmp endChecks
!:		lda passivityCount1						
	//break()						
		cmp passivityThreshold1						
		bcc !+						
		lda #0
		sta passivityCount1						
		lda #13								
		jmp passivityPenalty
!:		lda passivityCount2						
		cmp passivityThreshold2						
		bcc !+						
		lda #0
		sta passivityCount2						
		lda #12								
		jmp passivityPenalty										
!:		jmp	endChecks							

passivityPenalty:								
	setEffectMacro()								
		lda #0
		sta doHitTest
endChecks:

}								
/* -----------------------------------------												
		SET EFFECT MACRO, A register contains effect												
-------------------------------------------------*/														
												
.macro setEffectMacro(){
			sec
			sbc #1		//effect always >0 , effect = 0 is no effect. By subtracting 1 indexing is easier, you dont have to factor in an index = 0 
			sta currentEffect1
			sta currentEffect2 //TODO: check if neccesary, probably not because always the same?
			
			ldx #0		
			stx stepqueuecounter_1		
			stx queuecounter1		
			stx stepqueuecounter_2		
			stx queuecounter2		

			lda #<setEffect1 
			sta afterMoveSwitch
			lda #>setEffect1 
			sta afterMoveSwitch + 1
			
			lda #<setEffect2
			sta after_move2 + 1
			lda #>setEffect2
			sta after_move2 + 2
}
//FROM http://codebase64.org/doku.php?id=base:small_fast_8-bit_prng
.macro getRandom(){
     	lda randomSeed
     	beq doEor
    	asl
    	beq noEor //if the input was $80, skip the EOR
	    bcc noEor
doEor:  eor #$1d
noEor:  sta randomSeed		
}