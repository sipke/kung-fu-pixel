//super simple writer

.pc = $0801 "Basic upstart"
:BasicUpstart($0810)		
*=$0810
	lda #BLUE
	sta $d020
	jsr $e544 //clr screen
loop:
	lda #160
t0:	sta $0400
//really stupid delay
	ldx #0
!:	inx
	bne !- 
	ldx #0
!:	inx
	bne !- 


s:	lda text
	cmp #$ff
	beq done
	cmp #$fe
	bne t
!:	lda $dc01
	and #%00010000	
	bne !-	
	jsr $e544 //clr screen	
	lda #$00
	sta t + 1
	sta t0 + 1
	lda #$04
	sta t + 2
	sta t0 + 2
	inc s +1 
	bne !+
	inc s + 2
!:	jmp s
	
t:	sta $0400
	ldx #0
!:	inx
	bne !- 
	inc s + 1
	bne !+
	inc s+2
!:	inc t + 1
	bne !+
	inc t + 2
!:	inc t0 + 1
	bne !+
	inc t0 + 2
!:	jmp loop
done:	
!:	lda $dc01
	and #%00010000	
	bne !-	
	//move cursor to bottom screen
	lda #0
	sta 211
	lda #22
	sta 214
	rts


text:
//	   1234567890123456789012345678901234567890
.text "                                        "
.text "------  kung fu pixel instructions -----"
.text "                                        "
.text "* use joystick in port 2                "
.text "  for the menu and 1 player game        "
.text "                                        "
.text "* attack moves are done with fire       "
.text "* defense moves are always without fire "
.text "  if you defend the right way, you can  "
.text "  temporarily stun the opponent         "
.text "                                        "
.text "* some moves cost energy                "
.text "  so keep an eye on your energy level   "
.text " (the bars on the bottom right and left)"
.text "                                        "
.text "* if you don't attack enough            "
.text "  you will get a penalty                "
.text "  shown by a very big 'p'               "
.text "   (space for page 2)                   "
.byte $fe
.text "                                        "
.text "* the first person to score 6 wins      "
.text "  the scores are shown by a bar         "
.text "  at the top of the screen              "
.text "  in the colors of the players          "
.text "                                        "
.text "* when out of time the one with the     "
.text "  highest score wins                    "
.text "* the ai wins on a draw                 "
.text "                                        "
.text "* in pre game menu use:                 "
.text "  - left for one player game            "
.text "  - right for two player game           "
.text "  - use up and down, to select music    "
.text "    (random, a specific one or          "
.text "     bleeps and bloops)                 "
.text "                                        "
.text "             (press space to close this)"
.byte $ff

