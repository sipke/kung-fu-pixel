//========= INSTRUMENTS ==================
//1:triang
//2:pulse
//3:pulsewide
//4:pulsesml
//5:filtbass
//6:leadwrel
//7:hihat
//8:ting
//9:empty24
//10:trianrel
//11:syncpuls
//12:silentlo
//13:shortest
//14:pulseshrt
//15:pulsdecy
//16:pulsechrd
//17:koto
//18:unknown1

// instrument               0    1    2    3    4    5    6    7    8   9   10   11   12   13   14   15   16   17
.var pagecheckstart
.var pagecheckend
instruments:
//        ad   sr   wav  phi  plo  pvib
    .byte $00, $00, $00                   //filler
    .byte $63, $f0, $11, $00, $00, $00, $00, $00      //0
    .byte $00, $68, $41, $08, $00, $10, $00, $00      //1
    .byte $00, $ff, $41, $08, $00, $20, $00, $00      //2
    .byte $80, $ce, $21, $01, $10, $08, $00, $00      //3
    .byte $b0, $dd, $41, $08, $00, $08, $00, $00      //4
    .byte $80, $fe, $41, $0a, $11, $00, $00, $00      //5
    .byte $02, $00, $81, $00, $00, $00, $00, $00      //6
    .byte $00, $dc, $15, $00, $00, $00, $00, $00      //7
    .byte $00, $00, $00, $00, $00, $00, $00, $00      //8
    .byte $60, $ec, $11, $00, $00, $00, $00, $00      //9
    .byte $da, $ad, $43, $08, $00, $00, $00, $00      //10
    .byte $00, $00, $40, $08, $80, $00, $00, $00      //11
    .byte $22, $00, $41, $01, $00, $00, $00, $00      //12
    .byte $80, $c0, $41, $08, $80, $08, $00, $00      //13
    .byte $80, $bd, $21, $08, $10, $00, $00, $00      //14
    .byte $33, $80, $21, $02, $10, $00, $00, $00      //15
    .byte $40, $6c, $41, $08, $00, $08, $00, $00      //16
    .byte $80, $9d, $21, $08, $00, $00, $00, $00      //17
.assert "page not crossed with instruments", floor(instruments/256),floor((*-1)/256)
.var patternsStart = *
pattern1:
   :Event("instr1",39,12)
   :Event("noteoff",7,6)
   :Event("noteon",37,12)
   :Event("noteoff",7,6)
   :Event("noteon",34,12)
   :Event("noteoff",7,6)
   :Event("noteon",37,12)
   :Event("noteoff",7,6)
   :Event("notetied",39,24)
   :Event("vibr2",2,47)
   :Event("end",1,1)
pattern1_a:
.assert "page not crossed with pattern1", floor(pattern1/256),floor((pattern1_a-1)/256)
   :Event("instr1",27,12)
   :Event("noteoff",7,6)
   :Event("noteon",25,12)
   :Event("noteoff",7,6)
   :Event("noteon",22,12)
   :Event("noteoff",7,6)
   :Event("noteon",25,12)
   :Event("noteoff",7,6)
   :Event("notetied",27,24)
   :Event("vibr2",2,47)
   :Event("end",1,1)
.align $100   
pattern2:
.assert "page not crossed with pattern1_a", floor(pattern1_a/256),floor((pattern2-1)/256)
   :Event("instr1",42,12)
   :Event("noteoff",7,6)
   :Event("noteon",39,12)
   :Event("noteoff",7,6)
   :Event("noteon",37,12)
   :Event("noteoff",7,6)
   :Event("noteon",39,12)
   :Event("noteoff",7,6)
   :Event("notetied",42,30)
   :Event("vibr2",2,41)
   :Event("end",1,1)
pattern2_a:
.assert "page not crossed with pattern2", floor(pattern2/256),floor((pattern2_a-1)/256)
   :Event("instr1",30,12)
   :Event("noteoff",7,6)
   :Event("noteon",27,12)
   :Event("noteoff",7,6)
   :Event("noteon",25,12)
   :Event("noteoff",7,6)
   :Event("noteon",27,12)
   :Event("noteoff",7,6)
   :Event("notetied",30,30)
   :Event("vibr2",2,41)
   :Event("end",1,1)
pattern3:
.assert "page not crossed with pattern2_a", floor(pattern2_a/256),floor((pattern3-1)/256)
   :Event("instr1",44,12)
   :Event("noteoff",7,6)
   :Event("noteon",42,12)
   :Event("noteoff",7,6)
   :Event("noteon",39,12)
   :Event("noteoff",7,6)
   :Event("noteon",42,12)
   :Event("noteoff",7,6)
   :Event("notetied",44,30)
   :Event("vibr2",2,41)
   :Event("end",1,1)
pattern3_a:
.assert "page not crossed with pattern3", floor(pattern3/256),floor((pattern3_a-1)/256)
   :Event("instr1",32,12)
   :Event("noteoff",7,6)
   :Event("noteon",30,12)
   :Event("noteoff",7,6)
   :Event("noteon",27,12)
   :Event("noteoff",7,6)
   :Event("noteon",30,12)
   :Event("noteoff",7,6)
   :Event("notetied",32,30)
   :Event("vibr2",2,41)
   :Event("end",1,1)
pattern4:
.assert "page not crossed with pattern3_a", floor(pattern3_a/256),floor((pattern4-1)/256)
   :Event("instr1",46,12)
   :Event("noteoff",7,6)
   :Event("noteon",44,12)
   :Event("noteoff",7,6)
   :Event("noteon",42,12)
   :Event("noteoff",7,6)
   :Event("noteon",44,12)
   :Event("noteoff",7,6)
   :Event("noteon",46,12)
   :Event("noteoff",7,6)
   :Event("noteon",44,12)
   :Event("noteoff",7,6)
   :Event("noteon",46,12)
   :Event("noteoff",7,6)
   :Event("noteon",37,17)
   :Event("end",1,1)
pattern4_a:
.assert "page not crossed with pattern4", floor(pattern4/256),floor((pattern4_a-1)/256)
   :Event("instr1",34,12)
   :Event("noteoff",7,6)
   :Event("noteon",32,12)
   :Event("noteoff",7,6)
   :Event("noteon",30,12)
   :Event("noteoff",7,6)
   :Event("noteon",32,12)
   :Event("noteoff",7,6)
   :Event("noteon",34,12)
   :Event("noteoff",7,6)
   :Event("noteon",32,12)
   :Event("noteoff",7,6)
   :Event("noteon",34,12)
   :Event("noteoff",7,6)
   :Event("noteon",25,17)
   :Event("end",1,1)
.align $100
pattern5:
.assert "page not crossed with pattern4_a", floor(pattern4_a/256),floor((pattern5-1)/256)
   :Event("instr1",49,12)
   :Event("noteoff",7,6)
   :Event("noteon",46,12)
   :Event("noteoff",7,6)
   :Event("noteon",44,12)
   :Event("noteoff",7,6)
   :Event("noteon",46,12)
   :Event("noteoff",7,6)
   :Event("noteon",49,12)
   :Event("noteoff",7,6)
   :Event("noteon",46,12)
   :Event("noteoff",7,6)
   :Event("noteon",49,35)
   :Event("end",1,1)
pattern5_a:
.assert "page not crossed with pattern5", floor(pattern5/256),floor((pattern5_a-1)/256)
   :Event("instr1",37,12)
   :Event("noteoff",7,6)
   :Event("noteon",34,12)
   :Event("noteoff",7,6)
   :Event("noteon",32,12)
   :Event("noteoff",7,6)
   :Event("noteon",34,12)
   :Event("noteoff",7,6)
   :Event("noteon",37,12)
   :Event("noteoff",7,6)
   :Event("noteon",34,12)
   :Event("noteoff",7,6)
   :Event("noteon",37,35)
   :Event("end",1,1)
pattern6:
.assert "page not crossed with pattern5_a", floor(pattern5_a/256),floor((pattern6-1)/256)
   :Event("instr1",51,36)
   :Event("noteon",51,36)
   :Event("noteon",51,36)
   :Event("noteon",51,35)
   :Event("end",1,1)
pattern6_a:
.assert "page not crossed with pattern6", floor(pattern6/256),floor((pattern6_a-1)/256)
   :Event("instr1",39,36)
   :Event("noteon",39,36)
   :Event("noteon",39,36)
   :Event("noteon",39,35)
   :Event("end",1,1)
pattern6_b:
.assert "page not crossed with pattern6_a", floor(pattern6_a/256),floor((pattern6_b-1)/256)
   :Event("instr1",49,36)
   :Event("noteon",49,36)
   :Event("noteon",49,36)
   :Event("noteon",49,35)
   :Event("end",1,1)
pattern6_c:
.assert "page not crossed with pattern6_b", floor(pattern6_b/256),floor((pattern6_c-1)/256)
   :Event("instr1",37,36)
   :Event("noteon",37,36)
   :Event("noteon",37,36)
   :Event("noteon",37,35)
   :Event("end",1,1)
pattern7:
.assert "page not crossed with pattern6_c", floor(pattern6_c/256),floor((pattern7-1)/256)
   :Event("instr8",0,143)
   :Event("end",1,1)
pattern7_l6_t:
.assert "page not crossed with pattern7", floor(pattern7/256),floor((pattern7_l6_t-1)/256)
   :Event("noteofftied",0,143)
   :Event("end",1,1)
pattern7_a:
.assert "page not crossed with pattern7_l6_t", floor(pattern7_l6_t/256),floor((pattern7_a-1)/256)
   :Event("instr8",0,167)
   :Event("end",1,1)
pattern8:
.assert "page not crossed with pattern7_a", floor(pattern7_a/256),floor((pattern8-1)/256)
   :Event("instr1",46,12)
   :Event("noteon",44,12)
   :Event("noteon",42,12)
   :Event("noteon",39,12)
   :Event("noteon",42,12)
   :Event("noteon",44,12)
   :Event("noteon",46,12)
   :Event("noteon",44,12)
   :Event("noteon",42,12)
   :Event("noteon",39,12)
   :Event("noteon",42,12)
   :Event("noteon",44,11)
   :Event("end",1,1)
pattern9:
.assert "page not crossed with pattern8", floor(pattern8/256),floor((pattern9-1)/256)
   :Event("instr1",46,36)
   :Event("noteon",46,36)
   :Event("noteon",46,36)
   :Event("noteon",46,35)
   :Event("end",1,1)
patterna:
.assert "page not crossed with pattern9", floor(pattern9/256),floor((patterna-1)/256)
   :Event("instr0",39,240)
   :Event("instt0",39,144)
   :Event("noteoff",7,191)
   :Event("end",1,1)
.align $100   
patterna_a:
.assert "page not crossed with patterna", floor(patterna/256),floor((patterna_a-1)/256)
   :Event("instr0",27,240)
   :Event("instt0",27,144)
   :Event("noteoff",7,191)
   :Event("end",1,1)
patterna_b:
.assert "page not crossed with patterna_a", floor(patterna_a/256),floor((patterna_b-1)/256)
   :Event("instr0",15,255)
   :Event("nonotetied",15,129)
   :Event("noteoff",7,191)
   :Event("end",1,1)
patternb:
.assert "page not crossed with patterna_b", floor(patterna_b/256),floor((patternb-1)/256)
   :Event("instr1",44,36)
   :Event("noteon",44,36)
   :Event("noteon",44,36)
   :Event("noteon",44,35)
   :Event("end",1,1)
patternc:
.assert "page not crossed with patternb", floor(patternb/256),floor((patternc-1)/256)
   :Event("instt0",70,108)
   :Event("slideup",3,36)
   :Event("notetied",73,143)
   :Event("end",1,1)
patternd:
.assert "page not crossed with patternc", floor(patternc/256),floor((patternd-1)/256)
   :Event("notetied",78,78)
   :Event("slidedown",2,30)
   :Event("notetied",75,36)
   :Event("noteon",73,72)
   :Event("noteon",68,36)
   :Event("notetied",69,6)
   :Event("slideup",2,29)
   :Event("end",1,1)
patterne:
.assert "page not crossed with patternd", floor(patternd/256),floor((patterne-1)/256)
   :Event("instt0",73,108)
   :Event("slideup",2,36)
   :Event("notetied",75,114)
   :Event("notetied",77,6)
   :Event("slideup",2,23)
   :Event("end",1,1)
patternf:
.assert "page not crossed with patterne", floor(patterne/256),floor((patternf-1)/256)
   :Event("instr0",66,7)
   :Event("noteon",61,7)
   :Event("noteon",61,7)
   :Event("noteon",56,7)
   :Event("noteon",54,7)
   :Event("noteon",51,7)
   :Event("noteon",66,7)
   :Event("noteon",61,7)
   :Event("noteon",61,7)
   :Event("noteon",56,7)
   :Event("noteon",54,7)
   :Event("noteon",51,7)
   :Event("noteon",66,7)
   :Event("noteon",61,7)
   :Event("noteon",61,7)
   :Event("noteon",56,7)
   :Event("noteon",54,7)
   :Event("noteon",51,7)
   :Event("noteon",66,7)
   :Event("noteon",61,7)
   :Event("noteon",61,7)
   :Event("noteon",56,7)
   :Event("noteon",54,7)
   :Event("noteon",51,6)
   :Event("end",1,1)
pattern10:
.assert "page not crossed with patternf", floor(patternf/256),floor((pattern10-1)/256)
   :Event("instr2",27,255)
   :Event("nonotetied",27,80)
   :Event("end",1,1)
pattern11:
.assert "page not crossed with pattern10", floor(pattern10/256),floor((pattern11-1)/256)
   :Event("instr2",32,255)
   :Event("nonotetied",32,80)
   :Event("end",1,1)
pattern12:
.assert "page not crossed with pattern11", floor(pattern11/256),floor((pattern12-1)/256)
   :Event("instr13",82,7)
   :Event("noteon",80,7)
   :Event("noteon",78,7)
   :Event("instr3",80,56)
   :Event("slidedown",10,28)
   :Event("instr13",78,7)
   :Event("noteon",80,7)
   :Event("noteon",80,7)
   :Event("noteon",78,7)
   :Event("noteon",80,7)
   :Event("noteon",80,7)
   :Event("noteon",80,14)
   :Event("noteon",90,6)
   :Event("end",1,1)
.align $100   
pattern13:
.assert "page not crossed with pattern12", floor(pattern12/256),floor((pattern13-1)/256)
   :Event("instr13",63,7)
   :Event("noteon",61,7)
   :Event("noteon",58,7)
   :Event("instr3",61,56)
   :Event("slidedown",10,56)
   :Event("instr13",54,7)
   :Event("noteon",54,7)
   :Event("noteon",54,7)
   :Event("noteon",58,7)
   :Event("noteon",56,6)
   :Event("end",1,1)
pattern14:
.assert "page not crossed with pattern13", floor(pattern13/256),floor((pattern14-1)/256)
   :Event("instr0",61,7)
   :Event("noteon",63,7)
   :Event("noteon",66,7)
   :Event("noteon",68,7)
   :Event("noteon",63,7)
   :Event("noteon",66,7)
   :Event("noteon",68,7)
   :Event("noteon",70,7)
   :Event("noteon",66,7)
   :Event("noteon",68,7)
   :Event("noteon",70,7)
   :Event("noteon",73,91)
   :Event("noteon",61,7)
   :Event("noteon",56,7)
   :Event("noteon",58,7)
   :Event("noteon",61,49)
   :Event("noteon",51,7)
   :Event("noteon",49,7)
   :Event("noteon",46,7)
   :Event("noteon",49,76)
   :Event("end",1,1)
pattern15:
.assert "page not crossed with pattern14", floor(pattern14/256),floor((pattern15-1)/256)
   :Event("instt2",32,77)
   :Event("noteon",30,245)
   :Event("noteoff",7,13)
   :Event("end",1,1)
pattern16:
.assert "page not crossed with pattern15", floor(pattern15/256),floor((pattern16-1)/256)
   :Event("instt3",56,126)
   :Event("noteon",82,42)
   :Event("slidedown",2,91)
   :Event("noteon",80,21)
   :Event("noteoff",7,55)
   :Event("end",1,1)
pattern17:
.assert "page not crossed with pattern16", floor(pattern16/256),floor((pattern17-1)/256)
   :Event("instr13",63,7)
   :Event("noteon",61,7)
   :Event("instr3",66,35)
   :Event("noteon",63,21)
   :Event("noteon",63,97)
   :Event("end",1,1)
pattern18:
.assert "page not crossed with pattern17", floor(pattern17/256),floor((pattern18-1)/256)
   :Event("instt2",32,112)
   :Event("noteon",32,223)
   :Event("end",1,1)
pattern1a:
.assert "page not crossed with pattern18", floor(pattern18/256),floor((pattern1a-1)/256)
   :Event("instr3",56,7)
   :Event("noteoff",7,255)
   :Event("nonotetied",7,255)
   :Event("nonotetied",7,154)
   :Event("end",1,1)
pattern1a_g3:
.assert "page not crossed with pattern1a", floor(pattern1a/256),floor((pattern1a_g3-1)/256)
   :Event("instr3",32,7)
   :Event("noteoff",7,255)
   :Event("nonotetied",7,255)
   :Event("nonotetied",7,154)
   :Event("end",1,1)
pattern1a_d6:
.assert "page not crossed with pattern1a_g3", floor(pattern1a_g3/256),floor((pattern1a_d6-1)/256)
   :Event("instr3",63,7)
   :Event("noteoff",7,255)
   :Event("nonotetied",7,255)
   :Event("nonotetied",7,154)
   :Event("end",1,1)
.align $100   
pattern40:
.assert "page not crossed with pattern1a_d6", floor(pattern1a_d6/256),floor((pattern40-1)/256)
   :Event("instr1",13,12)
   :Event("instr6",84,12)
   :Event("noteon",72,12)
   :Event("noteon",84,12)
   :Event("instr1",25,12)
   :Event("instr6",84,12)
   :Event("noteon",72,12)
   :Event("noteon",84,12)
   :Event("instr1",30,12)
   :Event("instr6",84,6)
   :Event("noteon",60,6)
   :Event("instr1",27,12)
   :Event("instr6",72,6)
   :Event("noteon",48,5)
   :Event("end",1,1)
pattern41:
.assert "page not crossed with pattern40", floor(pattern40/256),floor((pattern41-1)/256)
   :Event("instr1",49,72)
   :Event("noteon",56,12)
   :Event("noteon",54,60)
   :Event("noteoff",7,143)
   :Event("end",1,1)
pattern42:
.assert "page not crossed with pattern41", floor(pattern41/256),floor((pattern42-1)/256)
   :Event("instr1",49,72)
   :Event("noteon",56,12)
   :Event("noteon",58,60)
   :Event("noteoff",7,143)
   :Event("end",1,1)
pattern43:
.assert "page not crossed with pattern42", floor(pattern42/256),floor((pattern43-1)/256)
   :Event("instr1",63,6)
   :Event("noteon",61,6)
   :Event("noteon",58,6)
   :Event("noteon",56,6)
   :Event("noteon",61,6)
   :Event("noteon",58,6)
   :Event("noteon",56,6)
   :Event("noteon",54,6)
   :Event("noteon",58,6)
   :Event("noteon",56,6)
   :Event("noteon",54,6)
   :Event("noteon",51,6)
   :Event("noteon",56,6)
   :Event("noteon",54,6)
   :Event("noteon",51,6)
   :Event("noteon",49,6)
   :Event("noteon",49,6)
   :Event("noteon",51,6)
   :Event("noteon",54,6)
   :Event("noteon",56,6)
   :Event("noteon",54,6)
   :Event("noteon",56,6)
   :Event("noteon",58,6)
   :Event("noteon",61,6)
   :Event("instr8",0,24)
   :Event("instr8",75,119)
   :Event("end",1,1)
pattern45:
.assert "page not crossed with pattern43", floor(pattern43/256),floor((pattern45-1)/256)
   :Event("instr8",13,36)
   :Event("instr7",61,6)
   :Event("noteoff",7,101)
   :Event("end",1,1)
.align $100   
pattern46:
.assert "page not crossed with pattern45", floor(pattern45/256),floor((pattern46-1)/256)
   :Event("instr1",63,6)
   :Event("noteon",61,6)
   :Event("noteon",58,6)
   :Event("noteon",56,6)
   :Event("noteon",61,6)
   :Event("noteon",58,6)
   :Event("noteon",56,6)
   :Event("noteon",54,6)
   :Event("noteon",58,6)
   :Event("noteon",56,6)
   :Event("noteon",54,6)
   :Event("noteon",51,6)
   :Event("noteon",56,6)
   :Event("noteon",54,6)
   :Event("noteon",51,6)
   :Event("noteon",49,6)
   :Event("noteon",49,6)
   :Event("noteon",51,6)
   :Event("noteon",54,6)
   :Event("noteon",56,6)
   :Event("noteon",54,6)
   :Event("noteon",56,6)
   :Event("noteon",58,6)
   :Event("noteon",63,6)
   :Event("noteoff",7,36)
   :Event("instr8",75,107)
   :Event("end",1,1)
pattern47:
.assert "page not crossed with pattern46", floor(pattern46/256),floor((pattern47-1)/256)
   :Event("instr1",58,6)
   :Event("noteon",56,6)
   :Event("noteon",54,6)
   :Event("noteon",51,6)
   :Event("noteon",56,6)
   :Event("noteon",54,6)
   :Event("noteon",51,6)
   :Event("noteon",49,6)
   :Event("noteon",54,6)
   :Event("noteon",51,6)
   :Event("noteon",49,6)
   :Event("noteon",46,6)
   :Event("noteon",39,6)
   :Event("noteon",37,6)
   :Event("noteon",46,6)
   :Event("noteon",44,6)
   :Event("noteon",44,6)
   :Event("noteon",46,6)
   :Event("noteon",49,6)
   :Event("noteon",51,6)
   :Event("noteon",49,6)
   :Event("noteon",51,6)
   :Event("noteon",54,6)
   :Event("noteon",58,6)
   :Event("noteoff",7,36)
   :Event("instr8",75,107)
   :Event("end",1,1)
pattern48:
.assert "page not crossed with pattern47", floor(pattern47/256),floor((pattern48-1)/256)
   :Event("instr8",0,36)
   :Event("noteon",75,107)
   :Event("end",1,1)
.align $100   
pattern49:
.assert "page not crossed with pattern48", floor(pattern48/256),floor((pattern49-1)/256)
   :Event("instr8",13,12)
   :Event("instr6",84,12)
   :Event("noteon",72,12)
   :Event("noteon",84,12)
   :Event("instr8",25,12)
   :Event("instr6",84,12)
   :Event("noteon",72,12)
   :Event("noteon",84,12)
   :Event("instr8",30,12)
   :Event("instr6",84,6)
   :Event("noteon",60,6)
   :Event("instr8",27,12)
   :Event("instr6",72,6)
   :Event("noteon",60,5)
   :Event("end",1,1)
pattern4a:
.assert "page not crossed with pattern49", floor(pattern49/256),floor((pattern4a-1)/256)
   :Event("instr6",84,6)
   :Event("noteon",84,6)
   :Event("noteon",84,6)
   :Event("noteon",84,6)
   :Event("noteon",84,6)
   :Event("noteon",84,6)
   :Event("noteon",84,6)
   :Event("noteon",84,6)
   :Event("noteon",84,6)
   :Event("noteon",84,6)
   :Event("noteon",84,6)
   :Event("noteon",84,6)
   :Event("noteon",84,6)
   :Event("noteon",84,6)
   :Event("noteon",84,6)
   :Event("noteon",84,6)
   :Event("noteon",84,6)
   :Event("noteon",84,6)
   :Event("noteon",84,6)
   :Event("noteon",84,6)
   :Event("noteon",84,6)
   :Event("noteon",84,6)
   :Event("noteon",84,6)
   :Event("noteon",84,5)
   :Event("end",1,1)
pattern4c:
.assert "page not crossed with pattern4a", floor(pattern4a/256),floor((pattern4c-1)/256)
   :Event("instr1",30,12)
   :Event("instr6",84,12)
   :Event("noteon",72,12)
   :Event("noteon",84,12)
   :Event("instr1",32,12)
   :Event("instr6",84,12)
   :Event("instr6",72,12)
   :Event("instr6",84,12)
   :Event("instr1",35,12)
   :Event("instr6",84,6)
   :Event("noteon",60,6)
   :Event("instr1",32,12)
   :Event("instr6",72,6)
   :Event("noteon",60,5)
   :Event("end",1,1)
.align $100   
pattern50:
.assert "page not crossed with pattern4c", floor(pattern4c/256),floor((pattern50-1)/256)
   :Event("instr9",46,18)
   :Event("noteon",49,18)
   :Event("noteon",54,18)
   :Event("noteon",46,18)
   :Event("noteon",49,18)
   :Event("noteon",54,18)
   :Event("noteon",46,18)
   :Event("noteon",49,18)
   :Event("noteon",54,30)
   :Event("noteon",54,41)
   :Event("end",1,1)
pattern51:
.assert "page not crossed with pattern50", floor(pattern50/256),floor((pattern51-1)/256)
   :Event("instr9",37,18)
   :Event("noteon",39,18)
   :Event("noteon",44,18)
   :Event("noteon",37,18)
   :Event("noteon",39,18)
   :Event("noteon",44,18)
   :Event("noteon",37,18)
   :Event("noteon",39,18)
   :Event("noteon",44,30)
   :Event("noteon",44,41)
   :Event("end",1,1)
pattern52:
.assert "page not crossed with pattern51", floor(pattern51/256),floor((pattern52-1)/256)
   :Event("instt11",1,215)
   :Event("end",1,1)
pattern53:
.assert "page not crossed with pattern52", floor(pattern52/256),floor((pattern53-1)/256)
   :Event("noteofftied",0,24)
   :Event("instt10",70,192)
   :Event("noteoff",7,215)
   :Event("end",1,1)
pattern54:
.assert "page not crossed with pattern53", floor(pattern53/256),floor((pattern54-1)/256)
   :Event("noteofftied",0,24)
   :Event("instt10",68,192)
   :Event("noteoff",7,215)
   :Event("end",1,1)
pattern55:
.assert "page not crossed with pattern54", floor(pattern54/256),floor((pattern55-1)/256)
   :Event("noteofftied",0,24)
   :Event("instt10",70,168)
   :Event("noteoff",7,24)
   :Event("noteofftied",73,18)
   :Event("noteofftied",70,12)
   :Event("noteofftied",73,24)
   :Event("noteofftied",70,90)
   :Event("noteofftied",66,36)
   :Event("slideup",2,35  -1)
   		:Event("slideup",0,1)
   :Event("end",1,1)
pattern56:
.assert "page not crossed with pattern55", floor(pattern55/256),floor((pattern56-1)/256)
   :Event("instr14",66,18)
   :Event("vibr1",4,18)
   		:Event("vibr0",0,18)
   :Event("noteon",63,18)
   :Event("noteon",61,18)
   :Event("noteon",58,18)
   :Event("noteon",61,18)
   :Event("noteon",63,18)
   :Event("noteon",66,18)
   :Event("vibr1",4,18)
   		:Event("vibr0",0,18)
   :Event("noteon",63,18)
   :Event("noteon",61,18)
   :Event("noteon",58,18)
   :Event("noteon",61,18)
   :Event("noteon",63,18)
   :Event("noteon",68,6)
   :Event("vibr1",3,17)
   		:Event("vibr1",0,1)
   :Event("slidedown",8,11)
   		:Event("slidedown",0,1)
   :Event("noteon",63,48)
   :Event("noteoff",7,6)
   :Event("notetied",61,18)
   :Event("notetied",61,35)
   :Event("end",1,1)
pattern57:
.assert "page not crossed with pattern56", floor(pattern56/256),floor((pattern57-1)/256)
   :Event("instr14",56,18)
   :Event("vibr1",4,18)
   :Event("notetied",54,18)
   :Event("vibr1",4,24)
   :Event("noteoff",7,78)
   :Event("vibr1",4,60)
   :Event("noteon",46,18)
   :Event("vibr1",4,18)
   :Event("noteon",51,18)
   :Event("vibr1",4,36)
   :Event("noteoff",7,125)
   :Event("end",1,1)
.align $100   
pattern60:
.assert "page not crossed with pattern57", floor(pattern57/256),floor((pattern60-1)/256)
   :Event("instt4",29,12)
   :Event("slideup",1,23)
   		:Event("slideup",0,1)
   :Event("noteon",30,48)
   :Event("vibr1",1,156)
   :Event("noteoff",7,119)
   :Event("end",1,1)
pattern61:
.assert "page not crossed with pattern60", floor(pattern60/256),floor((pattern61-1)/256)
   :Event("instt4",26,6)
   :Event("slideup",1,29)
   		:Event("slideup",0,1)
   :Event("notetied",27,54)
   :Event("notetied",25,36)
   :Event("noteon",15,66)
   :Event("noteon",30,114)
   :Event("noteoff",7,6)
   :Event("slideup",1,41)
   		:Event("slideup",0,1)
   :Event("noteofftied",34,77)
   :Event("end",1,1)
pattern62:
.assert "page not crossed with pattern61", floor(pattern61/256),floor((pattern62-1)/256)
   :Event("noteofftied",0,71)
   :Event("end",1,1)
pattern22:
.assert "page not crossed with pattern62", floor(pattern62/256),floor((pattern22-1)/256)
   :Event("instr9",54,18)
   :Event("noteon",56,18)
   :Event("noteon",54,18)
   :Event("noteon",46,18)
   :Event("noteon",44,18)
   :Event("noteon",49,18)
   :Event("noteon",54,18)
   :Event("noteon",56,18)
   :Event("noteon",54,18)
   :Event("noteon",46,18)
   :Event("noteon",44,18)
   :Event("noteon",49,17)
   :Event("end",1,1)
pattern23:
.assert "page not crossed with pattern22", floor(pattern22/256),floor((pattern23-1)/256)
   :Event("instr9",56,18)
   :Event("noteon",54,18)
   :Event("noteon",51,18)
   :Event("noteon",44,18)
   :Event("noteon",46,18)
   :Event("noteon",49,18)
   :Event("noteon",56,18)
   :Event("noteon",54,18)
   :Event("noteon",51,36)
   :Event("noteon",51,35)
   :Event("end",1,1)
pattern24:
.assert "page not crossed with pattern23", floor(pattern23/256),floor((pattern24-1)/256)
   :Event("instr9",54,18)
   :Event("noteon",56,18)
   :Event("noteon",54,18)
   :Event("noteon",46,18)
   :Event("noteon",44,18)
   :Event("noteon",49,18)
   :Event("noteon",54,18)
   :Event("noteon",56,18)
   :Event("noteon",54,18)
   :Event("noteon",46,18)
   :Event("noteon",44,18)
   :Event("noteon",48,17)
   :Event("end",1,1)
pattern25:
.assert "page not crossed with pattern24", floor(pattern24/256),floor((pattern25-1)/256)
   :Event("instr9",37,18)
   :Event("slideup",1,18)
   :Event("noteon",39,18)
   :Event("noteoff",7,167)
   :Event("end",1,1)
patternf0:
.assert "page not crossed with pattern25", floor(pattern25/256),floor((patternf0-1)/256)
   :Event("instt12",37,1)
   :Event("end",1,1)
patternf1:
.assert "page not crossed with patternf0", floor(patternf0/256),floor((patternf1-1)/256)
.assert "page not crossed with patternf1", floor(patternf1/256),floor((* -1)/256)
   :Event("instt12",37,1)
   :Event("end",1,1)
   
//      ---- song0----
.align $100
.eval pagecheckstart = *
song0Voice1Lo:  .byte <pattern1,<pattern2,<pattern3,<pattern4,<pattern5,<pattern6,<pattern6,<pattern6,<pattern6,<pattern6_c,<pattern6_c,<pattern6_c,<pattern6_c,<pattern6_c,<pattern6_c,<pattern6_c,<pattern6_c,<pattern6_c,<pattern6_c,<pattern6_c,<pattern6_c,<pattern6_c,<pattern6_c,<pattern6_c,<pattern6_c,<pattern6_c,<pattern6_c,<patterna_a,<pattern7_l6_t
song0Voice1Hi:  .byte >pattern1,>pattern2,>pattern3,>pattern4,>pattern5,>pattern6,>pattern6,>pattern6,>pattern6,>pattern6_c,>pattern6_c,>pattern6_c,>pattern6_c,>pattern6_c,>pattern6_c,>pattern6_c,>pattern6_c,>pattern6_c,>pattern6_c,>pattern6_c,>pattern6_c,>pattern6_c,>pattern6_c,>pattern6_c,>pattern6_c,>pattern6_c,>pattern6_c,>patterna_a,>pattern7_l6_t,0
.eval pagecheckend = *-1
 .assert " pagecross ", floor(pagecheckstart/256),floor(pagecheckend/256) 
.eval pagecheckstart = *
song0Voice2Lo:  .byte <pattern7,<pattern7,<pattern7,<pattern7,<pattern7,<pattern7,<pattern8,<pattern9,<pattern8,<patternb,<pattern8,<pattern9,<pattern8,<patternb,<pattern8,<pattern9,<pattern8,<patternb,<pattern8,<pattern9,<pattern8,<patternb,<pattern8,<pattern9,<pattern8,<patterna_b,<pattern7_l6_t,<pattern7_l6_t,<pattern7_l6_t
song0Voice2Hi:  .byte >pattern7,>pattern7,>pattern7,>pattern7,>pattern7,>pattern7,>pattern8,>pattern9,>pattern8,>patternb,>pattern8,>pattern9,>pattern8,>patternb,>pattern8,>pattern9,>pattern8,>patternb,>pattern8,>pattern9,>pattern8,>patternb,>pattern8,>pattern9,>pattern8,>patterna_b,>pattern7_l6_t,>pattern7_l6_t,>pattern7_l6_t,0
.eval pagecheckend = *-1
 .assert " pagecross ", floor(pagecheckstart/256),floor(pagecheckend/256) 
.eval pagecheckstart = *
song0Voice3Lo:  .byte <pattern1_a,<pattern2_a,<pattern3_a,<pattern4_a,<pattern5_a,<pattern6_a,<pattern6_a,<pattern6_a,<pattern6_b,<pattern6_b,<patternc,<patterne,<patternc,<patterne,<patternd,<patternd,<patterna,<pattern7_l6_t,<pattern7_l6_t,<pattern7_l6_t,<pattern7_l6_t,<pattern7_l6_t,<pattern7_l6_t
song0Voice3Hi:  .byte >pattern1_a,>pattern2_a,>pattern3_a,>pattern4_a,>pattern5_a,>pattern6_a,>pattern6_a,>pattern6_a,>pattern6_b,>pattern6_b,>patternc,>patterne,>patternc,>patterne,>patternd,>patternd,>patterna,>pattern7_l6_t,>pattern7_l6_t,>pattern7_l6_t,>pattern7_l6_t,>pattern7_l6_t,>pattern7_l6_t,0
.eval pagecheckend = *-1
 .assert " pagecross song0Voice3Lo", floor(pagecheckstart/256),floor(pagecheckend/256) 
//      ---- song1----
.eval pagecheckstart = *
song1Voice1Lo:  .byte <pattern40,<pattern40,<pattern49,<pattern40,<pattern40,<pattern40,<pattern40,<pattern40,<pattern40,<pattern4c,<pattern4c,<pattern40,<pattern40,<pattern49,<pattern40,<pattern40,<pattern40,<pattern40,<pattern40,<pattern49,<pattern4c,<pattern4c,<pattern40,<pattern40,<pattern40,<pattern40,<pattern49,<pattern49,<pattern4a,<pattern7,<pattern7,<pattern7,<pattern7,<pattern7,<pattern7
song1Voice1Hi:  .byte >pattern40,>pattern40,>pattern49,>pattern40,>pattern40,>pattern40,>pattern40,>pattern40,>pattern40,>pattern4c,>pattern4c,>pattern40,>pattern40,>pattern49,>pattern40,>pattern40,>pattern40,>pattern40,>pattern40,>pattern49,>pattern4c,>pattern4c,>pattern40,>pattern40,>pattern40,>pattern40,>pattern49,>pattern49,>pattern4a,>pattern7,>pattern7,>pattern7,>pattern7,>pattern7,>pattern7,0
.eval pagecheckend = *-1
 .assert " pagecross ", floor(pagecheckstart/256),floor(pagecheckend/256) 
.align $100
.eval pagecheckstart = *
song1Voice2Lo:  .byte <pattern7,<pattern7,<pattern7,<pattern41,<pattern42,<pattern43,<pattern7,<pattern7,<pattern7,<pattern7,<pattern7,<pattern41,<pattern42,<pattern43,<pattern7,<pattern7,<pattern41,<pattern42,<pattern46,<pattern7,<pattern7,<pattern48,<pattern7,<pattern7,<pattern7,<pattern7
song1Voice2Hi:  .byte >pattern7,>pattern7,>pattern7,>pattern41,>pattern42,>pattern43,>pattern7,>pattern7,>pattern7,>pattern7,>pattern7,>pattern41,>pattern42,>pattern43,>pattern7,>pattern7,>pattern41,>pattern42,>pattern46,>pattern7,>pattern7,>pattern48,>pattern7,>pattern7,>pattern7,>pattern7,0
.eval pagecheckend = *-1
 .assert " pagecross ", floor(pagecheckstart/256),floor(pagecheckend/256) 
.eval pagecheckstart = *
song1Voice3Lo:  .byte <pattern7,<pattern7,<pattern7,<pattern7,<pattern7,<pattern7,<pattern7,<pattern7,<pattern45,<pattern7,<pattern7,<pattern7,<pattern7,<pattern7,<pattern7,<pattern7,<pattern41,<pattern7,<pattern45,<pattern7,<pattern7,<pattern7,<pattern7,<pattern41,<pattern47,<pattern7,<pattern7,<pattern45,<pattern7,<pattern7,<pattern7,<pattern7
song1Voice3Hi:  .byte >pattern7,>pattern7,>pattern7,>pattern7,>pattern7,>pattern7,>pattern7,>pattern7,>pattern45,>pattern7,>pattern7,>pattern7,>pattern7,>pattern7,>pattern7,>pattern7,>pattern41,>pattern7,>pattern45,>pattern7,>pattern7,>pattern7,>pattern7,>pattern41,>pattern47,>pattern7,>pattern7,>pattern45,>pattern7,>pattern7,>pattern7,>pattern7,0
.eval pagecheckend = *-1
 .assert " pagecross song1Voice3Lo", floor(pagecheckstart/256),floor(pagecheckend/256) 
//      ---- song2----
.eval pagecheckstart = *
song2Voice1Lo:  .byte <patternf,<patternf,<patternf,<patternf,<patternf,<patternf,<patternf,<patternf,<patternf,<patternf,<patternf,<patternf,<patternf,<patternf,<patternf,<patternf,<patternf,<patternf,<patternf,<patternf,<patternf,<patternf,<patternf,<patternf,<pattern14,<pattern7,<pattern14,<pattern7,<pattern14,<pattern14,<pattern14
song2Voice1Hi:  .byte >patternf,>patternf,>patternf,>patternf,>patternf,>patternf,>patternf,>patternf,>patternf,>patternf,>patternf,>patternf,>patternf,>patternf,>patternf,>patternf,>patternf,>patternf,>patternf,>patternf,>patternf,>patternf,>patternf,>patternf,>pattern14,>pattern7,>pattern14,>pattern7,>pattern14,>pattern14,>pattern14,0
.eval pagecheckend = *-1
 .assert " pagecross ", floor(pagecheckstart/256),floor(pagecheckend/256) 
.eval pagecheckstart = *
song2Voice2Lo:  .byte <pattern10,<pattern11,<pattern10,<pattern11,<pattern10,<pattern11,<pattern10,<pattern11,<pattern10,<pattern11,<pattern10,<pattern11,<pattern10,<pattern11,<pattern15,<pattern18,<pattern15,<pattern18,<pattern1a_g3
song2Voice2Hi:  .byte >pattern10,>pattern11,>pattern10,>pattern11,>pattern10,>pattern11,>pattern10,>pattern11,>pattern10,>pattern11,>pattern10,>pattern11,>pattern10,>pattern11,>pattern15,>pattern18,>pattern15,>pattern18,>pattern1a_g3,0
.eval pagecheckend = *-1
.assert " pagecross ", floor(pagecheckstart/256),floor(pagecheckend/256) 
.align $100
.eval pagecheckstart = *
song2Voice3Lo:  .byte <pattern7_a,<pattern7_a,<pattern7_a,<pattern7_a,<pattern12,<pattern12,<pattern12,<pattern12,<pattern13,<pattern13,<pattern13,<pattern13,<pattern1a,<pattern12,<pattern12,<pattern12,<pattern12,<pattern13,<pattern13,<pattern13,<pattern13,<pattern16,<pattern17,<pattern16,<pattern17,<pattern1a_d6
song2Voice3Hi:  .byte >pattern7_a,>pattern7_a,>pattern7_a,>pattern7_a,>pattern12,>pattern12,>pattern12,>pattern12,>pattern13,>pattern13,>pattern13,>pattern13,>pattern1a,>pattern12,>pattern12,>pattern12,>pattern12,>pattern13,>pattern13,>pattern13,>pattern13,>pattern16,>pattern17,>pattern16,>pattern17,>pattern1a_d6,0
.eval pagecheckend = *-1
 .assert " pagecross song2Voice3Lo", floor(pagecheckstart/256),floor(pagecheckend/256) 
//      ---- song3----
.eval pagecheckstart = *
song3Voice1Lo:  .byte <pattern50,<pattern50,<pattern51,<pattern51,<pattern50,<pattern50,<pattern51,<pattern51,<pattern22,<pattern24,<pattern23,<pattern23,<pattern22,<pattern24,<pattern23,<pattern23,<pattern22,<pattern24,<pattern23,<pattern23,<pattern62,<pattern25,<pattern25,<pattern25,<pattern62
song3Voice1Hi:  .byte >pattern50,>pattern50,>pattern51,>pattern51,>pattern50,>pattern50,>pattern51,>pattern51,>pattern22,>pattern24,>pattern23,>pattern23,>pattern22,>pattern24,>pattern23,>pattern23,>pattern22,>pattern24,>pattern23,>pattern23,>pattern62,>pattern25,>pattern25,>pattern25,>pattern62,0
.eval pagecheckend = *-1
 .assert " pagecross ", floor(pagecheckstart/256),floor(pagecheckend/256) 
.eval pagecheckstart = *
song3Voice2Lo:  .byte <pattern52,<pattern52,<pattern52,<pattern52,<pattern52,<pattern52,<pattern52,<pattern52,<pattern52,<pattern52,<pattern52,<pattern52,<pattern60,<pattern61,<pattern62,<pattern62,<pattern60,<pattern61,<pattern62,<pattern62,<pattern62,<pattern62,<pattern62,<pattern62,<pattern62,<pattern62,<pattern62,<pattern62,<pattern62
song3Voice2Hi:  .byte >pattern52,0,>pattern52,>pattern52,>pattern52,>pattern52,>pattern52,>pattern52,>pattern52,>pattern52,>pattern52,>pattern52,>pattern52,>pattern60,>pattern61,>pattern62,>pattern62,>pattern60,>pattern61,>pattern62,>pattern62,>pattern62,>pattern62,>pattern62,>pattern62,>pattern62,>pattern62,>pattern62,>pattern62,>pattern62,0
.eval pagecheckend = *-1
 .assert " pagecross ", floor(pagecheckstart/256),floor(pagecheckend/256) 
.eval pagecheckstart = *
song3Voice3Lo:  .byte <pattern53,<pattern54,<pattern55,<pattern54,<pattern53,<pattern54,<pattern56,<pattern57,<pattern62,<pattern56,<pattern57,<pattern62,<pattern62,<pattern62,<pattern62,<pattern62,<pattern62,<pattern62,<pattern62,<pattern62,<pattern62
song3Voice3Hi:  .byte >pattern53,>pattern54,>pattern55,>pattern54,>pattern53,>pattern54,>pattern56,>pattern57,>pattern62,>pattern56,>pattern57,>pattern62,>pattern62,>pattern62,>pattern62,>pattern62,>pattern62,>pattern62,>pattern62,>pattern62,>pattern62,0
.eval pagecheckend = *-1
 .assert " pagecross song3Voice3Lo", floor(pagecheckstart/256),floor(pagecheckend/256) 
//      ---- song4----
.eval pagecheckstart = *
song4Voice1Lo:  .byte <patternf0
song4Voice1Hi:  .byte >patternf0,0
.eval pagecheckend = *-1
 .assert " pagecross ", floor(pagecheckstart/256),floor(pagecheckend/256) 
.eval pagecheckstart = *
song4Voice3Lo:  .byte <patternf
song4Voice3Hi:  .byte >patternf,0
.eval pagecheckend = *-1
 .assert " pagecross ", floor(pagecheckstart/256),floor(pagecheckend/256) 
.eval pagecheckstart = *
song4Voice2Lo:  .byte <patternf1
song4Voice2Hi:  .byte >patternf1,0
.eval pagecheckend = *-1
 .assert " pagecross song4Voice3Lo", floor(pagecheckstart/256),floor(pagecheckend/256) 
// ============= song pointers =================
songPointersLo1_lo:    .byte <song0Voice1Lo,<song1Voice1Lo,<song2Voice1Lo,<song3Voice1Lo,<song4Voice1Lo
songPointersLo1_hi:    .byte >song0Voice1Lo,>song1Voice1Lo,>song2Voice1Lo,>song3Voice1Lo,>song4Voice1Lo
songPointersHi1_lo:    .byte <song0Voice1Hi,<song1Voice1Hi,<song2Voice1Hi,<song3Voice1Hi,<song4Voice1Hi
songPointersHi1_hi:    .byte >song0Voice1Hi,>song1Voice1Hi,>song2Voice1Hi,>song3Voice1Hi,>song4Voice1Hi
songPointersLo2_lo:    .byte <song0Voice2Lo,<song1Voice2Lo,<song2Voice2Lo,<song3Voice2Lo,<song4Voice2Lo
songPointersLo2_hi:    .byte >song0Voice2Lo,>song1Voice2Lo,>song2Voice2Lo,>song3Voice2Lo,>song4Voice2Lo
songPointersHi2_lo:    .byte <song0Voice2Hi,<song1Voice2Hi,<song2Voice2Hi,<song3Voice2Hi,<song4Voice2Hi
songPointersHi2_hi:    .byte >song0Voice2Hi,>song1Voice2Hi,>song2Voice2Hi,>song3Voice2Hi,>song4Voice2Hi
songPointersLo3_lo:    .byte <song0Voice3Lo,<song1Voice3Lo,<song2Voice3Lo,<song3Voice3Lo,<song4Voice3Lo
songPointersLo3_hi:    .byte >song0Voice3Lo,>song1Voice3Lo,>song2Voice3Lo,>song3Voice3Lo,>song4Voice3Lo
songPointersHi3_lo:    .byte <song0Voice3Hi,<song1Voice3Hi,<song2Voice3Hi,<song3Voice3Hi,<song4Voice3Hi
songPointersHi3_hi:    .byte >song0Voice3Hi,>song1Voice3Hi,>song2Voice3Hi,>song3Voice3Hi,>song4Voice3Hi
// ============= song filter and volume params  =================
d415data:    .byte 0,0,2,0,2
d416data:    .byte 0,0,163,0,16
d417data:    .byte 0,0,0,0,34
d418data:    .byte 15,15,15,15,191
nrOfSongs:     .byte 5

