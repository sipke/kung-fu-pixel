
.var brkFile = createFile("debug.txt") 

.macro break() {
	.eval brkFile.writeln("break " + toHexString(*))
}

//super simple writer

.pc = $0801 "Basic upstart"
:BasicUpstart($0810)		
*=$0810
	lda #BLUE
	sta $d020
	jsr $e544 //clr screen

loop:
	lda #160
t0:	sta $0400
//really stupid delay
	ldx #0
!:	inx
	bne !- 
	ldx #0
!:	inx
	bne !- 


s:	lda text
	cmp #$ff
	beq done
	cmp #$fe
	bne t
!:	lda $dc01
	and #%00010000	
	bne !-	
	jsr $e544 //clr screen	
	lda #$00
	sta t + 1
	sta t0 + 1
	lda #$04
	sta t + 2
	sta t0 + 2
	inc s +1 
	bne !+
	inc s + 2
!:	jmp s
	
t:	sta $0400
	ldx #0
!:	inx
	bne !- 
	inc s + 1
	bne !+
	inc s+2
!:	inc t + 1
	bne !+
	inc t + 2
!:	inc t0 + 1
	bne !+
	inc t0 + 2
!:	jmp loop
done:	
!:	lda $dc01
	and #%00010000	
	bne !-	
	//move cursor to bottom screen
	lda #0
	sta 211
	lda #22
	sta 214
	rts


text:
//	   1234567890123456789012345678901234567890
.text "                                        "
.text "some (technical) info about this game:  "
.text "                                        "
.text "         when you play this game,       "
.text "             you're looking             "
.text "         at a blank(ed) screen . . .    " 
.text "          (bit 4 of 53265 is 0)         "  
.text "         ..a blank screen               "
.text "              that just changes a lot!  "  
.text "                                        "  
.text "          what started as               "  
.text "       an experiment with a             "  
.text "    so-called 'stable raster'           "  
.text "        ended up as a                   "  
.text "    sprite-resolution fighting game     "  
.text " run  by a custom graphics              "  
.text "         and sound game engine          "  
.text "                                        "
.text "                         (press space)  "  
.byte $fe  
.text "the pixels are created by changing the  "
.text "border color at just the right time     " 
.text "(12 times every screen line)            "
.text "it's a bit like screens are drawn       "
.text "on an atari 2600.                       "
.text "                                        "
.text "although you end up with a screen that  "  
.text "has a 12x21 pixel resolution            " 
.text "with 4 colors (just like a sprite)      "  
.text "the screen does extend into the border  "
.text "and is 252 pixels high                  "  
.text "                                        "  
.text "this way of drawing takes a lot of      "  
.text "processor time and memory and to change "  
.text "a pixel, you have to change a lot of    "  
.text "code                                    "  
.text "that's why you have the empty lines     "  
.text "in between the colored lines:           "
.text "i use that time to change the drawing   "
.text "code of the previous line.              "
.text "because i change the drawing code while "
.text "it is drawing, there are sometimes      "
.text "artefacts, but that is hardly visible.  "
.text "there is a way to prevent that but i    "
.text "didn't know then if it would fit (space)"
.byte $fe
.text "rewriting is a little bit more efficient" 
.text "then drawing so i have 18 blank lines   "
.text "to spare for other things.              "
.text "i use these 18 lines for a custom made  "
.text "music routine (the music in the intro is" 
.text "done with sid wizard by the way)        " 
.text "                                        " 
.text "creating that routine was the hardest   " 
.text "part for me because whatever it had to  "
.text "be cycle exact whatever it was doing    "
.text "(like playing a note, doing vibrato,    "
.text " changing patterns or doing nothing).   "
.text "any cycle delay meant there was tearing "
.text "on screen!                              "
.text "                                        "
.text "debugging the music routine gave me a   "
.text "lot of headaches and without the great  " 
.text "c64 debugger i dont think i would ever  "
.text "have finished this.                     "
.text "                                        "
.text "and i still think the music routine is  "
.text "worst part of the game.                 "
.text "                                        "
.text "(space)                                 "
.byte $fe
.text "choice of game:                         "
.text "                                        "
.text "i wanted to make a game with this new   "
.text "game engine but because of its          " 
.text "pretty severe resolution limitations    " 
.text "not many games seemed doable.           " 
.text "in the end i went with this.            " 
.text "                                        "
.text "after working on it for a long time     "
.text "i lost a bit of motivation and the      "
.text "development ceased for months.          "
.text "                                        "
.text "then along came this competition        "
.text "with a theme that fit!                  "
.text "so (after starting on two other ideas   "
.text " first) i finally finished this game!   "
.text "                                        "
.text "there's still stuff to do, but that's   "
.text "always the case.                        "
.text "                                        "
.text "i'm very happy i finished this game     "
.text "and i hope you enjoy playing it         "
.text "                                        "
.text "               jeroen (goerp), may 2018 "
.text "(space)                                 "
.byte $ff
/*

 
 
(I also have a version that is 294 pixels high but at the time 
i didnt know if i would have enough processor time left for the rest of the game)
Every pixel is read from zero page (the first 256 bytes) 
so almost all of the zero page is used for this

                                        "  
.text "                                        "  
.text "with a stable raster you can change the "  
.text "border color at exactly the same place  "  
.text "every screen.                           "  
.text "when i used that to create a screen full"  
.text "of 32x32 wide blocks i thought that     "  
.text "of 32x32 wide blocks i thought that     "
  
the game is run with the screen blanked 
the pixels are created by changing the border color at just the right time 
(12 times every line)



drawing the screen takes about 12k of code
to make it possible to achieve 50 fps i only draw every other line. 
on the lines inbetween the border color is not changed
but i use that time to rewrite the drawing code for the next frame


there are sometimes artefacts on screen, but that usually isnt 
visible
( again i didnt  for it).


*/
